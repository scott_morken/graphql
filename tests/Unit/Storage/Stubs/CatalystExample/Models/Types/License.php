<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types;

use Smorken\Graphql\Models\Queryable;
use Smorken\Graphql\Models\Traits\AttributeToRelation;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;

class License extends Queryable
{

    use AttributeToRelation, FakeRelations;

    protected array $fields = ['assignedAt'];

    public function licensePool()
    {
        return $this->fakeBelongsToViaCallback(
            LicensePool::class,
            function (FakeRelation $relation, License $model) {
                return $this->addAttributeAsModelForRelation('licensePool', $model, $relation);
            }
        );
    }

    protected function addToFields(array $fields): array
    {
        $fields[] = $this->licensePool()->getRelated()->newInstance()->newQuery()->defaultFields();
        return $fields;
    }
}
