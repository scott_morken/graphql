<?php

namespace Smorken\Graphql\Query;

use Illuminate\Support\Str;

abstract class Argument implements \Smorken\Graphql\Contracts\Query\Argument
{

    protected ?string $name = null;

    protected mixed $value = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): \Smorken\Graphql\Contracts\Query\Argument
    {
        $this->name = $name;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue($value, bool $isRequestArgument = true): \Smorken\Graphql\Contracts\Query\Argument
    {
        $this->value = $isRequestArgument && is_string($value) ? Str::start($value, '$') : $value;
        return $this;
    }

    public function isRequestArgument(): bool
    {
        return $this->isStringRequestArgument($this->getValue());
    }

    public function newInstance(
        string $name,
        $value,
        $isRequestArgument = true
    ): \Smorken\Graphql\Contracts\Query\Argument {
        return (new static())
            ->setName($name)
            ->setValue($value, $isRequestArgument);
    }

    protected function isStringRequestArgument($value): bool
    {
        return is_string($value) && str_starts_with($value, '$');
    }
}
