<?php

namespace Tests\Smorken\Graphql\Unit\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Clients\GuzzleProxy;
use Smorken\Graphql\Clients\MockHandlerProxy;
use Smorken\Graphql\Contracts\Clients\Proxy;
use Smorken\Graphql\PhpGraphQL\Query;
use Tests\Smorken\Graphql\Traits\Response;

class GuzzleProxyTest extends TestCase
{

    use Response;

    protected $base_path = 'Response/FromClientTest/';

    public function testAddResponseCanAddMockResponse(): void
    {
        $response = $this->makeResponse('one.model.json');
        $sut = $this->getSut();
        $sut->addResponse($response);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r);
    }

    public function testClientException(): void
    {
        $response = new ClientException('Error', new Request('POST', 'test'), new \GuzzleHttp\Psr7\Response(500));
        $sut = $this->getSut();
        $sut->addResponse($response);
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage('Error');
        $r = $sut->query(new Query());
    }

    public function testClientExceptionStatus400CanPassThrough(): void
    {
        $response = new \GuzzleHttp\Psr7\Response(400);
        $ce = new ClientException('Error', new Request('POST', 'test'), $response);
        $sut = $this->getSut();
        $sut->addResponse($ce);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r);
    }

    protected function getSut(): Proxy
    {
        $handler = new MockHandler();
        $stack = HandlerStack::create($handler);
        $proxy = new GuzzleProxy('https://localhost', new Client(['handler' => $stack]));
        $proxy->getMockHandlerProxy()->setHandler($handler);
        return $proxy;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        MockHandlerProxy::reset();
    }
}
