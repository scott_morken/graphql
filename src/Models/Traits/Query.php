<?php

namespace Smorken\Graphql\Models\Traits;

use Illuminate\Support\Str;
use Smorken\Graphql\Contracts\Models\Mutatable;
use Smorken\Graphql\Traits\ApplyFilter;
use Smorken\Support\Contracts\Filter;

trait Query
{

    use ApplyFilter;

    /**
     * @var \Smorken\Graphql\Contracts\Query\Query|null
     */
    protected static ?\Smorken\Graphql\Contracts\Query\Query $query = null;

    public static function setQuery(\Smorken\Graphql\Contracts\Query\Query $query): void
    {
        self::$query = $query;
    }

    public function getFields(): array
    {
        if ($this->fields) {
            $fields = $this->getValuesByAttributes($this->fields, true);
        } else {
            $fields = $this->getAttributes();
        }
        return $this->addToFields($fields);
    }

    public function getQuery(): \Smorken\Graphql\Contracts\Query\Query
    {
        if (is_null(self::$query)) {
            self::setQuery(new \Smorken\Graphql\PhpGraphQL\Query());
        }
        return self::$query;
    }

    public function getQueryName(bool $multi = false): string
    {
        if (property_exists($this, 'queryName') && $this->queryName) {
            $qn = $this->queryName;
        } else {
            $qn = lcfirst($this->getName());
        }
        if ($multi && property_exists($this, 'canMulti') && $this->canMulti) {
            return Str::plural($qn);
        }
        return $qn;
    }

    public function getRequestVariables(array $overrides = []): array
    {
        $vars = [];
        foreach ($this->getValuesByAttributes(array_keys($this->requestVariables)) as $attr => $val) {
            $vars[$attr] = $val;
        }
        foreach ($overrides as $k => $v) {
            $vars[$k] = $v;
        }
        return $vars;
    }

    public function newQuery(): \Smorken\Graphql\Contracts\Query\Query
    {
        return $this->defaultNewQuery(true);
    }

    public function newQueryEmpty(): \Smorken\Graphql\Contracts\Query\Query
    {
        return $this->getQuery()->newInstance(null)
                    ->setQueryType($this->getQueryType());
    }

    public function newQueryWithoutDefaults(): \Smorken\Graphql\Contracts\Query\Query
    {
        return $this->defaultNewQuery(false);
    }

    public function queryApplyFilter(
        \Smorken\Graphql\Contracts\Query\Query $query,
        ?Filter $filter
    ): \Smorken\Graphql\Contracts\Query\Query {
        if ($filter) {
            $this->applyFilterToQuery($query, $filter);
        }
        return $query;
    }

    public function queryDefaultFields(
        \Smorken\Graphql\Contracts\Query\Query $query
    ): \Smorken\Graphql\Contracts\Query\Query {
        $fields = [];
        foreach ($this->getFields() as $attr => $field) {
            if ($field instanceof \Smorken\Graphql\Contracts\Query\Query) {
                $fields[] = $field;
            } else {
                $fields[] = $attr;
            }
        }
        return $query->addFields($fields);
    }

    /**
     * Override if needed to handle additional fields (eg, query fields)
     *
     * @param  array  $fields
     * @return array
     */
    protected function addToFields(array $fields): array
    {
        return $fields;
    }

    protected function applyQueryToParentQuery(
        \Smorken\Graphql\Contracts\Query\Query $query
    ): void {
        $parent = $this->newParentQuery();
        if ($parent) {
            $parent->addField($query);
        }
    }

    protected function defaultNewQuery(
        bool $addDefaults
    ): \Smorken\Graphql\Contracts\Query\Query {
        $q = $this->getQuery()
                  ->newInstance($this)
                  ->setQueryType($this->getQueryType());
        if ($addDefaults) {
            if (property_exists($this, 'defaultQueryStack')) {
                foreach ($this->defaultQueryStack as $call => $args) {
                    $q->addToQueue($call, $args);
                }
            }
            $this->applyQueryToParentQuery($q);
        }
        return $q;
    }

    protected function getQueryType(): string
    {
        if ($this instanceof Mutatable) {
            return \Smorken\Graphql\Contracts\Query\Query::MUTATION;
        }
        return \Smorken\Graphql\Contracts\Query\Query::QUERY;
    }

    protected function getValuesByAttributes(array $attributes, bool $allow_null = false): array
    {
        $arr = [];
        foreach ($attributes as $attribute) {
            $val = $this->getAttribute($attribute);
            if ($allow_null || !is_null($val)) {
                $arr[$attribute] = $val;
            }
        }
        return $arr;
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        //return $this->defaultNewQuery(false)->setName('myName');
        return null;
    }
}
