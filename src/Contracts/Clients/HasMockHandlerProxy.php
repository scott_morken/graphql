<?php

namespace Smorken\Graphql\Contracts\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

interface HasMockHandlerProxy
{

    /**
     * @param  \Psr\Http\Message\ResponseInterface|\GuzzleHttp\Exception\GuzzleException  $response
     */
    public function addResponse(ResponseInterface|GuzzleException $response): void;

    public function getMockHandlerProxy(): MockHandlerProxy;
}
