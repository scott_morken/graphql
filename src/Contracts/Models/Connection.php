<?php

namespace Smorken\Graphql\Contracts\Models;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Query\Query;

/**
 * Interface Connection
 *
 * @package Smorken\Graphql\Contracts\Models
 *
 * @property int $totalCount
 * @property \Illuminate\Support\Collection $nodes
 * @property \Smorken\Graphql\Contracts\Models\PageInfo $pageInfo
 */
interface Connection extends Queryable
{

    const EDGE = 'edge';
    const NODE = 'node';
    const PAGEINFO = 'pageInfo';

    public function getModelByKey(string $key): Queryable;

    public function getNodes(): Collection;

    public function getQueryByKey(string $key): Query;

    public function queryAfterCursor(Query $query, string $cursor, bool $isRequestVariable = false): Query;

    public function queryFirst(Query $query, int $first): Query;

    public function setModelByKey(string $key, Queryable $model): Connection;
}
