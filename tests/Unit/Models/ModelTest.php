<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Models\Model;

class ModelTest extends TestCase
{

    public function testGetName(): void
    {
        $sut = new Model();
        $this->assertEquals('Model', $sut->getName());
    }
}
