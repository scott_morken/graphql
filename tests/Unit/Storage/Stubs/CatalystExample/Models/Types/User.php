<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;
use Smorken\Graphql\Models\Traits\AttributeToRelation;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;
use Smorken\Support\Contracts\Filter;

class User extends Queryable
{

    use AttributeToRelation, FakeRelations;

    protected array $fields = ['id', 'email', 'createdAt', 'updatedAt', 'firstName', 'lastName', 'ssoId', 'status'];

    public $keyType = 'ID';

    public function languagePrograms()
    {
        return $this->fakeHasManyViaCallback(
            LanguageProgram::class,
            function (FakeRelation $relation, User $user, Collection $collection) {
                $this->addAttributeToCollectionForRelation('languagePrograms', $user, $relation, $collection);
            }
        );
    }

    public function licenses()
    {
        return $this->fakeHasManyViaCallback(
            License::class,
            function (FakeRelation $relation, User $user, Collection $collection) {
                $this->addAttributeToCollectionForRelation('licenses', $user, $relation, $collection);
            }
        );
    }

    public function queryGroupIds(Query $query): Query
    {
        return $query->addField('groupIds');
    }

    public function queryLanguagePrograms(Query $query, array $fields = []): Query
    {
        $q = $this->languagePrograms()->getRelated()->newInstance()->newQuery()->setMulti(true);
        if ($fields) {
            $q->addFields($fields);
        } else {
            $q->defaultFields();
        }
        return $query->addfield($q);
    }

    public function queryLicenses(Query $query): Query
    {
        $q = $this->licenses()->getRelated()->newInstance()->newQuery()->setMulti(true)->defaultFields();
        return $query->addField($q);
    }

    public function querySsoIdIs(Query $query, string $ssoId): Query
    {
        $ssoId = urlencode($ssoId);
        return $query->addVariableFromComponents(
            'ssoId',
            'String',
            true
        )->addArgumentAsVariable('ssoId')
                     ->addRequestVariableFromComponents('ssoId', $ssoId);
    }

    protected function filterGroupIds(Query $query, Filter $filter, $value): Query
    {
        if ($value === true) {
            $query->groupIds();
        }
        return $query;
    }

    protected function filterLanguagePrograms(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            $query->languagePrograms(is_array($value) ? $value : []);
        }
        return $query;
    }

    protected function filterLicenses(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            $query->licenses();
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'languagePrograms' => 'filterLanguagePrograms',
            'licenses' => 'filterLicenses',
            'groupIds' => 'filterGroupIds',
        ];
    }
}
