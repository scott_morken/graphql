<?php

namespace Tests\Smorken\Graphql\Unit\PhpGraphQL;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\PhpGraphQL\Argument;

class ArgumentTest extends TestCase
{

    public function testArgumentIsNull(): void
    {
        $sut = (new Argument())->setName('foo');
        $this->assertEquals('foo: null', (string) $sut);
    }

    public function testArgumentIsString(): void
    {
        $sut = (new Argument())->setName('foo')->setValue('bar', false);
        $this->assertEquals('foo: "bar"', (string) $sut);
    }

    public function testArgumentIsBool(): void
    {
        $sut = (new Argument())->setName('foo')->setValue(true, false);
        $this->assertEquals('foo: true', (string) $sut);
    }

    public function testArgumentAsVariable(): void
    {
        $sut = (new Argument())->setName('foo')->setValue('bar');
        $this->assertEquals('foo: $bar', (string) $sut);
    }
}
