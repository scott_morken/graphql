<?php

namespace Smorken\Graphql\Contracts\Response;

use Psr\Http\Message\ResponseInterface;

interface FromClient
{

    public function getData(): array;

    public function getErrors(): array;

    public function getResponse(): ?ResponseInterface;

    public function hasData(): bool;

    public function hasErrors(): bool;

    public function newInstance(?ResponseInterface $response = null): static;

    public function setResponse(ResponseInterface $response): void;

    public function toArray(): array;
}
