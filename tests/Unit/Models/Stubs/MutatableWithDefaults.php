<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Mutatable;

class MutatableWithDefaults extends Mutatable
{

    protected array $defaultQueryStack = [
        'myFields' => [],
    ];

    protected array $rules = [
        'foo' => 'required',
        'bar' => [
            'required',
            'int',
        ],
    ];

    public function queryMyFields(Query $query): Query
    {
        return $query->addFields(['id', 'name']);
    }
}
