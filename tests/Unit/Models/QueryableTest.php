<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Models\Input;
use Smorken\Graphql\Models\Queryable;
use Smorken\Graphql\PhpGraphQL\Query;
use Smorken\Graphql\PhpGraphQL\Variable;
use Tests\Smorken\Graphql\Unit\Models\Stubs\QueryableWithDefaults;
use Tests\Smorken\Graphql\Unit\Models\Stubs\QueryNameModel;
use Tests\Smorken\Graphql\Unit\Models\Stubs\TestInput;

class QueryableTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        Query::setVariableProvider(new Variable());
        Queryable::setQuery(new Query());
    }

    public function testGetDefaultQueryName(): void
    {
        $sut = new Queryable();
        $this->assertEquals('queryable', $sut->getQueryName());
    }

    public function testGetDefaultQueryNameStripsInput(): void
    {
        $sut = new TestInput();
        $this->assertEquals('test', $sut->getQueryName());
    }

    public function testGetDefaultQueryNameWithMultiAndCanMulti(): void
    {
        $sut = new Input();
        $this->assertEquals('inputs', $sut->getQueryName(true));
    }

    public function testGetDefaultQueryNameWithMultiAndCannotMultiIsSingular(): void
    {
        $sut = new Queryable();
        $this->assertEquals('queryables', $sut->getQueryName(true));
    }

    public function testGetQueryInstanceOfQuery(): void
    {
        $sut = new Queryable();
        $this->assertInstanceOf(\Smorken\Graphql\Contracts\Query\Query::class, $sut->getQuery());
    }

    public function testGetQueryNameWithQueryNameProperty(): void
    {
        $sut = new QueryNameModel();
        $this->assertEquals('fooBar', $sut->getQueryName());
    }

    public function testNewQueryAppliesDefaults(): void
    {
        $sut = new QueryableWithDefaults();
        $q = $sut->newQuery();
        $expected = 'query {
queryableWithDefaults {
id
name
}
}';
        $this->assertEquals($expected, (string) $q);
    }

    public function testNewQueryAppliesQueryType(): void
    {
        $sut = new Queryable();
        $q = $sut->newQuery();
        $expected = 'query {
queryable
}';
        $this->assertEquals($expected, (string) $q);
    }

    public function testNewQueryIsNewInstance(): void
    {
        $sut = new Queryable();
        $this->assertNotSame($sut->newQuery(), $sut->getQuery());
    }

    public function testNewQueryWithAllDefaults(): void
    {
        $sut = new QueryableWithDefaults(['id' => 99, 'name' => 'Test', 'foo' => 'bar']);
        $q = $sut->newQuery()->defaultVariables()->defaultArguments()->defaultFields()->defaultRequestVariables();
        $expected = 'query($name: String) {
queryableWithDefaults(name: $name) {
id
name
foo
}
}';
        $this->assertEquals($expected, (string) $q);
        $this->assertEquals(['name' => 'Test'], $q->getRequestVariablesAsArray());
    }

    public function testNewQueryWithDefaultArguments(): void
    {
        $sut = new QueryableWithDefaults();
        $q = $sut->newQuery()->defaultArguments();
        $expected = 'query {
queryableWithDefaults(name: $name) {
id
name
}
}';
        $this->assertEquals($expected, (string) $q);
    }

    public function testNewQueryWithDefaultVariables(): void
    {
        $sut = new QueryableWithDefaults();
        $q = $sut->newQuery()->defaultVariables();
        $expected = 'query($name: String) {
queryableWithDefaults {
id
name
}
}';
        $this->assertEquals($expected, (string) $q);
    }

    public function testNewQueryWithoutDefaults(): void
    {
        $sut = new QueryableWithDefaults();
        $q = $sut->newQueryWithoutDefaults();
        $expected = 'query {
queryableWithDefaults
}';
        $this->assertEquals($expected, (string) $q);
    }
}
