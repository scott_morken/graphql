<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Models\Model;

class User extends Model
{

    protected array $fields = ['id', 'firstName', 'lastName'];
}
