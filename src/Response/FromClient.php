<?php

namespace Smorken\Graphql\Response;

use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Error;

class FromClient implements \Smorken\Graphql\Contracts\Response\FromClient
{

    protected ?string $body = null;

    protected ?ResponseInterface $response = null;

    public function __construct(?ResponseInterface $response = null)
    {
        if ($response) {
            $this->setResponse($response);
        }
    }

    public function getData(): array
    {
        return $this->toArray()['data'] ?? [];
    }

    public function getErrors(): array
    {
        $errors = [];
        $raws = $this->getRawErrors();
        foreach ($raws as $i => $raw) {
            $errors[$i] = $this->errorFromRaw($raw);
        }
        if ($this->useResponseError()) {
            $errors[] = new Error(
                ['message' => $this->getResponse()->getReasonPhrase()],
                $this->getResponse()->getStatusCode()
            );
        }
        return $errors;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;
        $this->body = $response->getBody()->getContents();
    }

    public function hasData(): bool
    {
        $d = $this->getData();
        return count($d) > 0 && $this->verifyDataIsValid($d);
    }

    public function hasErrors(): bool
    {
        return count($this->getErrors()) > 0;
    }

    public function newInstance(?ResponseInterface $response = null): static
    {
        return new static($response);
    }

    public function toArray(): array
    {
        return json_decode($this->body ?? '[]', true) ?? [];
    }

    protected function errorFromRaw(array|string $raw): \Smorken\Graphql\Contracts\Error
    {
        $error = new Error($raw);
        if (!$error->getStatusCode() && $this->getStatusCodeFromResponse()) {
            $error->setStatusCode($this->getStatusCodeFromResponse());
        }
        return $error;
    }

    protected function getRawErrors(): array
    {
        if (array_key_exists('errors', $this->toArray())) {
            return (array) $this->toArray()['errors'] ?? [];
        }
        if (array_key_exists('error', $this->toArray())) {
            return (array) $this->toArray()['error'] ?? [];
        }
        return [];
    }

    protected function getStatusCodeFromResponse(): int
    {
        if ($this->hasResponse()) {
            return $this->getResponse()->getStatusCode();
        }
        return 0;
    }

    protected function hasResponse(): bool
    {
        return $this->response instanceof ResponseInterface;
    }

    protected function useResponseError(): bool
    {
        return empty($this->getRawErrors()) && $this->hasResponse() && $this->getStatusCodeFromResponse() !== 200;
    }

    protected function verifyDataIsValid(array $data): bool
    {
        foreach ($data as $k => $v) {
            if (!is_null($v)) {
                return true;
            }
        }
        return false;
    }
}
