<?php

namespace Smorken\Graphql;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Smorken\Graphql\Contracts\Authenticate;
use Smorken\Graphql\Contracts\Clients\Client;
use Smorken\Graphql\Contracts\Last;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Contracts\Response\FromClient;

class Factory implements Contracts\Factory
{

    /**
     * @var \Smorken\Graphql\Contracts\Authenticate
     */
    protected Authenticate $authenticate;

    /**
     * @var \Smorken\Graphql\Contracts\Clients\Client
     */
    protected Client $client;

    /**
     * @var bool
     */
    protected bool $debug = false;

    /**
     * @var Last|null
     */
    protected ?Last $last = null;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @var bool
     */
    protected bool $retried = false;

    public function __construct(
        Authenticate $authenticate,
        Client $client,
        LoggerInterface $logger,
        bool $debug = false
    ) {
        $this->authenticate = $authenticate;
        $this->client = $client;
        $this->logger = $logger;
        $this->debug = $debug;
    }

    public function authenticate(): void
    {
        $this->getAuthenticate()->authenticate($this->getClient());
    }

    public function debug(string $message, mixed $context = []): void
    {
        if ($this->debug) {
            if (!is_array($context)) {
                $context = (array) $context;
            }
            $this->logger->debug($message, $context);
        }
    }

    public function getAuthenticate(): Authenticate
    {
        return $this->authenticate;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getLast(): Last
    {
        if (!$this->last) {
            $this->last = new \Smorken\Graphql\Last();
        }
        return $this->last;
    }

    public function query(Query $query, array $variables = []): FromClient
    {
        $this->getLast()->reset();
        $this->retried = false;
        $variables = $this->mergeVariables($query, $variables);
        return $this->tryQuery($query, $variables);
    }

    protected function addResultsToLast(FromClient $results): void
    {
        $arr = $results->toArray();
        if ($arr) {
            $this->addToLast('results', $arr);
        } else {
            $this->addToLast('results', (string) $results->getResponse()->getBody());
        }
    }

    protected function addToLast(string $key, $value): void
    {
        $this->debug("Factory [$key]", $value);
        $this->getLast()->$key = $value;
    }

    protected function getStatusCodeFromException(\Exception $exception): int
    {
        if ($exception instanceof GuzzleException) {
            return $exception->getCode();
        }
        if ($exception instanceof \Smorken\Graphql\Response\Exception) {
            return $exception->getStatusCode();
        }
        return 0;
    }

    protected function handleOtherException(\Exception $e, Query $query, array $variables): ?FromClient
    {
        if ($this->isAuthenticationException($e)) {
            return $this->onNotAuthenticated($e, $query, $variables);
        }
        if ($e instanceof ClientException) {
            return $this->onClientException($e);
        }
        throw $e;
    }

    protected function isAuthenticationException(\Exception $e): bool
    {
        $statusCode = $this->getStatusCodeFromException($e);
        return $this->getAuthenticate()->isAuthenticationException($e, $statusCode);
    }

    protected function mergeVariables(Query $query, array $vars): array
    {
        $query->build();
        return array_replace($query->getAllRequestVariablesAsArray(), $vars);
    }

    protected function onClientException(ClientException $ce): ?FromClient
    {
        return $this->getClient()->getFromClient()->newInstance($ce->getResponse());
    }

    protected function onConnectionLost(ConnectException $connectException, Query $query, array $variables): ?FromClient
    {
        if (!$this->retried) {
            sleep(1);
            $this->retried = true;
            $this->debug('connection lost, retrying...');
            return $this->tryQuery($query, $variables);
        }
        $this->logger->critical('Connection lost.', ['query' => (string) $query]);
        throw $connectException;
    }

    protected function onNotAuthenticated(\Exception $exception, Query $query, array $variables): ?FromClient
    {
        if (!$this->retried) {
            $this->retried = true;
            $this->authenticate();
            $this->debug('not authenticated, retrying...');
            return $this->tryQuery($query, $variables);
        }
        $this->logger->critical('Unable to authenticate.', ['query' => (string) $query]);
        throw $exception;
    }

    protected function tryQuery(Query $query, array $variables): FromClient
    {
        $this->getAuthenticate()->authenticateClient($this->getClient());
        $results = $this->getClient()->getFromClient()->newInstance();
        try {
            $this->addToLast('query', (string) $query);
            $this->addToLast('variables', $variables);
            $results = $this->getClient()->query($query, $variables);
            $this->addResultsToLast($results);
        } catch (ConnectException $ce) {
            $temp = $this->onConnectionLost($ce, $query, $variables);
            if ($temp) {
                $results = $temp;
            }
        } catch (\Exception $e) {
            $temp = $this->handleOtherException($e, $query, $variables);
            if ($temp) {
                $results = $temp;
            }
        } finally {
            $this->getLast()->stop();
            $this->debug('Factory [elapsed]', $this->getLast()->getElapsed());
        }
        return $results;
    }
}
