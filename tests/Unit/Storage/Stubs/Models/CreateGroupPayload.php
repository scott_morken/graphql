<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Payload;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;

class CreateGroupPayload extends Payload
{

    use FakeRelations;

    protected array $fields = ['transactionId'];

    public function group()
    {
        return $this->fakeBelongsToViaCallback(
            Group::class,
            function (FakeRelation $relation, CreateGroupPayload $model) {
                $g = $model->getAttributeFromArray('group');
                if ($g) {
                    return $relation->getRelated()->newInstance($g);
                }
                return null;
            }
        );
    }

    protected function addToFields(array $fields): array
    {
        $fields[] = $this->newMutationWithoutDefaults()
                         ->setName('group')
                         ->addFields(['id', 'name']);
        return $fields;
    }
}
