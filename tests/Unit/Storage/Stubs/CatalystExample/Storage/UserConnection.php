<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Storage;

use Smorken\Graphql\Contracts\Models\Connection;
use Smorken\Graphql\Storage\Queryable;
use Smorken\Support\Contracts\Filter;

class UserConnection extends Queryable
{

    public function chunk(Filter $filter, callable $chunk): void
    {
        $filter->f_myOrg_parent = true;
        do {
            $connection = $this->getConnectionByPageableFilter($filter);
            $continue = $this->handleChunk($filter, $connection, $chunk);
        } while ($continue);
    }

    protected function getConnectionByPageableFilter(Filter $filter): Connection
    {
        return $this->getConnectionByFilter($filter, $filter->f_first ?: 100, $filter->f_after ?: null);
    }

    protected function handleChunk(
        Filter $filter,
        ?\Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Connections\UserConnection $connection,
        callable $chunk
    ): bool {
        if (is_null($connection)) {
            return false;
        }
        $models = $connection->users;
        $r = $chunk($models);
        if ($r === false) {
            return false;
        }
        if (!$connection->pageInfo->hasNextPage || count($models) === 0) {
            return false;
        }
        $filter->f_after = $connection->pageInfo->endCursor;
        return true;
    }
}
