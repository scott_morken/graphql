<?php

namespace Smorken\Graphql\Contracts\Clients;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use Psr\Http\Message\ResponseInterface;

interface MockHandlerProxy
{

    public function addResponse(ResponseInterface|GuzzleException $response): self;

    public function getHandler(): ?MockHandler;

    public function setHandler(?MockHandler $handler): self;

    public static function getInstance(): self;

    public static function reset(): void;
}
