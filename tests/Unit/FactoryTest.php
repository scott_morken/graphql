<?php

namespace Tests\Smorken\Graphql\Unit;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Smorken\Graphql\Authenticate\BearerToken;
use Smorken\Graphql\Authenticate\None;
use Smorken\Graphql\Clients\Client;
use Smorken\Graphql\Clients\GuzzleProxy;
use Smorken\Graphql\Clients\MockHandlerProxy;
use Smorken\Graphql\Contracts\Factory;
use Smorken\Graphql\PhpGraphQL\Query;
use Smorken\Graphql\Response\FromClient;
use Tests\Smorken\Graphql\Traits\Response;

class FactoryTest extends TestCase
{

    use Response;

    public function testConnectExceptionCanFail(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $ce = new ConnectException('Unable to connect', new Request('POST', ''));
        $ce2 = new ConnectException('Unable to connect', new Request('POST', ''));
        $params['client']->addResponse($ce);
        $params['client']->addResponse($ce2);
        $params['logger']->shouldReceive('debug')->times(7);
        $params['logger']->shouldReceive('critical')->once();
        $this->expectException(ConnectException::class);
        $r = $sut->query(new Query());
    }

    public function testConnectExceptionCanRetry(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $ce = new ConnectException('Unable to connect', new Request('POST', ''));
        $response = $this->makeResponse('test.object.json');
        $params['client']->addResponse($ce);
        $params['client']->addResponse($response);
        $params['logger']->shouldReceive('debug')->times(8);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
        $this->assertEquals(['test' => true], $r->getData());
        $this->assertEmpty($r->getErrors());
        $this->assertFalse($r->hasErrors());
    }

    public function testMockHandlerOnAuthAndFactoryClient(): void
    {
        $handler = new MockHandler();
        $stack = HandlerStack::create($handler);
        $client = new \GuzzleHttp\Client(['handler' => $stack]);
        $auth = new BearerToken(
            $client,
            new Request('GET', 'http://localhost')
        );
        $auth->getMockHandlerProxy()->setHandler($handler);
        $proxy = new GuzzleProxy('https://localhost', $client);
        $proxy->getMockHandlerProxy()->setHandler($handler);
        $client = new Client($proxy, new FromClient());
        $sut = new \Smorken\Graphql\Factory($auth, $client, new NullLogger(), true);
        $mockHandlerProxy = MockHandlerProxy::getInstance();
        $authResponse = $this->makeResponse('token.json');
        $mockHandlerProxy->addResponse($authResponse);
        $response = $this->makeResponse('test.object2.json');
        $mockHandlerProxy->addResponse($response);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
        $this->assertEquals(['test2' => true], $r->getData());
        $this->assertEmpty($r->getErrors());
        $this->assertFalse($r->hasErrors());
    }

    public function testNotAuthenticatedCanFail(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response1 = $this->makeResponse('unauthorized', 401);
        $response2 = $this->makeResponse('unauthorized', 401);
        $params['client']->addResponse($response1);
        $params['client']->addResponse($response2);
        $params['logger']->shouldReceive('debug')->times(7);
        $params['logger']->shouldReceive('critical')->once();
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage('401 Unauthorized');
        $r = $sut->query(new Query());
    }

    public function testNotAuthenticatedCanRetry(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $unauthresponse = $this->makeResponse('unauthorized', 401);
        $response = $this->makeResponse('test.object.json');
        $params['client']->addResponse($unauthresponse);
        $params['client']->addResponse($response);
        $params['logger']->shouldReceive('debug')->times(8);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
        $this->assertEquals(['test' => true], $r->getData());
        $this->assertEmpty($r->getErrors());
        $this->assertFalse($r->hasErrors());
    }

    public function testNotAuthenticatedRetryFailsWithClientException(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $unauthresponse = $this->makeResponse('unauthorized', 401);
        $response = $this->makeResponse('bad_credentials.json', 400);
        $ce = new ClientException('Bad Credentials', new Request('POST', ''), $response);
        $params['client']->addResponse($unauthresponse);
        $params['client']->addResponse($ce);
        $params['logger']->shouldReceive('debug')->times(8);
        $r = $sut->query(new Query());
        $this->assertTrue($r->hasErrors());
        $this->assertEquals('Status: 400 - Message: invalid_grant - Url: None - Path: None',
            (string) $r->getErrors()[0]);
    }

    public function testNotAuthenticatedRetryFailsWithoutClientException(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $unauthresponse = $this->makeResponse('unauthorized', 401);
        $response = $this->makeResponse('bad_credentials.json', 400);
        $params['client']->addResponse($unauthresponse);
        $params['client']->addResponse($response);
        $params['logger']->shouldReceive('debug')->times(8);
        $r = $sut->query(new Query());
        $this->assertTrue($r->hasErrors());
        $this->assertEquals('Status: 400 - Message: invalid_grant - Url: None - Path: None',
            (string) $r->getErrors()[0]);
    }

    public function testValidResponse(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('test.object.json');
        $params['client']->addResponse($response);
        $params['logger']->shouldReceive('debug')->times(4);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
        $this->assertEquals(['test' => true], $r->getData());
        $this->assertEmpty($r->getErrors());
        $this->assertFalse($r->hasErrors());
    }

    public function testValidResponseHasLast(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('test.object.json');
        $params['client']->addResponse($response);
        $params['logger']->shouldReceive('debug')->times(4);
        $r = $sut->query(new Query());
        $last = $sut->getLast();
        $expected = [
            'query' => 'query',
            'variables' => [],
            'results' => [
                'data' => [
                    'test' => true,
                ],
            ],
        ];
        $last_results = $last->toArray();
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $last_results[$k]);
        }
        $this->assertGreaterThan(0, $last_results['elapsed']);
    }

    protected function getParams(): array
    {
        $handler = new MockHandler();
        $stack = HandlerStack::create($handler);
        $proxy = new GuzzleProxy('https://localhost', new \GuzzleHttp\Client(['handler' => $stack]));
        $proxy->getMockHandlerProxy()->setHandler($handler);
        $client = new Client($proxy, new FromClient());
        return [
            'authenticate' => new None(),
            'client' => $client,
            'logger' => m::mock(LoggerInterface::class),
            true,
        ];
    }

    protected function getSut(array $params): Factory
    {
        return new \Smorken\Graphql\Factory(...array_values($params));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        MockHandlerProxy::reset();
    }
}
