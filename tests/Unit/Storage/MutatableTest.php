<?php

namespace Tests\Smorken\Graphql\Unit\Storage;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Models\Mutatable;
use Smorken\Graphql\Response\ToModel;
use Tests\Smorken\Graphql\Traits\Response;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\AddUserToGroupInput;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\AddUserToGroupPayload;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\CreateGroup;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\CreateGroupInput;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\CreateGroupPayload;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\Group;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\User;

class MutatableTest extends TestCase
{

    use Response;

    protected $base_path = 'Storage/MutatableTest/';

    public function testAddViaManyMutations(): void
    {
        $params = $this->getParams(new AddUserToGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('addUserToGroup.many.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations(
            [
                new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-0']),
                new AddUserToGroupInput(['groupId' => 'group-id-1', 'userId' => 'user-id-0']),
                new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-1']),
                new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-2']),
            ]
        );
        $this->assertCount(4, $r);
        $expected = [
            'index_0' => ['user-id-0::group-id-0', 'user-id-0'],
            'index_1' => ['user-id-0::group-id-1', 'user-id-0'],
            'index_2' => ['user-id-1::group-id-0', 'user-id-1'],
            'index_3' => ['user-id-2::group-id-0', 'user-id-2'],
        ];
        foreach ($expected as $k => $v) {
            $m = $r[$k];
            $this->assertInstanceOf(AddUserToGroupPayload::class, $m);
            $this->assertEquals($v[0], $m->clientMutationId);
            $this->assertInstanceOf(User::class, $m->user);
            $this->assertEquals($v[1], $m->user->id);
        }
        $last = $params['factory']->getLast();
        $expected = 'mutation($input_0: AddUserToGroupInput! $input_1: AddUserToGroupInput! $input_2: AddUserToGroupInput! $input_3: AddUserToGroupInput!) {
index_0: addUserToGroup(input: $input_0) {
clientMutationId
user {
id
}
}
index_1: addUserToGroup(input: $input_1) {
clientMutationId
user {
id
}
}
index_2: addUserToGroup(input: $input_2) {
clientMutationId
user {
id
}
}
index_3: addUserToGroup(input: $input_3) {
clientMutationId
user {
id
}
}
}';
        $this->assertEquals($expected, $last->query);
        $expected = [
            'input_0' => [
                'userId' => 'user-id-0',
                'groupId' => 'group-id-0',
            ],
            'input_1' => [
                'userId' => 'user-id-0',
                'groupId' => 'group-id-1',
            ],
            'input_2' => [
                'userId' => 'user-id-1',
                'groupId' => 'group-id-0',
            ],
            'input_3' => [
                'userId' => 'user-id-2',
                'groupId' => 'group-id-0',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
    }

    public function testAddViaManyMutationsWithError(): void
    {
        $params = $this->getParams(new AddUserToGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('addUserToGroup.many.withError.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations(
            [
                new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-0']),
                new AddUserToGroupInput(['groupId' => 'group-id-1', 'userId' => 'user-id-0']),
                new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-1']),
                new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-2']),
            ]
        );
        $this->assertCount(3, $r);
        $expected = [
            'index_0' => ['user-id-0::group-id-0', 'user-id-0'],
            'index_1' => ['user-id-0::group-id-1', 'user-id-0'],
            'index_3' => ['user-id-2::group-id-0', 'user-id-2'],
        ];
        foreach ($expected as $k => $v) {
            $m = $r[$k];
            $this->assertInstanceOf(AddUserToGroupPayload::class, $m);
            $this->assertEquals($v[0], $m->clientMutationId);
            $this->assertInstanceOf(User::class, $m->user);
            $this->assertEquals($v[1], $m->user->id);
        }
        $this->assertTrue($sut->hasErrors());
        $this->assertEquals(
            [
                'message' => '409: Conflict',
                'locations' => [],
                'path' => [
                    'createUser',
                ],
                'extensions' => [
                    'code' => 'INTERNAL_SERVER_ERROR',
                    'response' => [
                        'url' => 'http://ego/api/users/',
                        'status' => 409,
                        'statusText' => 'Conflict',
                        'body' => [
                            'type' => 'ErrorResponse',
                            'error' => 'DATA_CONFLICT',
                            'error_description' => 'user-id-1 already exists in group-id-0',
                        ],
                    ],
                ],
            ],
            $sut->getErrors()[0]->toRawArray()
        );
        $last = $params['factory']->getLast();
        $expected = 'mutation($input_0: AddUserToGroupInput! $input_1: AddUserToGroupInput! $input_2: AddUserToGroupInput! $input_3: AddUserToGroupInput!) {
index_0: addUserToGroup(input: $input_0) {
clientMutationId
user {
id
}
}
index_1: addUserToGroup(input: $input_1) {
clientMutationId
user {
id
}
}
index_2: addUserToGroup(input: $input_2) {
clientMutationId
user {
id
}
}
index_3: addUserToGroup(input: $input_3) {
clientMutationId
user {
id
}
}
}';
        $this->assertEquals($expected, $last->query);
        $expected = [
            'input_0' => [
                'userId' => 'user-id-0',
                'groupId' => 'group-id-0',
            ],
            'input_1' => [
                'userId' => 'user-id-0',
                'groupId' => 'group-id-1',
            ],
            'input_2' => [
                'userId' => 'user-id-1',
                'groupId' => 'group-id-0',
            ],
            'input_3' => [
                'userId' => 'user-id-2',
                'groupId' => 'group-id-0',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
    }

    public function testAddViaOneMutation(): void
    {
        $params = $this->getParams(new AddUserToGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('addUserToGroup.one.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->oneMutation(new AddUserToGroupInput(['groupId' => 'group-id-0', 'userId' => 'user-id-0']));
        $this->assertInstanceOf(AddUserToGroupPayload::class, $r);
        $this->assertEquals('user-id-0::group-id-0', $r->clientMutationId);
        $this->assertInstanceOf(User::class, $r->user);
        $this->assertEquals('user-id-0', $r->user->id);
        $last = $params['factory']->getLast();
        $expected = 'mutation($input: AddUserToGroupInput!) {
addUserToGroup(input: $input) {
clientMutationId
user {
id
}
}
}';
        $this->assertEquals($expected, $last->query);
        $expected = [
            'input' => [
                'userId' => 'user-id-0',
                'groupId' => 'group-id-0',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
    }

    public function testManyMutationMinimalQuery(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations([new Mutatable(), new Mutatable()]);
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation {
index_0: mutatable
index_1: mutatable
}',
            $last->query
        );
        $this->assertEquals([], $last->variables);
        $this->assertEmpty($r);
    }

    public function testManyMutationsInputNoVariables(): void
    {
        $params = $this->getParams(new CreateGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations([new CreateGroupInput(), new CreateGroupInput()]);
        $last = $sut->getFactory()->getLast();
        $expected = 'mutation($input_0: CreateGroupInput! $input_1: CreateGroupInput!) {
index_0: createGroup(input: $input_0) {
transactionId
group {
id
name
}
}
index_1: createGroup(input: $input_1) {
transactionId
group {
id
name
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals(['input_0' => [], 'input_1' => []], $last->variables);
        $this->assertEmpty($r);
    }

    public function testManyMutationsInputWithVariables(): void
    {
        $params = $this->getParams(new CreateGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('createGroup.many.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations(
            [
                new CreateGroupInput(['name' => 'Foo Group', 'description' => 'Awesome group']),
                new CreateGroupInput(['name' => 'Bar Group', 'description' => 'Another awesome group']),
            ]
        );
        $last = $sut->getFactory()->getLast();
        $expected = 'mutation($input_0: CreateGroupInput! $input_1: CreateGroupInput!) {
index_0: createGroup(input: $input_0) {
transactionId
group {
id
name
}
}
index_1: createGroup(input: $input_1) {
transactionId
group {
id
name
}
}
}';
        $this->assertEquals($expected, $last->query);
        $expected = [
            'input_0' => [
                'name' => 'Foo Group',
                'description' => 'Awesome group',
            ],
            'input_1' => [
                'name' => 'Bar Group',
                'description' => 'Another awesome group',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
        $this->assertCount(2, $r);
        $this->assertInstanceOf(CreateGroupPayload::class, $r['index_0']);
        $this->assertEquals('test.foo.bar', $r['index_0']->transactionId);
        $this->assertInstanceOf(Group::class, $r['index_0']->group);
        $this->assertEquals('group-id-99', $r['index_0']->group->id);
        $this->assertEquals('Foo Group', $r['index_0']->group->name);
        $this->assertInstanceOf(CreateGroupPayload::class, $r['index_1']);
        $this->assertEquals('test.foo.foo', $r['index_1']->transactionId);
        $this->assertInstanceOf(Group::class, $r['index_1']->group);
        $this->assertEquals('group-id-100', $r['index_1']->group->id);
        $this->assertEquals('Bar Group', $r['index_1']->group->name);
    }

    public function testManyMutationsInputWithVariablesCheckChunkCounter(): void
    {
        $params = $this->getParams(new CreateGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('createGroup.many.1.json');
        $response2 = $this->makeResponse('createGroup.many.2.json');
        $params['factory']->getClient()->addResponse($response);
        $params['factory']->getClient()->addResponse($response2);
        $r = $sut->manyMutations(
            [
                new CreateGroupInput(['name' => 'Foo Group 1']),
                new CreateGroupInput(['name' => 'Foo Group 2']),
                new CreateGroupInput(['name' => 'Foo Group 3']),
                new CreateGroupInput(['name' => 'Foo Group 4']),
            ],
            2
        );
        $last = $sut->getFactory()->getLast();
        $expected = 'mutation($input_2: CreateGroupInput! $input_3: CreateGroupInput!) {
index_2: createGroup(input: $input_2) {
transactionId
group {
id
name
}
}
index_3: createGroup(input: $input_3) {
transactionId
group {
id
name
}
}
}';
        $this->assertEquals($expected, $last->query);
        $expected = [
            'input_2' => [
                'name' => 'Foo Group 3',
            ],
            'input_3' => [
                'name' => 'Foo Group 4',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
        $this->assertCount(4, $r);
        $this->assertEquals('group-id-99', $r['index_0']->group->id);
        $this->assertEquals('group-id-100', $r['index_1']->group->id);
        $this->assertEquals('group-id-101', $r['index_2']->group->id);
        $this->assertEquals('group-id-102', $r['index_3']->group->id);
    }

    public function testManyMutationsInputWithVariablesExcludesMissingModel(): void
    {
        $params = $this->getParams(new CreateGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('createGroup.many.missing.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations(
            [
                new CreateGroupInput(['name' => 'Foo Group', 'description' => 'Awesome group']),
                new CreateGroupInput(['name' => 'Bar Group', 'description' => 'Another awesome group']),
            ]
        );
        $last = $sut->getFactory()->getLast();
        $expected = 'mutation($input_0: CreateGroupInput! $input_1: CreateGroupInput!) {
index_0: createGroup(input: $input_0) {
transactionId
group {
id
name
}
}
index_1: createGroup(input: $input_1) {
transactionId
group {
id
name
}
}
}';
        $this->assertEquals($expected, $last->query);
        $expected = [
            'input_0' => [
                'name' => 'Foo Group',
                'description' => 'Awesome group',
            ],
            'input_1' => [
                'name' => 'Bar Group',
                'description' => 'Another awesome group',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
        $this->assertCount(1, $r);
        $this->assertInstanceOf(CreateGroupPayload::class, $r['index_1']);
        $this->assertEquals('test.foo.foo', $r['index_1']->transactionId);
        $this->assertInstanceOf(Group::class, $r['index_1']->group);
        $this->assertEquals('group-id-100', $r['index_1']->group->id);
        $this->assertEquals('Bar Group', $r['index_1']->group->name);
    }

    public function testManyMutationsNotInputNoVariables(): void
    {
        $params = $this->getParams(new CreateGroup());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations([new CreateGroup(), new CreateGroup()]);
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation {
index_0: createGroup {
transactionId
group {
id
name
}
}
index_1: createGroup {
transactionId
group {
id
name
}
}
}',
            $last->query
        );
        $this->assertEquals([], $last->variables);
        $this->assertEmpty($r);
    }

    public function testManyMutationsNotInputWithVariables(): void
    {
        $params = $this->getParams(new CreateGroup());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->manyMutations([new CreateGroup(['name' => 'Foo Group']), new CreateGroup(['name' => 'Bar Group'])]);
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation($name_0: String! $name_1: String!) {
index_0: createGroup(name: $name_0) {
transactionId
group {
id
name
}
}
index_1: createGroup(name: $name_1) {
transactionId
group {
id
name
}
}
}',
            $last->query
        );
        $this->assertEquals(['name_0' => 'Foo Group', 'name_1' => 'Bar Group'], $last->variables);
        $this->assertEmpty($r);
    }

    public function testOneMutationInputNoVariables(): void
    {
        $params = $this->getParams(new CreateGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->oneMutation(new CreateGroupInput());
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation($input: CreateGroupInput!) {
createGroup(input: $input) {
transactionId
group {
id
name
}
}
}',
            $last->query
        );
        $this->assertEquals(['input' => []], $last->variables);
        $this->assertNull($r);
    }

    public function testOneMutationInputWithVariables(): void
    {
        $params = $this->getParams(new CreateGroupInput());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('createGroup.one.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->oneMutation(new CreateGroupInput(['name' => 'Foo Group', 'description' => 'Awesome group']));
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation($input: CreateGroupInput!) {
createGroup(input: $input) {
transactionId
group {
id
name
}
}
}',
            $last->query
        );
        $expected = [
            'input' => [
                'name' => 'Foo Group',
                'description' => 'Awesome group',
            ],
        ];
        $this->assertEquals($expected, $last->variables);
        $this->assertInstanceOf(CreateGroupPayload::class, $r);
        $this->assertEquals('test.foo.bar', $r->transactionId);
        $this->assertInstanceOf(Group::class, $r->group);
        $this->assertEquals('group-id-99', $r->group->id);
        $this->assertEquals('Foo Group', $r->group->name);
    }

    public function testOneMutationMinimalQuery(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->oneMutation(new Mutatable());
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation {
mutatable
}',
            $last->query
        );
        $this->assertEquals([], $last->variables);
        $this->assertNull($r);
    }

    public function testOneMutationNotInputNoVariables(): void
    {
        $params = $this->getParams(new CreateGroup());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('empty.object.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->oneMutation(new CreateGroup());
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation {
createGroup {
transactionId
group {
id
name
}
}
}',
            $last->query
        );
        $this->assertEquals([], $last->variables);
        $this->assertNull($r);
    }

    public function testOneMutationNotInputWithVariables(): void
    {
        $params = $this->getParams(new CreateGroup());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('createGroup.one.json');
        $params['factory']->getClient()->addResponse($response);
        $r = $sut->oneMutation(new CreateGroup(['name' => 'Foo Group']));
        $last = $sut->getFactory()->getLast();
        $this->assertEquals(
            'mutation($name: String!) {
createGroup(name: $name) {
transactionId
group {
id
name
}
}
}',
            $last->query
        );
        $this->assertEquals(['name' => 'Foo Group'], $last->variables);
        $this->assertInstanceOf(CreateGroup::class, $r);
        $this->assertEquals('test.foo.bar', $r->transactionId);
        $this->assertInstanceOf(Group::class, $r->group);
        $this->assertEquals('group-id-99', $r->group->id);
        $this->assertEquals('Foo Group', $r->group->name);
    }

    protected function getParams(?Model $model = null): array
    {
        if (is_null($model)) {
            $model = new \Smorken\Graphql\Models\Mutatable();
        }
        return [
            'model' => $model,
            'factory' => $this->createFactory(),
            'toModel' => new ToModel(),
        ];
    }

    protected function getSut(array $params): \Smorken\Graphql\Contracts\Storage\Mutatable
    {
        return new \Smorken\Graphql\Storage\Mutatable(...array_values($params));
    }
}
