<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;

class QueryableWithDefaults extends Queryable
{

    protected array $defaultQueryStack = [
        'myFields' => [],
    ];

    protected array $requestVariables = [
        'name' => [
            'type' => 'String',
            'required' => false,
        ],
    ];

    public function queryDefaultArguments(Query $query): Query
    {
        foreach ($this->requestVariables as $k => $data) {
            $query->addArgumentAsVariable($k);
        }
        return $query;
    }

    public function queryDefaultRequestVariables(Query $query): Query
    {
        foreach ($this->getRequestVariables() as $k => $v) {
            $query->addRequestVariableFromComponents($k, $v);
        }
        return $query;
    }

    public function queryDefaultVariables(Query $query): Query
    {
        foreach ($this->requestVariables as $k => $data) {
            $query->addVariableFromComponents(
                $k,
                $data['type'] ?? 'String',
                $data['required'] ?? false,
                $data['default'] ?? null
            );
        }
        return $query;
    }

    public function queryMyFields(Query $query): Query
    {
        return $query->addFields(['id', 'name']);
    }
}
