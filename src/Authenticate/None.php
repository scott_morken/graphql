<?php

namespace Smorken\Graphql\Authenticate;

use Smorken\Graphql\Contracts\Clients\Client;

class None extends Base
{

    public function authenticate(...$params): void
    {
        // do nothing, this is an empty handler
    }

    public function authenticateClient(Client $client): void
    {
        // do nothing, this is an empty handler
    }
}
