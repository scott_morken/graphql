<?php

namespace Smorken\Graphql\PhpGraphQL;

class Variable extends \Smorken\Graphql\Query\Variable implements \Smorken\Graphql\Contracts\Query\Variable
{

    public function __toString(): string
    {
        return (string) $this->get();
    }

    public function get(): mixed
    {
        return new \GraphQL\Variable(
            $this->getName(),
            $this->getType(),
            $this->getIsRequired(),
            $this->getDefaultValue()
        );
    }
}
