<?php

namespace Smorken\Graphql\Models;

use Smorken\Graphql\Models\Traits\Mutate;

class Mutatable extends Model implements \Smorken\Graphql\Contracts\Models\Mutatable
{

    use \Smorken\Graphql\Models\Traits\Query, Mutate;

    protected array $rules = [];
}
