<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Storage\Queryable;
use Smorken\Support\Contracts\Filter;

class Group extends Queryable
{

    protected function filterOrgId(Query $query, Filter $filter, $value): Query
    {
        if (strlen($value)) {
            return $query->orgIdIs($value);
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_org_id' => 'filterOrgId',
        ];
    }
}
