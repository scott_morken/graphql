<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Models\Queryable;

class QueryNameModel extends Queryable
{

    protected ?string $queryName = 'fooBar';
}
