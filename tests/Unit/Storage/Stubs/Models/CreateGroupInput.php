<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Input;

class CreateGroupInput extends Input
{

    protected array $inputAttributes = ['name', 'description'];

    protected \Smorken\Graphql\Contracts\Models\Payload|string $payload = CreateGroupPayload::class;
}
