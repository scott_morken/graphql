<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Illuminate\Support\Collection;
use Smorken\Graphql\Models\Connection;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;

class UserConnection extends Connection
{

    use FakeRelations;

    protected array $models = [
        self::NODE => User::class,
        self::EDGE => \Smorken\Graphql\Models\Queryable::class,
        self::PAGEINFO => \Smorken\Graphql\Models\PageInfo::class,
    ];

    public function users()
    {
        return $this->fakeHasManyViaCallback(
            User::class,
            function (FakeRelation $relation, UserConnection $model, Collection $collection) {
                foreach ($model->nodes as $node) {
                    if (is_array($node)) {
                        $node = $relation->getRelated()->newInstance($node);
                    }
                    $collection->push($node);
                }
            }
        );
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_first' => 'filterFirst',
            'f_after_cursor' => 'filterAfterCursor',
        ];
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        $group = (new Group())->newQueryWithoutDefaults();
        $org = (new MyOrg())->newQueryWithoutDefaults()->addField($group);
        return $group;
    }
}
