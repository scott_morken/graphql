<?php

namespace Smorken\Graphql\Contracts\Query;

use Smorken\Graphql\Contracts\Models\Model;

interface Query
{

    const MUTATION = 'mutation';
    const QUERY = 'query';

    public function __toString(): string;

    public function addArgument(Argument $argument, ?string $index = null): self;

    public function addArgumentAsVariable(string $key, ?string $index = null): self;

    public function addArgumentFromComponents(
        string $key,
        $value,
        bool $isRequestArgument,
        ?string $index = null
    ): self;

    public function addArguments(array $arguments, ?string $index = null): self;

    public function addField($field): self;

    public function addFields(array $fields): self;

    public function addRequestVariable(RequestVariable $requestVariable, ?string $index = null): self;

    public function addRequestVariableFromComponents(string $key, $value, ?string $index = null): self;

    public function addRequestVariables(array $variables, ?string $index = null): self;

    public function addToQueue(string $name, array $args): self;

    public function addVariable(Variable $variable, ?string $index = null): self;

    public function addVariableFromComponents(
        string $name,
        string $type = 'String',
        bool $required = true,
        $default = null,
        ?string $index = null
    ): self;

    public function addVariables(array $variables, ?string $index = null): self;

    public function build(): void;

    public function fromString(string $query): self;

    /**
     * Return the query object
     *
     * @return mixed
     */
    public function get(): mixed;

    public function getAllRequestVariables(): array;

    public function getAllRequestVariablesAsArray(): array;

    public function getAllVariables(): array;

    public function getArguments(): array;

    public function getBaseQuery(bool $build = false): Query;

    public function getCallStack(): array;

    public function getFields(): array;

    public function getIndex(?string $index = null): ?string;

    public function getName(): string;

    public function getOperationName(): string;

    public function getParent(): ?Query;

    public function getQueryObject(string $name = ''): mixed;

    public function getRequestVariables(): array;

    public function getRequestVariablesAsArray(): array;

    public function getUseIndex(): bool;

    public function getVariables(): array;

    public function newFromSelf(
        bool $useModel = true,
        bool $useQueryObject = false
    ): Query;

    /**
     * @param  \Smorken\Graphql\Contracts\Models\Model|null  $model
     * @param  object|null  $queryObject
     * @return static
     */
    public function newInstance(?Model $model, ?object $queryObject = null): static;

    public function setIndex(?string $index): self;

    public function setModel(Model $model): self;

    public function setMulti(bool $multi = false): self;

    public function setName(string $name): self;

    public function setOperationName(string $name): self;

    public function setParent(Query $query): Query;

    public function setQueryObject(object $queryObject): self;

    public function setQueryType(string $type): self;

    public function setUseIndex(bool $use): self;

    public static function setArgumentProvider(Argument $argument): void;

    public static function setRequestVariableProvider(RequestVariable $requestVariable): void;

    public static function setVariableProvider(Variable $variable): void;
}
