<?php

namespace Smorken\Graphql\Models;

use Smorken\Graphql\Contracts\Models\Payload;
use Smorken\Graphql\Contracts\Query\Query;

class Input extends Mutatable implements \Smorken\Graphql\Contracts\Models\Input
{

    /**
     * Which attributes to use for input array
     * defaults to empty which is all attributes
     *
     * @var array
     */
    protected array $inputAttributes = [];

    protected string $inputVariableName = 'input';

    /**
     * @var string|Payload
     */
    protected string|Payload $payload = \Smorken\Graphql\Models\Payload::class;

    /**
     * Get payload model fields as the payload is populated in the result
     *
     * @return array
     */
    public function getFields(): array
    {
        $fields = $this->getPayloadModel()->getFields();
        return $this->addToFields($fields);
    }

    public function getInputVariableName(): string
    {
        return $this->inputVariableName;
    }

    public function getPayloadModel(): Payload
    {
        if (is_string($this->payload)) {
            return new $this->payload;
        }
        return $this->payload;
    }

    public function getQueryName(bool $multi = false): string
    {
        $name = parent::getQueryName($multi);
        if (preg_match('/.+Input/', $name)) {
            return str_ireplace('Input', '', $name);
        }
        return $name;
    }

    public function getRequestVariables(array $overrides = []): array
    {
        $combined = array_replace($this->toInput(), $overrides);
        return [$this->getInputVariableName() => $combined];
    }

    public function queryDefaultVariables(Query $query, array $overrides = []): Query
    {
        foreach ($this->getRequestVariables($overrides) as $key => $value) {
            $this->addVariableByKey($query, $key);
            $this->addArgumentAsVariable($query, $key);
            $this->addRequestVariable($query, $key, $value);
        }
        return $query;
    }

    public function setPayloadModel(Payload $model): \Smorken\Graphql\Contracts\Models\Input
    {
        $this->payload = $model;
        return $this;
    }

    public function toInput(): array
    {
        return $this->getValuesByAttributes($this->getAllowedInputAttributes());
    }

    protected function addInputVariable(Query $query, string $key): void
    {
        $query->addVariableFromComponents($key, $this->getName(), true);
    }

    protected function addVariableByKey(Query $query, string $key): void
    {
        $reqvar = $this->requestVariables[$key] ?? [];
        if ($key === $this->getInputVariableName() && empty($reqvar)) {
            $this->addInputVariable($query, $key);
            return;
        }
        $query->addVariableFromComponents(
            $key,
            $reqvar['type'] ?? 'String',
            $reqvar['required'] ?? false,
            $reqvar['default'] ?? null
        );
    }

    protected function getAllowedInputAttributes(): array
    {
        if ($this->inputAttributes) {
            return $this->inputAttributes;
        }
        return $this->fields;
    }
}
