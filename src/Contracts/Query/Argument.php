<?php

namespace Smorken\Graphql\Contracts\Query;

interface Argument
{

    public function __toString(): string;

    /**
     * Get the proxied item
     *
     * @return mixed
     */
    public function get(): mixed;

    public function getName(): string;

    public function getValue(): mixed;

    public function isRequestArgument(): bool;

    public function newInstance(string $name, $value): Argument;

    public function setName(string $name): self;

    public function setValue($value, bool $isRequestArgument = true): self;
}
