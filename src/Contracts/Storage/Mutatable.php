<?php

namespace Smorken\Graphql\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Models\Model;

interface Mutatable extends Queryable
{

    /**
     * @param  array<\Smorken\Graphql\Contracts\Models\Mutatable>  $models
     * @param  int  $per_transaction
     * @return \Illuminate\Support\Collection<Model>
     */
    public function manyMutations(array $models, int $per_transaction = 100): Collection;

    /**
     * @param  \Smorken\Graphql\Contracts\Models\Mutatable  $model
     * @return \Smorken\Graphql\Contracts\Models\Model|null
     */
    public function oneMutation(\Smorken\Graphql\Contracts\Models\Mutatable $model): ?Model;
}
