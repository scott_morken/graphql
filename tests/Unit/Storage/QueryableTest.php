<?php

namespace Tests\Smorken\Graphql\Unit\Storage;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Storage\Queryable;
use Smorken\Graphql\Response\ToModel;
use Smorken\Support\Filter;
use Tests\Smorken\Graphql\Traits\Response;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\GroupConnection;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\LicensePool;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\MyOrg;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\QueryableWithApplyFilter;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\QueryableWithFields;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\QueryableWithFilter;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\UserConnection;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage\Group;

class QueryableTest extends TestCase
{

    use Response;

    protected $base_path = 'Storage/QueryableTest/';

    public function testAllManyAtBaseLevelNotMulti(): void
    {
        $params = $this->getParams(new MyOrg());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('myOrg.many.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->all();
        $this->assertCount(2, $all);
        $this->assertEquals('some-org-id-1', $all['myOrg_0']->id);
        $this->assertEquals('Test Org 1', $all['myOrg_0']->name);
        $this->assertEquals('some-org-id-2', $all['myOrg_1']->id);
        $this->assertEquals('Test Org 2', $all['myOrg_1']->name);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
id
namespace
name
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testAllManyWithBaseQueryIsMulti(): void
    {
        $params = $this->getParams(new LicensePool());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('licensePool.many.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->all();
        $this->assertCount(3, $all);
        $this->assertEquals('SalesPackageConfig.id-0', $all['licensePools_0']->id);
        $this->assertEquals('SalesPackageConfig.id-1', $all['licensePools_1']->id);
        $this->assertEquals('SalesPackageConfig.id-2', $all['licensePools_2']->id);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
licensePools {
id
productId
productName
salesPackageId
salesPackageName
poNumber
createdAt
totalLicenseCount
assignedLicenseCount
licenseModel
termStart
termEnd
}
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testAllOneAtBaseLevelNotMulti(): void
    {
        $params = $this->getParams(new MyOrg());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('myOrg.one.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->all();
        $this->assertCount(1, $all);
        $this->assertEquals('some-org-id-1', $all['myOrg']->id);
        $this->assertEquals('Test Org 1', $all['myOrg']->name);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
id
namespace
name
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testAllOneWithBaseQueryIsMulti(): void
    {
        $params = $this->getParams(new LicensePool());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('licensePool.one.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->all();
        $this->assertCount(1, $all);
        $this->assertEquals('SalesPackageConfig.id-0', $all['licensePools']->id);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
licensePools {
id
productId
productName
salesPackageId
salesPackageName
poNumber
createdAt
totalLicenseCount
assignedLicenseCount
licenseModel
termStart
termEnd
}
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testAnotherQueryResetsErrors(): void
    {
        $params = $this->getParams(new MyOrg());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('error.404.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->all();
        $this->assertCount(0, $all);
        $this->assertTrue($sut->hasErrors());
        $this->assertEquals(
            "Status: 404 - Message: No user found in with externalId 'abc123' in org 'org-id-0' - Url: http://ego/api/users/userBySsoExternalId/abc123/org-id-0 - Path: 0: userBySsoId",
            (string) $sut->getErrors()[0]
        );
        $response = $this->makeResponse('one.model.json');
        $params['factory']->getClient()->addResponse($response);
        $one = $sut->find('id');
        $this->assertInstanceOf(MyOrg::class, $one);
        $this->assertFalse($sut->hasErrors());
    }

    public function testFind(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('one.model.json');
        $params['factory']->getClient()->addResponse($response);
        $m = $sut->find(1);
        $this->assertInstanceOf(\Smorken\Graphql\Contracts\Models\Queryable::class, $m);
        $this->assertEquals(1, $m->id);
        $this->assertEquals('One Model', $m->name);
        $expected = 'query($id: Int!) {
queryable(id: $id)
}';
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['query']);
        $expected = ['id' => 1];
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['variables']);
    }

    public function testFindWithDefaultFields(): void
    {
        $params = $this->getParams(new QueryableWithFields());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('one.model.json');
        $params['factory']->getClient()->addResponse($response);
        $m = $sut->find(1);
        $this->assertInstanceOf(\Smorken\Graphql\Contracts\Models\Queryable::class, $m);
        $this->assertEquals(1, $m->id);
        $this->assertEquals('One Model', $m->name);
        $expected = 'query($id: Int!) {
queryableWithFields(id: $id) {
id
name
foo
}
}';
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['query']);
    }

    public function testFindWithFilter(): void
    {
        $params = $this->getParams(new QueryableWithFilter());
        $sut = new \Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage\QueryableWithFilter(...array_values($params));
        $response = $this->makeResponse('one.model.json');
        $params['factory']->getClient()->addResponse($response);
        $m = $sut->findWithFilter(1, new Filter(['f_apply_field' => 'fooBar']));
        $this->assertInstanceOf(\Smorken\Graphql\Contracts\Models\Queryable::class, $m);
        $this->assertEquals(1, $m->id);
        $this->assertEquals('One Model', $m->name);
        $expected = 'query($id: Int!) {
queryableWithFilter(id: $id) {
fooBar
}
}';
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['query']);
        $expected = ['id' => 1];
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['variables']);
    }

    public function testFindWithModelFilter(): void
    {
        $params = $this->getParams(new QueryableWithApplyFilter());
        $sut = new \Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage\QueryableWithModelFilter(...array_values($params));
        $response = $this->makeResponse('one.model.json');
        $params['factory']->getClient()->addResponse($response);
        $m = $sut->findWithFilter(1, new Filter(['f_apply_field' => 'fooBar', 'f_something_is' => 'bizBuz']));
        $this->assertInstanceOf(\Smorken\Graphql\Contracts\Models\Queryable::class, $m);
        $this->assertEquals(1, $m->id);
        $this->assertEquals('One Model', $m->name);
        $expected = 'query($id: Int!) {
queryableWithApplyFilter(id: $id something: "bizBuz") {
fooBar
}
}';
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['query']);
        $expected = ['id' => 1];
        $this->assertEquals($expected, $sut->getFactory()->getLast()->toArray()['variables']);
    }

    public function testGetByFilterAppliesFilter(): void
    {
        $params = $this->getParams(new GroupConnection());
        $sut = new Group(...array_values($params));
        $response = $this->makeResponse('group.connection.edges.json');
        $params['factory']->getClient()->addResponse($response);
        $c = $sut->getConnectionByFilter(new Filter(['f_org_id' => 'org-id-1']));
        $all = $c->nodes;
        $this->assertCount(2, $all);
        $this->assertEquals('some-group-id-1', $all[0]->id);
        $this->assertEquals('Test Group 1', $all[0]->name);
        $this->assertEquals('some-group-id-2', $all[1]->id);
        $this->assertEquals('Test Group 2', $all[1]->name);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg(orgId: "org-id-1") {
groupConnections(first: 100 after: null) {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testGetByFilterBase(): void
    {
        $params = $this->getParams(new GroupConnection());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('group.connection.edges.json');
        $params['factory']->getClient()->addResponse($response);
        $c = $sut->getConnectionByFilter(new Filter());
        $all = $c->nodes;
        $this->assertCount(2, $all);
        $this->assertEquals('some-group-id-1', $all[0]->id);
        $this->assertEquals('Test Group 1', $all[0]->name);
        $this->assertEquals('some-group-id-2', $all[1]->id);
        $this->assertEquals('Test Group 2', $all[1]->name);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
groupConnections(first: 100 after: null) {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testGetByFilterCollection(): void
    {
        $params = $this->getParams(new UserConnection());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('groups.user.connection.nodes.json');
        $params['factory']->getClient()->addResponse($response);
        $many = $sut->getConnectionsByFilter(new Filter());
        $this->assertCount(2, $many);
        $nodes = $many['groups_0']->nodes;
        $this->assertEquals($nodes, $many['groups_0']->users);
        $this->assertCount(3, $nodes);
        $this->assertEquals('user-id-1', $nodes[0]->id);
        $this->assertEquals('user-id-2', $nodes[1]->id);
        $this->assertEquals('user-id-3', $nodes[2]->id);
        $nodes = $many['groups_1']->nodes;
        $this->assertEquals($nodes, $many['groups_1']->users);
        $this->assertCount(2, $nodes);
        $this->assertEquals('user-id-1', $nodes[0]->id);
        $this->assertEquals('user-id-4', $nodes[1]->id);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
group {
userConnections(first: 100 after: null) {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
firstName
lastName
email
}
}
}
}
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    public function testGetByFilterWithNodes(): void
    {
        $params = $this->getParams(new GroupConnection());
        $sut = $this->getSut($params);
        $response = $this->makeResponse('group.connection.nodes.json');
        $params['factory']->getClient()->addResponse($response);
        $c = $sut->getConnectionByFilter(new Filter());
        $all = $c->nodes;
        $this->assertCount(2, $all);
        $this->assertEquals('some-group-id-1', $all[0]->id);
        $this->assertEquals('Test Group 1', $all[0]->name);
        $this->assertEquals('some-group-id-2', $all[1]->id);
        $this->assertEquals('Test Group 2', $all[1]->name);
        $last = $sut->getFactory()->getLast()->toArray();
        $expected = 'query {
myOrg {
groupConnections(first: 100 after: null) {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}
}';
        $this->assertEquals($expected, $last['query']);
    }

    protected function getParams(?Model $model = null): array
    {
        if (is_null($model)) {
            $model = new \Smorken\Graphql\Models\Queryable();
        }
        return [
            'model' => $model,
            'factory' => $this->createFactory(),
            'toModel' => new ToModel(),
        ];
    }

    protected function getSut(array $params): Queryable
    {
        return new \Smorken\Graphql\Storage\Queryable(...array_values($params));
    }
}
