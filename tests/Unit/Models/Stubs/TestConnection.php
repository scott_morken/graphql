<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Models\Connection;

class TestConnection extends Connection
{

    protected array $models = [
        self::NODE => TestQueryable::class,
        self::EDGE => \Smorken\Graphql\Models\Queryable::class,
        self::PAGEINFO => \Smorken\Graphql\Models\PageInfo::class,
    ];
}
