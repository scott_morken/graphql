<?php

namespace Smorken\Graphql\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Support\Contracts\Filter;

class Connection extends Queryable implements \Smorken\Graphql\Contracts\Models\Connection
{

    protected $attributes = [
        'totalCount' => 0,
    ];

    /**
     * @var string[]
     */
    protected array $base_fields = [
        'totalCount',
    ];

    /**
     * @var string
     */
    protected string $edgesName = 'edges';

    protected array $models = [
        self::NODE => \Smorken\Graphql\Models\Queryable::class,
        self::EDGE => \Smorken\Graphql\Models\Queryable::class,
        self::PAGEINFO => \Smorken\Graphql\Models\PageInfo::class,
    ];

    /**
     * @var string
     */
    protected string $nodeName = 'node';

    protected array $queries = [
        self::EDGE => null,
        self::NODE => null,
        self::PAGEINFO => null,
    ];

    /**
     * @var \Illuminate\Support\Collection|null
     */
    protected ?Collection $virtualNodes = null;

    public function getModelByKey(string $key): \Smorken\Graphql\Contracts\Models\Queryable
    {
        return $this->getModel($key);
    }

    public function getNodes(): Collection
    {
        return $this->nodes;
    }

    public function getNodesAttribute(): Collection
    {
        if (!$this->virtualNodes) {
            $this->virtualNodes = $this->createNodeCollectionFromRawNodes($this->getRawNodes($this->attributes));
        }
        return $this->virtualNodes;
    }

    public function getPageInfoAttribute(): \Smorken\Graphql\Contracts\Models\PageInfo
    {
        return $this->getModelByKey(self::PAGEINFO)->newInstance($this->attributes['pageInfo'] ?? []);
    }

    public function getQueryByKey(string $key): Query
    {
        if ($key === self::EDGE) {
            $this->ensureEdgeQuery();
        }
        if ($key === self::NODE) {
            $this->ensureNodeQuery();
        }
        if ($key === self::PAGEINFO) {
            $this->ensurePageInfoQuery();
        }
        return $this->queries[$key];
    }

    public function queryAfterCursor(Query $query, string $cursor, bool $isRequestVariable = false): Query
    {
        if ($isRequestVariable) {
            $value = $cursor;
            $cursor = '$after';
        }
        $query->addArgumentFromComponents('after', $cursor, $isRequestVariable);
        if ($isRequestVariable) {
            $query->addRequestVariableFromComponents('after', $value);
        }
        return $query;
    }

    public function queryApplyFilter(
        \Smorken\Graphql\Contracts\Query\Query $query,
        ?Filter $filter
    ): \Smorken\Graphql\Contracts\Query\Query {
        if ($filter) {
            $this->applyFilterTo(self::NODE, $filter);
            $this->applyFilterTo(self::EDGE, $filter);
            $this->applyFilterTo(self::PAGEINFO, $filter);
        }
        return parent::queryApplyFilter($query, $filter);
    }

    public function queryDefaultFields(
        \Smorken\Graphql\Contracts\Query\Query $query
    ): \Smorken\Graphql\Contracts\Query\Query {
        return $query->addFields($this->base_fields)
                     ->addField($this->getQueryByKey(self::PAGEINFO))
                     ->addField($this->getQueryByKey(self::EDGE));
    }

    public function queryFirst(Query $query, int $first): Query
    {
        return $query->addArgumentFromComponents('first', $first, false);
    }

    public function setModelByKey(
        string $key,
        \Smorken\Graphql\Contracts\Models\Queryable $model
    ): \Smorken\Graphql\Contracts\Models\Connection {
        $this->models[$key] = $model;
        return $this;
    }

    protected function applyFilterTo(string $key, Filter $filter): void
    {
        if (method_exists($this->getModelByKey($key), 'queryApplyFilter')) {
            $this->getQueryByKey($key)->applyFilter($filter);
        }
    }

    protected function createNodeCollectionFromRawNodes(array $nodes): Collection
    {
        $coll = new Collection();
        foreach ($nodes as $node) {
            $coll->push($this->newNodeModel($node));
        }
        return $coll;
    }

    protected function ensureEdgeQuery(): void
    {
        if (!$this->hasQueryByKey(self::EDGE)) {
            $this->setQueryByKey(
                self::EDGE,
                $this->getModelByKey(self::EDGE)
                     ->newQueryWithoutDefaults()
                     ->setName($this->edgesName)
                     ->addField(
                         $this->getQueryByKey(self::NODE)
                     )
            );
        }
    }

    protected function ensureNodeQuery(): void
    {
        if (!$this->hasQueryByKey(self::NODE)) {
            $this->setQueryByKey(
                self::NODE,
                $this->getModelByKey(self::NODE)
                     ->newQueryWithoutDefaults()
                     ->setName($this->nodeName)
                     ->defaultFields()
            );
        }
    }

    protected function ensurePageInfoQuery(): void
    {
        if (!$this->hasQueryByKey(self::PAGEINFO)) {
            $this->setQueryByKey(
                self::PAGEINFO,
                $this->getModelByKey(self::PAGEINFO)->newQueryWithoutDefaults()->defaultFields()
            );
        }
    }

    protected function filterAfterCursor(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            $query->afterCursor($value);
        }
        return $query;
    }

    protected function filterFirst(Query $query, Filter $filter, $value): Query
    {
        if (strlen($value)) {
            $query->first($value);
        }
        return $query;
    }

    protected function getModel(string $key): Queryable
    {
        $m = $this->models[$key] ?? null;
        if (is_string($m)) {
            $this->setModelByKey($key, new $m);
        }
        if (is_null($m)) {
            $this->setModelByKey($key, new \Smorken\Graphql\Models\Queryable());
        }
        return $this->models[$key];
    }

    protected function getRawNodes(array $items): array
    {
        $nodes = [];
        if ($items) {
            foreach ($items as $k => $v) {
                if ($k === $this->nodeName) {
                    $nodes[] = $v;
                } elseif ($k === Str::plural($this->nodeName)) {
                    $nodes = array_merge($v);
                } else {
                    if (is_array($v)) {
                        $nodes = array_merge($nodes, $this->getRawNodes($v));
                    }
                }
            }
        }
        return $nodes;
    }

    protected function hasQueryByKey(string $key): bool
    {
        return !is_null($this->queries[$key] ?? null);
    }

    protected function newNodeModel(array $attributes): Model
    {
        return $this->getModelByKey(self::NODE)->newInstance($attributes);
    }

    protected function setQueryByKey(string $key, Query $query): \Smorken\Graphql\Contracts\Models\Connection
    {
        $this->queries[$key] = $query;
        return $this;
    }
}
