<?php

namespace Smorken\Graphql\Contracts\Models;

use Smorken\Graphql\Contracts\Query\Query;

interface Queryable extends MutateAndQuery
{

    /**
     * @return Query
     */
    public function newQuery(): Query;

    /**
     * @return Query
     */
    public function newQueryWithoutDefaults(): Query;

    public function newQueryEmpty(): Query;
}
