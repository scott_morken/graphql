<?php

namespace Smorken\Graphql\Models;

class Enum extends Queryable implements \Smorken\Graphql\Contracts\Models\Enum
{

    /**
     * @var bool
     */
    protected bool $canMulti = false;

    protected array $enumValues = ['name'];

    public function __toString()
    {
        return $this->getAttribute('name');
    }

    public function queryDefaultFields(
        \Smorken\Graphql\Contracts\Query\Query $query
    ): \Smorken\Graphql\Contracts\Query\Query {
        return $query->addField($this->newQueryEmpty()->setName('enumValues')->addFields($this->enumValues));
    }

    protected function defaultNewQuery(
        bool $addDefaults
    ): \Smorken\Graphql\Contracts\Query\Query {
        $q = parent::defaultNewQuery($addDefaults);
        return $q->setName('__type')
                 ->addArgumentFromComponents('name', $this->getName(), false);
    }
}
