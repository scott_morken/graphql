<?php

namespace Tests\Smorken\Graphql\Unit\PhpGraphQL;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\PhpGraphQL\Variable;

class VariableTest extends TestCase
{

    public function testCreate(): void
    {
        $sut = (new Variable())->newInstance('var', 'String');
        $this->assertEquals('$var: String', (string) $sut);
    }

    public function testCreateOptionalWithBooleanDefault(): void
    {
        $sut = (new Variable())->newInstance('var', 'Boolean', false, true);
        $this->assertEquals('$var: Boolean=true', (string) $sut);
    }

    public function testCreateOptionalWithIntDefault(): void
    {
        $sut = (new Variable())->newInstance('var', 'Int', false, 42);
        $this->assertEquals('$var: Int=42', (string) $sut);
    }

    public function testCreateOptionalWithIntegerAsStringDefault(): void
    {
        $sut = (new Variable())->newInstance('var', 'String', false, '42');
        $this->assertEquals('$var: String="42"', (string) $sut);
    }

    public function testCreateOptionalWithStringDefault(): void
    {
        $sut = (new Variable())->newInstance('var', 'String', false, 'foo');
        $this->assertEquals('$var: String="foo"', (string) $sut);
    }

    public function testCreateRequired(): void
    {
        $sut = (new Variable())->newInstance('var', 'String', true);
        $this->assertEquals('$var: String!', (string) $sut);
    }

    public function testCreateRequiredWithDefaultDoesNothing(): void
    {
        $sut = (new Variable())->newInstance('var', 'String', true, 'foo');
        $this->assertEquals('$var: String!', (string) $sut);
    }
}
