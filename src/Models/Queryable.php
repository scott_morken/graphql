<?php

namespace Smorken\Graphql\Models;

use Smorken\Graphql\Contracts\Query\Query;

class Queryable extends Model implements \Smorken\Graphql\Contracts\Models\Queryable
{

    use \Smorken\Graphql\Models\Traits\Query;

    public function queryIdIs(Query $query, string $id): Query
    {
        return $query->addVariableFromComponents(
            $this->primaryKey,
            ucfirst($this->keyType),
            true
        )->addArgumentAsVariable($this->primaryKey)
                     ->addRequestVariableFromComponents($this->primaryKey, $id);
    }
}
