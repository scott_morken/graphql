<?php

namespace Smorken\Graphql\Contracts\Models;

use Smorken\Graphql\Contracts\Query\Query;

interface Mutatable extends MutateAndQuery
{

    public function getValidationRules(array $overrides = []): array;

    /**
     * @return \Smorken\Graphql\Contracts\Query\Query
     */
    public function newMutation(): Query;

    public function newMutationEmpty(): Query;

    /**
     * @return \Smorken\Graphql\Contracts\Query\Query
     */
    public function newMutationWithoutDefaults(): Query;
}
