<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;
use Smorken\Support\Contracts\Filter;

class User extends Queryable
{

    use FakeRelations;

    protected array $fields = [
        'id',
        'firstName',
        'lastName',
        'email',
    ];

    public function languages()
    {
        return $this->fakeHasManyViaCallback(
            Language::class,
            function (FakeRelation $relation, User $user, Collection $collection) {
                $lps = $this->getAttributeFromArray('languages');
                if ($lps && count($lps)) {
                    foreach ($lps as $lp) {
                        $m = $relation->getRelated()->newInstance($lp);
                        $collection->push($m);
                    }
                }
            }
        );
    }

    public function queryLanguages(Query $query, array $fields = []): Query
    {
        $q = $this->languages()->getRelated()->newInstance()->newQuery()->setMulti(true);
        if ($fields) {
            $q->addFields($fields);
        } else {
            $q->defaultFields();
        }
        return $query->addfield($q);
    }

    protected function filterLanguages(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            $query->languages(is_array($value) ? $value : []);
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_languages' => 'filterLanguages',
        ];
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        $group = (new Group())->newQueryWithoutDefaults();
        $org = (new MyOrg())->newQueryWithoutDefaults()->addField($group);
        return $group;
    }
}
