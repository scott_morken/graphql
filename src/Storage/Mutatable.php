<?php

namespace Smorken\Graphql\Storage;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Models\Input;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Models\MutateAndQuery;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Support\Arr;
use Smorken\Support\Contracts\Filter;

class Mutatable extends Queryable implements \Smorken\Graphql\Contracts\Storage\Mutatable
{

    protected int $counter = 0;

    public function manyMutations(array $models, int $per_transaction = 100): Collection
    {
        $collection = new Collection();
        Arr::chunk($models, $this->handleManyChunk($collection), $per_transaction);
        return $collection;
    }

    public function oneMutation(\Smorken\Graphql\Contracts\Models\Mutatable $model): ?Model
    {
        $query = $this->makeBaseMutation($model);
        $this->applyDefaultVariables($model, $query);
        $fromClient = $this->fromClientFromQuery($query);
        return $this->modelFromFromClient($fromClient, $this->getFromClientMutationModel($this->getModel()));
    }

    protected function applyDefaultVariables(
        \Smorken\Graphql\Contracts\Models\Mutatable $model,
        Query $query,
        array $overrides = []
    ): void {
        if (method_exists($model, 'queryDefaultVariables')) {
            $query->defaultVariables($overrides);
        }
    }

    protected function getFromClientMutationModel(MutateAndQuery $model): Model
    {
        if ($model instanceof Input) {
            return $model->getPayloadModel();
        }
        return $model;
    }

    protected function handleManyChunk(Collection $collection): callable
    {
        return function (array $models) use ($collection) {
            $first = Arr::first($models);
            $baseQuery = $first->newMutationEmpty();
            foreach ($models as $model) {
                $subQuery = $this->makeBaseMutation($model, $this->counter);
                $this->applyDefaultVariables($model, $subQuery);
                $baseQuery->addField($subQuery);
                $this->counter++;
            }
            $fromClient = $this->fromClientFromQuery($baseQuery);
            $resultCollection = $this->collectionFromFromClient($fromClient);
            $this->mergeCollectionInto($collection, $resultCollection);
        };
    }

    protected function makeBaseMutation(
        \Smorken\Graphql\Contracts\Models\Mutatable $model,
        ?string $alias = null,
        ?Filter $filter = null
    ): Query {
        $q = $model->newMutation()
                   ->setIndex($alias);
        if (!is_null($alias)) {
            $q->setUseIndex(true);
        }
        $this->applyDefaultFields($model, $q);
        $this->applyFilter($model, $q, $filter);
        return $q;
    }

    protected function mergeCollectionInto(Collection $into, Collection $from): void
    {
        foreach ($from as $k => $v) {
            if (is_string($k)) {
                $into->put($k, $v);
            } else {
                $into->push($v);
            }
        }
    }
}
