<?php

namespace Smorken\Graphql\Query;

use Illuminate\Support\Str;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Query\Argument;
use Smorken\Graphql\Contracts\Query\RequestVariable;
use Smorken\Graphql\Contracts\Query\Variable;
use Smorken\Graphql\Index;

abstract class Query implements \Smorken\Graphql\Contracts\Query\Query
{

    /**
     * @var \Smorken\Graphql\Contracts\Query\Argument|null
     */
    protected static ?Argument $argument = null;

    /**
     * @var \Smorken\Graphql\Contracts\Query\RequestVariable|null
     */
    protected static ?RequestVariable $requestVariable = null;

    /**
     * @var Variable|null
     */
    protected static ?Variable $variable = null;

    protected array $arguments = [];

    protected bool $built = false;

    protected array $callQueue = [];

    protected array $fields = [];

    protected ?string $index = null;

    /**
     * @var Model|null
     */
    protected ?Model $model = null;

    protected bool $multi = false;

    protected ?string $name = null;

    protected ?string $operationName = null;

    /**
     * @var \Smorken\Graphql\Contracts\Query\Query|null
     */
    protected ?\Smorken\Graphql\Contracts\Query\Query $parent = null;

    /**
     * @var mixed
     */
    protected mixed $queryObject = null;

    protected string $queryType = self::QUERY;

    protected ?string $rawString = null;

    protected array $requestVariables = [];

    protected bool $useIndex = false;

    protected array $variables = [];

    abstract protected function applyArguments(): void;

    abstract protected function applyFields(): void;

    abstract protected function applyName(): void;

    abstract protected function applyOperationName(): void;

    abstract protected function applyVariables(): void;

    abstract public function getQueryObject(string $name = ''): mixed;

    /**
     * @param  object  $queryObject
     * @return \Smorken\Graphql\Query\Query
     */
    public function setQueryObject(object $queryObject): self
    {
        $this->queryObject = $queryObject;
        return $this;
    }

    public static function setArgumentProvider(Argument $argument): void
    {
        self::$argument = $argument;
    }

    public static function setRequestVariableProvider(RequestVariable $requestVariable): void
    {
        self::$requestVariable = $requestVariable;
    }

    public static function setVariableProvider(Variable $variable): void
    {
        self::$variable = $variable;
    }

    /**
     * Call ::query[Name] on the model
     *
     * @param  string  $name
     * @param  array  $arguments
     * @return \Smorken\Graphql\Contracts\Query\Query
     */
    public function __call(string $name, array $arguments): self
    {
        $method = $this->getMethodNameForCallQueue($name);
        if ($this->model && $this->queryObject && method_exists($this->model, $method)) {
            call_user_func([$this->model, $method], $this, ...$arguments);
        } else {
            $this->addToQueue($name, $arguments);
        }
        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->get();
    }

    public function addArgument(
        Argument $argument,
        ?string $index = null
    ): self {
        if ($argument->isRequestArgument()) {
            $argument->setValue(Index::apply($argument->getValue(), $this->getIndex($index)));
        }
        $this->arguments[] = $argument;
        $this->built = false;
        return $this;
    }

    public function addArgumentAsVariable(string $key, ?string $index = null): self
    {
        $argument = $this->getArgumentProviderInstance()->newInstance($key, null);
        $argument->setValue($key, true);
        $this->addArgument($argument, $index);
        return $this;
    }

    public function addArgumentFromComponents(
        string $key,
        $value,
        bool $isRequestArgument,
        ?string $index = null
    ): self {
        $argument = $this->getArgumentProviderInstance()->newInstance($key, $value);
        $argument->setValue($value, $isRequestArgument);
        $this->addArgument($argument, $index);
        return $this;
    }

    public function addArguments(array $arguments, ?string $index = null): self
    {
        foreach ($arguments as $argument) {
            $check = false;
            if (is_array($argument)) {
                $check = true;
                $name = $argument['name'] ?? null;
                $value = $argument['value'] ?? null;
                $isRequest = $argument['isRequestArgument'] ?? true;
                $this->addArgumentFromComponents($name, $value, $isRequest, $index);
            }
            if ($argument instanceof Argument) {
                $check = true;
                $this->addArgument($argument, $index);
            }
            if (!$check) {
                throw new Exception("Argument could not be created.");
            }
        }
        return $this;
    }

    public function addField($field): self
    {
        if ($this->canAddField($field)) {
            $this->ensureQueryFieldHasParent($field);
            $this->fields[] = $field;
            $this->built = false;
        }
        return $this;
    }

    public function addFields(array $fields): self
    {
        foreach ($fields as $field) {
            $this->addField($field);
        }
        return $this;
    }

    public function addRequestVariable(
        RequestVariable $requestVariable,
        ?string $index = null
    ): self {
        $requestVariable->setName(Index::apply($requestVariable->getName(), $this->getIndex($index)));
        $this->requestVariables[] = $requestVariable;
        $this->built = false;
        return $this;
    }

    public function addRequestVariableFromComponents(
        string $key,
        $value,
        ?string $index = null
    ): self {
        $this->addRequestVariable($this->getRequestVariableProviderInstance()->newInstance($key, $value), $index);
        $this->built = false;
        return $this;
    }

    public function addRequestVariables(array $variables, ?string $index = null): self
    {
        foreach ($variables as $k => $v) {
            if ($v instanceof RequestVariable) {
                $this->addRequestVariable($v, $index);
            } else {
                $this->addRequestVariableFromComponents($k, $v, $index);
            }
        }
        return $this;
    }

    public function addToQueue(string $name, array $args): self
    {
        $name = $this->getMethodNameForCallQueue($name);
        $this->callQueue[$name] = $args;
        $this->built = false;
        return $this;
    }

    public function addVariable(Variable $variable, ?string $index = null): self
    {
        $variable->setName(Index::apply($variable->getName(), $this->getIndex($index)));
        $this->variables[] = $variable;
        $this->built = false;
        return $this;
    }

    public function addVariableFromComponents(
        string $name,
        string $type = 'String',
        bool $required = true,
        $default = null,
        ?string $index = null
    ): self {
        $this->addVariable(
            $this->getVariableProviderInstance()
                 ->newInstance($name, $type, $required, $default),
            $index
        );
        return $this;
    }

    public function addVariables(array $variables, ?string $index = null): self
    {
        foreach ($variables as $variable) {
            $check = false;
            if (is_array($variable)) {
                $check = true;
                $name = $variable['name'] ?? null;
                $type = $variable['type'] ?? 'String';
                $required = $variable['required'] ?? true;
                $default = $variable['default'] ?? null;
                $this->addVariableFromComponents($name, $type, $required, $default, $index);
            }
            if ($variable instanceof Variable) {
                $check = true;
                $this->addVariable($variable, $index);
            }
            if (!$check) {
                throw new Exception("Variable could not be created.");
            }
        }
        return $this;
    }

    public function build(): void
    {
        if (!$this->built) {
            $this->applyName();
            $this->executeCallQueue();
            $this->applyOperationName();
            $this->applyFields();
            $this->applyVariables();
            $this->applyArguments();
            $this->buildSubQueries();
            $this->built = true;
        }
    }

    public function compareVariables(int $count = 0): bool
    {
        if ($count && $count !== count($this->getVariables())) {
            return false;
        }
        return true;
    }

    public function fromString(string $query): \Smorken\Graphql\Contracts\Query\Query
    {
        $this->rawString = $query;
        return $this;
    }

    public function get(): mixed
    {
        $this->build();
        if ($this->rawString) {
            return $this->handleRawString();
        }
        return $this->getQueryObject();
    }

    public function getAllRequestVariables(): array
    {
        $this->build();
        $vars = $this->requestVariables;
        return $this->getRequestVariablesFromFields($this->fields, $vars);
    }

    public function getAllRequestVariablesAsArray(): array
    {
        return $this->convertRequestVariablesToArray($this->getAllRequestVariables());
    }

    public function getAllVariables(): array
    {
        $vars = $this->variables;
        return $this->getVariablesFromFields($this->fields, $vars);
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function getBaseQuery(bool $build = false): self
    {
        if ($build) {
            $this->build();
        }
        if ($this->getParent()) {
            return $this->getParent()->getBaseQuery($build);
        }
        return $this;
    }

    public function getCallStack(): array
    {
        return $this->callQueue;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function getIndex(?string $index = null): ?string
    {
        if (!is_null($index)) {
            return $index;
        }
        return $this->getUseIndex() ? $this->index : null;
    }

    public function setIndex(?string $index): self
    {
        $this->setUseIndex(!is_null($index));
        $this->index = $index;
        return $this;
    }

    public function getName(): string
    {
        $name = $this->name;
        if (is_null($name)) {
            if ($this->model) {
                $name = $this->model->getQueryName($this->multi);
            }
        }
        return Index::index($name, $this->getIndex());
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getOperationName(): string
    {
        return $this->operationName ?? '';
    }

    public function setOperationName(string $name): self
    {
        $this->operationName = $name;
        return $this;
    }

    public function getParent(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        return $this->parent;
    }

    public function setParent(\Smorken\Graphql\Contracts\Query\Query $query): self
    {
        $this->parent = $query;
        return $this;
    }

    public function getRequestVariables(): array
    {
        return $this->requestVariables;
    }

    public function getRequestVariablesAsArray(): array
    {
        return $this->convertRequestVariablesToArray($this->getRequestVariables());
    }

    public function getUseIndex(): bool
    {
        return $this->useIndex;
    }

    public function setUseIndex(bool $use): self
    {
        $this->useIndex = $use;
        return $this;
    }

    public function getVariables(): array
    {
        if ($this->getParent()) {
            return [];
        }
        return $this->getAllVariables();
    }

    public function newFromSelf(
        bool $useModel = true,
        bool $useQueryObject = false
    ): static {
        $i = new static();
        if ($useModel && $this->model) {
            $i->setModel($this->model);
        }
        if ($useQueryObject) {
            $i->setQueryObject($this->getQueryObject());
        }
        return $i;
    }

    public function newInstance(?Model $model, ?object $queryObject = null): static
    {
        $i = new static();
        if ($model) {
            $i->setModel($model);
        }
        if ($queryObject) {
            $i->setQueryObject($queryObject);
        }
        return $i;
    }

    public function setModel(Model $model): self
    {
        $this->model = $model;
        return $this;
    }

    public function setMulti(bool $multi = false): self
    {
        $this->multi = $multi;
        return $this;
    }

    public function setQueryType(string $type): self
    {
        $this->queryType = $type;
        return $this;
    }

    protected function buildSubQueries(): void
    {
        foreach ($this->getFields() as $field) {
            if ($this->fieldIsQuery($field)) {
                $field->build();
            }
        }
    }

    protected function canAddField($field): bool
    {
        if (is_string($field)) {
            return !in_array($field, $this->fields);
        }
        return true;
    }

    protected function convertRequestVariablesToArray(array $requestVariables): array
    {
        $vars = [];
        foreach ($requestVariables as $requestVariable) {
            $vars[$requestVariable->getName()] = $requestVariable->getValue();
        }
        return $vars;
    }

    protected function ensureFields(array $fields): array
    {
        foreach ($fields as $i => $field) {
            if ($this->fieldIsQuery($field)) {
                $fields[$i] = $field->get();
            }
        }
        return $fields;
    }

    protected function ensureQueryFieldHasParent($field): void
    {
        if ($field instanceof \Smorken\Graphql\Contracts\Query\Query) {
            $field->setParent($this);
        }
    }

    protected function executeCallQueue(): void
    {
        if ($this->model) {
            foreach ($this->getCallStack() as $method => $args) {
                if (method_exists($this->model, $method)) {
                    call_user_func([$this->model, $method], $this, ...array_values($args));
                } else {
                    throw new Exception("Model does not contain $method.");
                }
                unset($this->getCallStack()[$method]);
            }
        }
    }

    protected function fieldIsQuery($field): bool
    {
        return $field instanceof \Smorken\Graphql\Contracts\Query\Query;
    }

    protected function getArgumentProviderInstance(): Argument
    {
        if (!self::$argument) {
            self::setArgumentProvider(new \Smorken\Graphql\PhpGraphQL\Argument());
        }
        return self::$argument;
    }

    protected function getMethodNameForCallQueue(string $name): string
    {
        if (!str_contains($name, 'query')) {
            $name = sprintf('query%s', Str::studly($name));
        }
        return $name;
    }

    protected function getRequestVariableProviderInstance(): RequestVariable
    {
        if (!self::$requestVariable) {
            self::setRequestVariableProvider(new \Smorken\Graphql\Query\RequestVariable());
        }
        return self::$requestVariable;
    }

    protected function getRequestVariablesFromFields(array $fields, array $variables): array
    {
        if ($fields) {
            foreach ($fields as $field) {
                if ($this->fieldIsQuery($field)) {
                    $variables = array_merge($variables, $field->getAllRequestVariables());
                } else {
                    if (is_array($field)) {
                        $variables = $this->getRequestVariablesFromFields($field, $variables);
                    }
                }
            }
        }
        return $variables;
    }

    protected function getVariableProviderInstance(): Variable
    {
        if (!self::$variable) {
            self::setVariableProvider(new \Smorken\Graphql\PhpGraphQL\Variable());
        }
        return self::$variable;
    }

    protected function getVariablesFromFields(array $fields, array $variables): array
    {
        if ($fields) {
            foreach ($fields as $field) {
                if ($this->fieldIsQuery($field)) {
                    $field->build();
                    $variables = array_merge($variables, $field->getAllVariables());
                } else {
                    if (is_array($field)) {
                        $variables = $this->getVariablesFromFields($field, $variables);
                    }
                }
            }
        }
        return $variables;
    }

    protected function handleRawString(): string
    {
        return $this->rawString;
    }
}
