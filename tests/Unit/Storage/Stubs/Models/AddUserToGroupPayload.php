<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Payload;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;

class AddUserToGroupPayload extends Payload
{

    use FakeRelations;

    protected array $fields = ['clientMutationId'];

    public function user()
    {
        return $this->fakeBelongsToViaCallback(
            User::class,
            function (FakeRelation $relation, AddUserToGroupPayload $model) {
                $g = $model->getAttributeFromArray('user');
                if ($g) {
                    return $relation->getRelated()->newInstance($g);
                }
                return null;
            }
        );
    }

    protected function addToFields(array $fields): array
    {
        $fields[] = $this->newMutationWithoutDefaults()
                         ->setName('user')
                         ->addField('id');
        return $fields;
    }
}
