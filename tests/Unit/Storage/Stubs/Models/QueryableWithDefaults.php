<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;

class QueryableWithDefaults extends Queryable
{

    protected $attributes = [
        'id' => null,
        'name' => null,
        'foo' => null,
    ];

    protected $request_variables = [
        'id' => [
            'type' => 'Int',
            'required' => true,
        ],
    ];

    public function queryDefaultFields(Query $query): Query
    {
        return parent::queryDefaultFields($query)->addField($this->newQuery()->setName('fizz')->addField('buzz'));
    }
}
