<?php

namespace Smorken\Graphql\PhpGraphQL;

use GraphQL\Mutation;
use Smorken\Graphql\Contracts\Models\Mutatable;
use Smorken\Graphql\Contracts\Models\Queryable;

class Query extends \Smorken\Graphql\Query\Query implements \Smorken\Graphql\Contracts\Query\Query
{

    public function getQueryObject(string $name = ''): mixed
    {
        if (!$this->queryObject) {
            if ($this->queryType === self::MUTATION || ($this->model instanceof Mutatable)) {
                $this->queryObject = new Mutation($name);
            }
            if ($this->queryType === self::QUERY || ($this->model instanceof Queryable)) {
                $this->queryObject = new \GraphQL\Query($name);
            }
        }
        return $this->queryObject;
    }

    protected function applyArguments(): void
    {
        if ($this->getArguments()) {
            $args = [];
            foreach ($this->getArguments() as $argument) {
                $args[$argument->getName()] = $argument->getValue();
            }
            $this->getQueryObject()->setArguments($args);
        }
    }

    protected function applyFields(): void
    {
        if ($this->getFields()) {
            $this->getQueryObject()->setSelectionSet($this->ensureFields($this->getFields()));
        }
    }

    protected function applyName(): void
    {
        $this->getQueryObject($this->getName());
    }

    protected function applyOperationName(): void
    {
        if (!is_null($this->operationName)) {
            $this->getQueryObject()->setOperationName($this->getOperationName());
        }
    }

    protected function applyVariables(): void
    {
        if ($this->getVariables()) {
            $this->getQueryObject()->setVariables($this->convertVariables());
        }
    }

    protected function convertVariables(): array
    {
        $vars = [];
        foreach ($this->getVariables() as $variable) {
            $vars[] = $variable->get();
        }
        return $vars;
    }
}
