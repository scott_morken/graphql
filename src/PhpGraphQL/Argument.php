<?php

namespace Smorken\Graphql\PhpGraphQL;

use GraphQL\Util\StringLiteralFormatter;

class Argument extends \Smorken\Graphql\Query\Argument implements \Smorken\Graphql\Contracts\Query\Argument
{

    public function __toString(): string
    {
        return sprintf('%s: %s', $this->getName(), StringLiteralFormatter::formatValueForRHS($this->getValue()));
    }

    public function get(): mixed
    {
        return (string) $this;
    }
}
