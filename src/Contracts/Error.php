<?php

namespace Smorken\Graphql\Contracts;

interface Error
{

    public function __toString(): string;

    public function getByKey(string $key): ?string;

    public function getMessage(): ?string;

    public function getPath(): ?string;

    public function getStatusCode(): int;

    public function getUrl(): ?string;

    public function setStatusCode(int $statusCode): void;

    public function toRawArray(): array;
}
