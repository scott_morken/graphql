<?php

namespace Smorken\Graphql\Query;

abstract class Variable implements \Smorken\Graphql\Contracts\Query\Variable
{
    /**
     * @var null|string|int|float|bool
     */
    protected mixed $defaultValue = null;

    /**
     * @var bool
     */
    protected bool $isRequired = false;

    /**
     * @var string|null
     */
    protected ?string $name = null;

    /**
     * @var string
     */
    protected string $type = 'String';

    public function getDefaultValue(): mixed
    {
        return $this->defaultValue;
    }

    public function setDefaultValue($value): self
    {
        $this->defaultValue = $value;
        return $this;
    }

    public function getIsRequired(): bool
    {
        return $this->isRequired;
    }

    public function setIsRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function newInstance(
        string $name,
        string $type,
        bool $isRequired = false,
        $defaultValue = null
    ): static {
        $i = new static();
        return $i->setName($name)
                 ->setType($type)
                 ->setIsRequired($isRequired)
                 ->setDefaultValue($defaultValue);
    }
}
