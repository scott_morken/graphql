<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Graphql\Unit\Models\Stubs\TestConnection;

class ConnectionTest extends TestCase
{

    public function testQuery(): void
    {
        $sut = new TestConnection();
        $expected = 'query {
testConnection {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}';
        $this->assertEquals($expected, (string) $sut->newQuery()->defaultFields());
    }

    public function testQueryWithAfterCursorAsLiteral(): void
    {
        $sut = new TestConnection();
        $expected = 'query {
testConnection(after: "abcdef") {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}';
        $this->assertEquals($expected, (string) $sut->newQuery()->defaultFields()->afterCursor('abcdef'));
    }

    public function testQueryWithAfterCursorAsVariable(): void
    {
        $sut = new TestConnection();
        $expected = 'query {
testConnection(after: $after) {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}';
        $q = $sut->newQuery()->defaultFields()->afterCursor('abcdef', true);
        $this->assertEquals($expected, (string) $q);
        $this->assertEquals(['after' => 'abcdef'], $q->getAllRequestVariablesAsArray());
    }

    public function testQueryWithFirst(): void
    {
        $sut = new TestConnection();
        $expected = 'query {
testConnection(first: 100) {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
name
}
}
}
}';
        $this->assertEquals($expected, (string) $sut->newQuery()->defaultFields()->first(100));
    }
}
