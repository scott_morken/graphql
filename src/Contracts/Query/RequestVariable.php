<?php

namespace Smorken\Graphql\Contracts\Query;

interface RequestVariable
{

    public function toArray(): array;

    /**
     * Get the proxied item
     *
     * @return mixed
     */
    public function get(): mixed;

    public function getName(): string;

    public function getValue(): mixed;

    public function newInstance(string $name, $value): RequestVariable;

    public function setName(string $name): self;

    public function setValue($value): self;
}
