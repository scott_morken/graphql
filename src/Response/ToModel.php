<?php

namespace Smorken\Graphql\Response;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Models\Connection;
use Smorken\Graphql\Contracts\Models\Input;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Response\FromClient;

class ToModel implements \Smorken\Graphql\Contracts\Response\ToModel
{

    protected int $counter = 0;

    /**
     * @var Model|null
     */
    protected ?Model $model = null;

    public function __construct(?Model $model = null)
    {
        if (!is_null($model)) {
            $this->setModel($model);
        }
    }

    public function connection(FromClient $response): ?Connection
    {
        return $this->connections($response)->first();
    }

    public function connectionFromIterable(iterable $items): ?Connection
    {
        return $this->connectionsFromIterable($items)->first();
    }

    public function connections(FromClient $response): Collection
    {
        $items = $this->getDataFromResponse($response);
        return $this->connectionsFromIterable($items ?? []);
    }

    public function connectionsFromIterable(iterable $items): Collection
    {
        $coll = new Collection();
        $data = $this->getDataFromDataArray($items);
        if ($data) {
            foreach ($data as $key => $item) {
                $connection = $this->getConnectionModel($item);
                $this->addItemToCollection($connection, $key, $coll);
            }
        } else {
            $coll->push($this->getConnectionModel([]));
        }
        return $coll;
    }

    public function many(FromClient $response): Collection
    {
        $items = $this->getDataFromResponse($response);
        return $this->manyFromIterable($items ?? []);
    }

    public function manyFromIterable(iterable $items): Collection
    {
        $coll = new Collection();
        $data = $this->getDataFromDataArray($items);
        foreach ($data as $key => $item) {
            $this->addItemToCollection($this->modelFromData($item), $key, $coll);
        }
        return $coll;
    }

    public function one(FromClient $response): ?Model
    {
        return $this->many($response)->first();
    }

    public function oneFromIterable(iterable $items): Collection
    {
        return $this->manyFromIterable($items)->first();
    }

    protected function addItemToCollection($item, $key, Collection $collection): void
    {
        if ($item) {
            if ($key && is_string($key)) {
                $collection->put($key, $item);
            } else {
                $collection->push($item);
            }
        }
    }

    protected function addItemToDataArray(array $existing, array $item, $item_key, $previous_key): array
    {
        $key = $this->getItemKey($existing, $item_key, $previous_key);
        if (!is_null($key)) {
            $existing[$key] = $item;
        } else {
            $existing[] = $item;
        }
        return $existing;
    }

    protected function addItemsToDataArray(array $existing, array $newItems, $previous_key): array
    {
        if ($newItems) {
            foreach ($newItems as $k => $newItem) {
                $existing = $this->addItemToDataArray($existing, $newItem, $k, $previous_key);
            }
        }
        return $existing;
    }

    protected function getConnectionModel($item): Connection
    {
        if ($this->getModel() instanceof Connection) {
            return $this->getModel()->newInstance($item);
        }
        return new \Smorken\Graphql\Models\Connection($item);
    }

    protected function getDataFromDataArray(iterable $data, ?string $last_key = null): ?array
    {
        $items = [];
        if (!$data) {
            return $items;
        }
        foreach ($data as $k => $v) {
            if ($this->looksLikeModel($v)) {
                $items = $this->addItemToDataArray($items, $v, $k, $last_key);
            } else {
                if (is_array($v)) {
                    $items = $this->addItemsToDataArray($items, $this->getDataFromDataArray($v, $k), $k);
                }
            }
        }
        return $items;
    }

    protected function getDataFromResponse(FromClient $response): ?array
    {
        return $response->getData();
    }

    protected function getItemKey(array $existing, $item_key, $previous_key): ?string
    {
        $key = null;
        if ($previous_key && is_string($previous_key)) {
            if (is_int($item_key)) {
                $key = $previous_key.'_'.$item_key;
                if ($this->shouldIncrementKey($key, $existing)) {
                    $key = $this->incrementKey($key);
                }
            }
        }
        if (is_null($key) && is_string($item_key)) {
            $key = $item_key;
            if ($this->shouldIncrementKey($key, $existing)) {
                $key = $this->incrementKey($key);
            }
        }
        return $key;
    }

    protected function getModel(): Model
    {
        if (!$this->model) {
            $this->model = new \Smorken\Graphql\Models\Model();
        }
        return $this->model;
    }

    public function setModel(Model $model): self
    {
        $this->model = $model;
        return $this;
    }

    protected function incrementKey(string $key): string
    {
        $key = $key.'_'.$this->counter;
        $this->counter++;
        return $key;
    }

    protected function looksLikeModel($item): bool
    {
        $string_keys = false;
        $non_array_attrs = 0;
        if (is_array($item)) {
            foreach ($item as $k => $v) {
                if (is_string($k)) {
                    $string_keys = true;
                }
                if (!is_array($v) && !empty($v)) {
                    $non_array_attrs++;
                }
                if ($string_keys && $non_array_attrs > 0) {
                    return true;
                }
            }
        }
        return $string_keys && $non_array_attrs > 0;
    }

    protected function modelFromData($item): ?Model
    {
        return $this->modelFromDataAndModel($item, $this->getModel());
    }

    protected function modelFromDataAndModel($data, Model $model): ?Model
    {
        if (!$data) {
            return null;
        }
        if ($this->getModel() instanceof Input) {
            $m = $model->getPayloadModel()->newInstance($data);
        } else {
            $m = $model->newInstance($data);
        }
        $m->applyData($data);
        return $m;
    }

    protected function shouldIncrementKey(string $key, array $existing): bool
    {
        return isset($existing[$key]);
    }
}
