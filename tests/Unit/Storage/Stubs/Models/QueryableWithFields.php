<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Queryable;

class QueryableWithFields extends Queryable
{

    protected $attributes = [
        'id' => null,
        'name' => null,
        'foo' => null,
    ];
}
