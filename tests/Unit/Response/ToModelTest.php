<?php

namespace Tests\Smorken\Graphql\Unit\Response;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Contracts\Models\Payload;
use Smorken\Graphql\Models\Input;
use Smorken\Graphql\Response\FromClient;
use Smorken\Graphql\Response\ToModel;
use Tests\Smorken\Graphql\Traits\Response;

class ToModelTest extends TestCase
{

    use Response;

    protected $base_path = 'Response/ToModelTest/';

    public function testGroupsGetByIdInCollection(): void
    {
        $sut = new ToModel();
        $response = $this->makeResponse('Groups.getByIdIn.json');
        $coll = $sut->many(new FromClient($response));
        $this->assertCount(2, $coll);
        $this->assertEquals('group-id-0', $coll['index_0_0']->id);
        $this->assertEquals('group-id-1', $coll['index_1_0']->id);
    }

    public function testManyWithEmptyResponseIsEmptyCollection(): void
    {
        $sut = new ToModel();
        $this->assertEmpty($sut->many(new FromClient()));
    }

    public function testManyWithValidResponseByDefaultKey(): void
    {
        $sut = new ToModel();
        $response = $this->makeResponse('many.model.json');
        $coll = $sut->many(new FromClient($response));
        $this->assertEquals('some-id-1', $coll['models_0']->id);
        $this->assertEquals('Model 1', $coll['models_0']->name);
        $this->assertEquals('some-id-2', $coll['models_1']->id);
        $this->assertEquals('Model 2', $coll['models_1']->name);
    }

    public function testManyWithValidResponseWithNestedKeyCollapsesData(): void
    {
        $sut = new ToModel();
        $response = $this->makeResponse('many.nestedKey.json');
        $coll = $sut->many(new FromClient($response));
        $this->assertEquals('some-id-1', $coll['nestedKey_0']->id);
        $this->assertEquals('Model 1', $coll['nestedKey_0']->name);
        $this->assertEquals('some-id-2', $coll['nestedKey_1']->id);
        $this->assertEquals('Model 2', $coll['nestedKey_1']->name);
    }

    public function testOneWithEmptyResponseIsNull(): void
    {
        $sut = new ToModel();
        $this->assertNull($sut->one(new FromClient()));
    }

    public function testOneWithValidResponseByDefaultModelKey(): void
    {
        $sut = new ToModel();
        $response = $this->makeResponse('one.model.json');
        $m = $sut->one(new FromClient($response));
        $this->assertEquals('some-id', $m->id);
        $this->assertEquals('One Model', $m->name);
    }

    public function testOneWithValidResponseCanReturnPayload(): void
    {
        $sut = new ToModel();
        $sut->setModel(new Input());
        $response = $this->makeResponse('one.model.json');
        $m = $sut->one(new FromClient($response));
        $this->assertInstanceOf(Payload::class, $m);
        $this->assertEquals('some-id', $m->id);
        $this->assertEquals('One Model', $m->name);
    }

    public function testOneWithValidResponseSetResponseKey(): void
    {
        $sut = new ToModel();
        $response = $this->makeResponse('one.oneThing.json');
        $m = $sut->one(new FromClient($response));
        $this->assertEquals('some-id', $m->id);
        $this->assertEquals('One Thing', $m->name);
    }

    public function testOneWithValidResponseSetResponseKeys(): void
    {
        $sut = new ToModel();
        $response = $this->makeResponse('one.oneThing.json');
        $m = $sut->one(new FromClient($response));
        $this->assertEquals('some-id', $m->id);
        $this->assertEquals('One Thing', $m->name);
    }
}
