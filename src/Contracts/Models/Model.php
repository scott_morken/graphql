<?php

namespace Smorken\Graphql\Contracts\Models;

interface Model extends \Smorken\Model\Contracts\VO\Model
{

    public function applyData(mixed $data): void;

    public function getName(): string;
}
