<?php

namespace Smorken\Graphql\Authenticate;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Smorken\Graphql\Contracts\Clients\Client;

class BearerToken extends Base
{

    /**
     * @var \GuzzleHttp\Client
     */
    protected ClientInterface $client;

    /**
     * @var \Psr\Http\Message\RequestInterface
     */
    protected RequestInterface $request;

    /**
     * @var string|null
     */
    protected ?string $token = null;

    public function __construct(ClientInterface $client, RequestInterface $request)
    {
        $this->client = $client;
        $this->request = $request;
    }

    public function authenticate(...$params): void
    {
        $this->token = $this->getNewToken();
    }

    public function authenticateClient(Client $client): void
    {
        $client->setHeader('Authorization', sprintf('Bearer %s', $this->getToken()));
    }

    protected function findLastToken(): ?string
    {
        return $this->token;
    }

    protected function getNewToken(): ?string
    {
        $response = $this->client->sendRequest($this->request);
        return $response->getBody()->getContents();
    }

    protected function getToken(): string
    {
        $token = $this->findLastToken();
        if (!$token) {
            $token = $this->getNewToken();
        }
        $this->token = $token;
        return $token;
    }
}
