<?php

namespace Smorken\Graphql\Contracts;

/**
 * Interface Request
 *
 * @package Smorken\Graphql\Contracts
 *
 * @property string $query
 * @property array $variables
 * @property string|array $results
 * @property float $start
 * @property float $stop
 */
interface Last
{

    public function getElapsed(): float;

    public function reset(): void;

    public function start(): void;

    public function stop(): void;

    public function toArray(): array;
}
