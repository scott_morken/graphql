<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types;

use Smorken\Graphql\Models\Queryable;

class LanguageProgram extends Queryable
{

    protected array $fields = ['id', 'level', 'languageOfStudy'];
}
