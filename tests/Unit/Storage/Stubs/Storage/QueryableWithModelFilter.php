<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Storage\Queryable;
use Smorken\Support\Contracts\Filter;

class QueryableWithModelFilter extends Queryable
{

    protected function filterApplyField(Query $query, Filter $filter, $value): Query
    {
        if (strlen($value)) {
            $query->applyField($value);
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_apply_field' => 'filterApplyField',
        ];
    }
}
