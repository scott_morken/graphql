<?php

namespace Smorken\Graphql\Contracts\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Contracts\Query\Query;

interface Proxy extends HasMockHandlerProxy
{

    /**
     * @param  \Psr\Http\Message\ResponseInterface|\GuzzleHttp\Exception\GuzzleException  $response
     */
    public function addResponse(ResponseInterface|GuzzleException $response): void;

    /**
     * @param  Query  $query
     * @param  array  $vars
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function query(Query $query, array $vars = []): ResponseInterface;

    /**
     * @param  string  $header
     * @param  string  $value
     */
    public function setHeader(string $header, string $value): void;
}
