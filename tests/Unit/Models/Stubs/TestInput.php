<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Models\Input;

class TestInput extends Input
{

    protected array $inputAttributes = ['foo', 'id'];
}
