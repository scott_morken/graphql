<?php

namespace Smorken\Graphql;

use Smorken\Support\Arr;

class Error implements \Smorken\Graphql\Contracts\Error
{

    protected array $error = [];

    protected ?int $statusCode = null;

    public function __construct(mixed $error, ?int $statusCode = null)
    {
        $this->setError($error);
        if (!is_null($statusCode)) {
            $this->setStatusCode($statusCode);
        }
    }

    public function __toString(): string
    {
        return sprintf(
            'Status: %d - Message: %s - Url: %s - Path: %s',
            $this->getStatusCode(),
            $this->getMessage(),
            $this->getUrl() ?: 'None',
            $this->getPath() ?: 'None'
        );
    }

    public function getByKey(string $key): ?string
    {
        $v = Arr::get($this->error, $key);
        if (is_array($v)) {
            $v = Arr::stringify($v);
        }
        return $v ? trim($v) : null;
    }

    public function getMessage(): ?string
    {
        return ($this->getByKey('extensions.response.body.error_description') ?: $this->getByKey('message'));
    }

    public function getPath(): ?string
    {
        return $this->getByKey('path');
    }

    public function getStatusCode(): int
    {
        return (int) ($this->statusCode ?: ($this->getByKey('extensions.response.status') ?: 0));
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function getUrl(): ?string
    {
        return $this->getByKey('extensions.response.url');
    }

    public function toRawArray(): array
    {
        return $this->error;
    }

    protected function setError(mixed $error): void
    {
        if (is_array($error)) {
            $this->error = $error;
        } else {
            $this->error = ['message' => (string) $error];
        }
    }
}
