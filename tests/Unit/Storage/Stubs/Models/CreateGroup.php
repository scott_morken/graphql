<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Mutatable;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;

class CreateGroup extends Mutatable
{

    use FakeRelations;

    protected array $fields = ['transactionId'];

    protected array $requestVariables = [
        'name' => ['type' => 'String', 'required' => true],
    ];

    public function group()
    {
        return $this->fakeBelongsToViaCallback(
            Group::class,
            function (FakeRelation $relation, CreateGroup $model) {
                $g = $model->getAttributeFromArray('group');
                if ($g) {
                    return $relation->getRelated()->newInstance($g);
                }
                return null;
            }
        );
    }

    protected function addToFields(array $fields): array
    {
        $fields[] = $this->newMutationWithoutDefaults()
                         ->setName('group')
                         ->addFields(['id', 'name']);
        return $fields;
    }
}
