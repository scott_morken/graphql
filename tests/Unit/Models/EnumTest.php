<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Models\Enum;
use Smorken\Graphql\PhpGraphQL\Query;

class EnumTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        Enum::setQuery(new Query());
    }

    public function testDefaultQuery(): void
    {
        $sut = new Enum();
        $expected = 'query {
__type(name: "Enum")
}';
        $this->assertEquals($expected, (string) $sut->newQuery());
    }

    public function testDefaultQueryWithDefaultFields(): void
    {
        $sut = new Enum();
        $expected = 'query {
__type(name: "Enum") {
enumValues {
name
}
}
}';
        $this->assertEquals($expected, (string) $sut->newQuery()->defaultFields());
    }
}
