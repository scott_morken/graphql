<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types;

use Smorken\Graphql\Models\Queryable;

class LicensePool extends Queryable
{

    protected array $fields = [
        'id',
        'productId',
        'productName',
        'salesPackageId',
        'salesPackageName',
        'poNumber',
        'totalLicenseCount',
        'assignedLicenseCount',
        'termStart',
        'termEnd',
        'createdAt',
        'licenseModel',
    ];

    public function getAvailableLicenseCount(): int
    {
        return (($this->totalLicenseCount ?? 0) - ($this->assignedLicenseCount ?? 0));
    }

    public function getTermEndAttribute()
    {
        return $this->toCarbonDate('termEnd');
    }

    public function getTermStartAttribute()
    {
        return $this->toCarbonDate('termStart');
    }

    public function hasCountAvailable(int $count = 1): bool
    {
        return $this->getAvailableLicenseCount() >= $count;
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        return $this->newQueryEmpty()->setName('myOrg');
    }
}
