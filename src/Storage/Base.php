<?php

namespace Smorken\Graphql\Storage;

use Smorken\Graphql\Contracts\Factory;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Models\MutateAndQuery;
use Smorken\Graphql\Contracts\Response\ToModel;
use Smorken\Storage\Traits\CacheAssist;

abstract class Base implements \Smorken\Storage\Contracts\CacheAssist
{

    use CacheAssist;

    protected array $errors = [];

    /**
     * @var \Smorken\Graphql\Contracts\Factory
     */
    protected Factory $factory;

    /**
     * @var \Smorken\Graphql\Contracts\Models\MutateAndQuery
     */
    protected MutateAndQuery $model;

    /**
     * @var \Smorken\Graphql\Contracts\Response\ToModel
     */
    protected ToModel $toModel;

    public function __construct(MutateAndQuery $model, Factory $factory, ToModel $toModel, array $cacheOptions = [])
    {
        $this->setModel($model);
        $this->setFactory($factory);
        $this->setToModel($toModel);
        if ($cacheOptions) {
            $this->cacheAssist = $this->createCacheAssist($cacheOptions);
        }
    }

    public function clearErrors(): void
    {
        $this->errors = [];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    protected function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    public function getFactory(): Factory
    {
        return $this->factory;
    }

    public function setFactory(Factory $factory): void
    {
        $this->factory = $factory;
    }

    public function getModel(): MutateAndQuery
    {
        return $this->model;
    }

    public function setModel(MutateAndQuery $model): void
    {
        $this->model = $model;
    }

    public function getToModel(): ToModel
    {
        return $this->toModel;
    }

    public function setToModel(ToModel $toModel): void
    {
        $this->toModel = $toModel;
        $this->setModelOnToModel();
    }

    public function hasErrors(): bool
    {
        return count($this->errors) > 0;
    }

    protected function reset(): void
    {
        $this->clearErrors();
    }

    protected function setModelOnToModel(?Model $model = null): void
    {
        if ($model) {
            $this->getToModel()->setModel($model);
        } else {
            $this->getToModel()->setModel($this->getModel());
        }
    }
}
