<?php

namespace Smorken\Graphql\Contracts\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Support\Contracts\Filter;

interface MutateAndQuery extends Model
{

    public function getFields(): array;

    public function getQuery(): Query;

    public function getQueryName(bool $multi = false): string;

    public function getRequestVariables(array $overrides = []): array;

    public function queryApplyFilter(Query $query, ?Filter $filter): Query;

    public function queryDefaultFields(Query $query): Query;

    public static function setQuery(Query $query): void;
}
