<?php

namespace Tests\Smorken\Graphql\Unit\PhpGraphQL;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Exception;
use Smorken\Graphql\Models\Queryable;
use Smorken\Graphql\PhpGraphQL\Argument;
use Smorken\Graphql\PhpGraphQL\Query;
use Smorken\Graphql\PhpGraphQL\Variable;
use Tests\Smorken\Graphql\Unit\PhpGraphQL\Models\QueryModel;

class QueryTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        Query::setVariableProvider(new Variable());
    }

    public function testCompareVariablesIsFalseWhenVariablesDoNotMatch(): void
    {
        $count = 3;
        $base = (new Query())->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION)
                             ->addVariable((new Variable())->newInstance('input', 'CreateGroupInput', true), 0);
        $subs = [];
        for ($i = 0; $i < $count; $i++) {
            $base->setIndex($i);
            $subs[] = (new Query())->setName('createGroup')
                                   ->setIndex($i)
                                   ->addArgumentAsVariable('input', $i)
                                   ->addFields(
                                       [
                                           'clientMutationId',
                                           (new Query())->setName('group')->addField('id'),
                                       ]
                                   )
                                   ->addRequestVariableFromComponents('input', ['name' => 'Group '.$i], $i);
        }
        $base->addFields($subs);
        $expected = 'mutation($input_0: CreateGroupInput!) {
index_0: createGroup(input: $input_0) {
clientMutationId
group {
id
}
}
index_1: createGroup(input: $input_1) {
clientMutationId
group {
id
}
}
index_2: createGroup(input: $input_2) {
clientMutationId
group {
id
}
}
}';
        $this->assertEquals($expected, (string) $base);
        $expected = [
            'input_0' => [
                'name' => 'Group 0',
            ],
            'input_1' => [
                'name' => 'Group 1',
            ],
            'input_2' => [
                'name' => 'Group 2',
            ],
        ];
        $this->assertEquals($expected, $base->getAllRequestVariablesAsArray());
        $this->assertFalse($base->compareVariables(count($base->getAllRequestVariables())));
    }

    public function testGetAllRequestVariablesRetrievesFromAllSubQueries(): void
    {
        $count = 3;
        $base = (new Query())->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION);
        $subs = [];
        for ($i = 0; $i < $count; $i++) {
            $subs[] = (new Query())->setName('createGroup')
                                   ->setIndex($i)
                                   ->addRequestVariableFromComponents('sub', 'val'.$i, $i)
                                   ->addFields(
                                       [
                                           'clientMutationId',
                                           (new Query())->setName('group')
                                                        ->addField('id')
                                                        ->addRequestVariableFromComponents('subsub', 'valval'.$i, $i),
                                       ]
                                   );
        }
        $base->addFields($subs);
        $vars = $base->getAllRequestVariablesAsArray();
        $expected = [
            'sub_0' => 'val0',
            'subsub_0' => 'valval0',
            'sub_1' => 'val1',
            'subsub_1' => 'valval1',
            'sub_2' => 'val2',
            'subsub_2' => 'valval2',
        ];
        $this->assertEquals($expected, $vars);
    }

    public function testMultipleMutationsWithIndexedVariableArgumentAndFields(): void
    {
        $count = 3;
        $base = (new Query())->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION);
        $subs = [];
        for ($i = 0; $i < $count; $i++) {
            $base->setIndex($i)
                 ->addVariable((new Variable())->newInstance('input', 'CreateGroupInput', true), $i);
            $subs[] = (new Query())->setName('createGroup')
                                   ->setIndex($i)
                                   ->addArgumentAsVariable('input', $i)
                                   ->addFields(
                                       [
                                           'clientMutationId',
                                           (new Query())->setName('group')->addField('id'),
                                       ]
                                   )
                                   ->addRequestVariableFromComponents('input', ['name' => 'Group '.$i], $i);
        }
        $base->addFields($subs);
        $expected = 'mutation($input_0: CreateGroupInput! $input_1: CreateGroupInput! $input_2: CreateGroupInput!) {
index_0: createGroup(input: $input_0) {
clientMutationId
group {
id
}
}
index_1: createGroup(input: $input_1) {
clientMutationId
group {
id
}
}
index_2: createGroup(input: $input_2) {
clientMutationId
group {
id
}
}
}';
        $this->assertEquals($expected, (string) $base);
        $expected = [
            'input_0' => [
                'name' => 'Group 0',
            ],
            'input_1' => [
                'name' => 'Group 1',
            ],
            'input_2' => [
                'name' => 'Group 2',
            ],
        ];
        $this->assertEquals($expected, $base->getAllRequestVariablesAsArray());
    }

    public function testMutationToString(): void
    {
        $sut = (new Query())->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION);
        $this->assertEquals('mutation', (string) $sut);
    }

    public function testMutationWithIndexedVariableArgumentAndFields(): void
    {
        $sut = (new Query())->setName('createGroup')
                            ->setIndex(0)
                            ->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION)
                            ->addVariable((new Variable())->newInstance('input', 'CreateGroupInput', true), 0)
                            ->addArgumentFromComponents('input', '$input', true, 0)
                            ->addFields(
                                [
                                    'clientMutationId',
                                    (new Query())->setName('group')->addField('id'),
                                ]
                            );
        $expected = "mutation(\$input_0: CreateGroupInput!) {\nindex_0: createGroup(input: \$input_0) {\nclientMutationId\ngroup {\nid\n}\n}\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testMutationWithName(): void
    {
        $sut = (new Query())->setName('doSomething')->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION);
        $expected = "mutation {\ndoSomething\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testMutationWithNameAndArgument(): void
    {
        $sut = (new Query())->setName('doSomething')
                            ->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION)
                            ->addArgumentFromComponents('name', 'Foo', false);
        $expected = "mutation {\ndoSomething(name: \"Foo\")\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testMutationWithOperationName(): void
    {
        $sut = (new Query())->setOperationName('doSomething')->setQueryType(
            \Smorken\Graphql\Contracts\Query\Query::MUTATION
        );
        $expected = "mutation doSomething";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testMutationWithVariableArgumentAndFields(): void
    {
        $sut = (new Query())->setName('createGroup')
                            ->setQueryType(\Smorken\Graphql\Contracts\Query\Query::MUTATION)
                            ->addVariable((new Variable())->newInstance('input', 'CreateGroupInput', true))
                            ->addArgumentFromComponents('input', '$input', true)
                            ->addFields(
                                [
                                    'clientMutationId',
                                    (new Query())->setName('group')->addField('id'),
                                ]
                            );
        $expected = "mutation(\$input: CreateGroupInput!) {\ncreateGroup(input: \$input) {\nclientMutationId\ngroup {\nid\n}\n}\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testNewFromSelfWithModel(): void
    {
        $model = new Queryable();
        $sut = (new Query())->setModel($model);
        $q = $sut->newFromSelf();
        $this->assertEquals("query {\nqueryable\n}", (string) $q);
    }

    public function testNewFromSelfWithQueryObject(): void
    {
        $model = new Queryable();
        $sut = (new Query())->setModel($model)
                            ->setQueryObject(new \GraphQL\Query('test'));
        $q = $sut->newFromSelf(false, true);
        $this->assertEquals("query {\ntest\n}", (string) $q);
    }

    public function testNewFromSelfWithoutModel(): void
    {
        $model = new Queryable();
        $sut = (new Query())->setModel($model);
        $q = $sut->newFromSelf(false);
        $this->assertEquals("query", (string) $q);
    }

    public function testQueryCanCallQueryMethodOnModel(): void
    {
        $model = new QueryModel();
        $sut = (new Query())->setName('One')->setModel($model)->myFields();
        $expected = "query {\nOne {\nsubQuery {\nid\ndescr\n}\n}\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQueryCanCallQueryMethodOnModelWithArgument(): void
    {
        $model = new QueryModel();
        $sut = (new Query())->setName('One')->setModel($model)->myVariable('foo');
        $expected = "query(\$myVar: String=\"foo\") {\nOne\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQueryFromStringIsString(): void
    {
        $sut = new Query();
        $sut->fromString('query { foo }');
        $this->assertEquals('query { foo }', (string) $sut);
    }

    public function testQuerySetArrayIntegerArgument(): void
    {
        $sut = (new Query())->setName('One')
                            ->addArgumentFromComponents('var', [42, 24], false);
        $expected = "query {\nOne(var: [42, 24])\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetIndexedVariableArgument(): void
    {
        $sut = (new Query())->setName('One')
                            ->setIndex(0)
                            ->addVariable((new Variable())->setName('var'), 0)
                            ->addArgumentFromComponents('var', '$var', true, 0);
        $expected = "query(\$var_0: String) {\nindex_0: One(var: \$var_0)\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetMixedArguments(): void
    {
        $sut = (new Query())->setName('One')
                            ->addArguments(
                                [
                                    (new Argument())->setName('var1')->setValue([42, 24], false),
                                    ['name' => 'var2', 'value' => 'test', 'isRequestArgument' => false],
                                    (new Argument())->setName('var3')->setValue(true, false),
                                ]
                            );
        $expected = "query {\nOne(var1: [42, 24] var2: \"test\" var3: true)\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetMixedVariables(): void
    {
        $sut = (new Query())->setName('One')
                            ->addVariables(
                                [
                                    (new Variable())->newInstance('var', 'String'),
                                    [
                                        'name' => 'int',
                                        'type' => 'Int',
                                        'required' => false,
                                        'default' => 4,
                                    ],
                                ]
                            );
        $expected = "query(\$var: String \$int: Int=4) {\nOne\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetMixedVariablesWithOperationName(): void
    {
        $sut = (new Query())->setName('One')
                            ->setOperationName('doSomething')
                            ->addVariables(
                                [
                                    (new Variable())->newInstance('var', 'String'),
                                    [
                                        'name' => 'int',
                                        'type' => 'Int',
                                        'required' => false,
                                        'default' => 4,
                                    ],
                                ]
                            );
        $expected = "query doSomething(\$var: String \$int: Int=4) {\nOne\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetSingleBooleanArgument(): void
    {
        $sut = (new Query())->setName('One')
                            ->addArgumentFromComponents('var', false, false);
        $expected = "query {\nOne(var: false)\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetSingleIntegerArgument(): void
    {
        $sut = (new Query())->setName('One')
                            ->addArgumentFromComponents('var', 42, false);
        $expected = "query {\nOne(var: 42)\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetSingleNullArgument(): void
    {
        $sut = (new Query())->setName('One')
                            ->addArgumentFromComponents('var', null, false);
        $expected = "query {\nOne(var: null)\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetSingleStringArgument(): void
    {
        $sut = (new Query())->setName('One')
                            ->addArgumentFromComponents('var', 'test', false);
        $expected = "query {\nOne(var: \"test\")\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetSingleVariable(): void
    {
        $sut = (new Query())->setName('One')
                            ->addVariable((new Variable())->newInstance('var', 'String'));
        $expected = "query(\$var: String) {\nOne\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQuerySetVariablesWithoutVariablesIsException(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Variable could not be created');
        $sut = (new Query())
            ->addVariables(['foo', 'bar']);
    }

    public function testQueryToString(): void
    {
        $sut = new Query();
        $this->assertEquals('query', (string) $sut);
    }

    public function testQueryWithFields(): void
    {
        $sut = new Query();
        $sut->addField((new Query())->setName('One')->addFields(['field_one', 'field_two']));
        $expected = "query {\nOne {\nfield_one\nfield_two\n}\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQueryWithOperationName(): void
    {
        $sut = (new Query())->setName('One')->setOperationName('doSomething');
        $expected = "query doSomething {\nOne\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQueryWithOperationNameAndOperationNameInSubqueryFieldDoesNothing(): void
    {
        $sut = (new Query())
            ->setName('One')
            ->setOperationName('doSomething')
            ->addField((new Query())->setName('Two')->setOperationName('doSomethingElse'));
        $expected = "query doSomething {\nOne {\nTwo\n}\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQueryWithOperationNameAndSubqueryField(): void
    {
        $sut = (new Query())
            ->setOperationName('doSomething')
            ->addField((new Query())->setName('Two'));
        $expected = "query doSomething {\nTwo\n}";
        $this->assertEquals($expected, (string) $sut);
    }

    public function testQueryWithSubqueryField(): void
    {
        $sut = new Query();
        $sut->addField((new Query())->setName('One')->addField('field_one'));
        $expected = "query {\nOne {\nfield_one\n}\n}";
        $this->assertEquals($expected, (string) $sut);
    }
}
