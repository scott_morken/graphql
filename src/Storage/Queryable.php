<?php

namespace Smorken\Graphql\Storage;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Models\Connection;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Models\MutateAndQuery;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Contracts\Response\FromClient;
use Smorken\Graphql\Traits\ApplyFilter;
use Smorken\Support\Contracts\Filter;

class Queryable extends Base implements \Smorken\Graphql\Contracts\Storage\Queryable
{

    use ApplyFilter;

    public function all(): Collection
    {
        $q = $this->makeBaseQuery($this->getModel(), null, null, true);
        $fromClient = $this->fromClientFromQuery($q);
        return $this->collectionFromFromClient($fromClient);
    }

    public function find(string $id): ?Model
    {
        $q = $this->getFindQuery($id, null);
        $fromClient = $this->fromClientFromQuery($q);
        return $this->modelFromFromClient($fromClient);
    }

    public function findWithFilter(string $id, Filter $filter): ?Model
    {
        $q = $this->getFindQuery($id, $filter);
        $fromClient = $this->fromClientFromQuery($q);
        return $this->modelFromFromClient($fromClient);
    }

    public function getByFilter(Filter $filter): Collection
    {
        $q = $this->makeBaseQuery($this->getModel(), null, $filter, true);
        $fromClient = $this->fromClientFromQuery($q);
        return $this->getToModel()->many($fromClient);
    }

    public function getConnectionByFilter(Filter $filter, int $per_page = 100, string $after = null): Connection
    {
        $fromClient = $this->getByFilterFromClient($filter, $per_page, $after);
        return $this->getToModel()->connection($fromClient);
    }

    public function getConnectionsByFilter(Filter $filter, int $per_page = 100, string $after = null): Collection
    {
        $fromClient = $this->getByFilterFromClient($filter, $per_page, $after);
        return $this->getToModel()->connections($fromClient);
    }

    protected function applyDefaultFields(MutateAndQuery $model, Query $query, array $additionalFields = []): void
    {
        if (method_exists($model, 'queryDefaultFields')) {
            $query->defaultFields();
        }
        $query->addFields($additionalFields);
    }

    protected function applyFilter(MutateAndQuery $model, Query $query, ?Filter $filter): void
    {
        if ($filter) {
            $this->applyFilterToQuery($query, $filter);
            if (method_exists($model, 'queryApplyFilter')) {
                $query->applyFilter($filter);
            }
        }
    }

    protected function collectionFromFromClient(
        FromClient $fromClient,
        ?MutateAndQuery $model = null
    ): Collection {
        $this->setModelOnToModel($model);
        return $this->getToModel()->many($fromClient);
    }

    protected function fromClientFromQuery(Query $query, array $vars = []): FromClient
    {
        $this->reset();
        $fromClient = $this->getFactory()->query($query->getBaseQuery(true), $vars);
        if ($fromClient->hasErrors()) {
            $this->setErrors($fromClient->getErrors());
        }
        return $fromClient;
    }

    protected function getByFilterFromClient(Filter $filter, int $per_page, ?string $after): FromClient
    {
        $q = $this->makeBaseQuery($this->getModel(), null, $filter, true);
        $q->addArgumentFromComponents('first', $per_page, false)
          ->addArgumentFromComponents('after', $after, false);
        return $this->fromClientFromQuery($q);
    }

    protected function getFindQuery(string $id, ?Filter $filter): Query
    {
        $model = $this->getModel();
        $q = $this->makeBaseQuery($model);
        if (method_exists($model, 'queryIdIs')) {
            $q->idIs($id);
        }
        $this->applyFilter($model, $q, $filter);
        return $q;
    }

    protected function makeBaseQuery(
        MutateAndQuery $model,
        ?string $alias = null,
        ?Filter $filter = null,
        bool $multi = false
    ): Query {
        $q = $model->newQuery()
                   ->setMulti($multi)
                   ->setIndex($alias);
        if (!is_null($alias)) {
            $q->setUseIndex(true);
        }
        $this->applyDefaultFields($model, $q);
        $this->applyFilter($model, $q, $filter);
        return $q;
    }

    protected function modelFromFromClient(
        FromClient $fromClient,
        ?Model $model = null
    ): ?Model {
        $this->setModelOnToModel($model);
        return $this->getToModel()->one($fromClient);
    }
}
