<?php

namespace Smorken\Graphql\Traits;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Support\Contracts\Filter;

trait ApplyFilter
{

    protected function applyFilterToQuery(Query $query, Filter $filter): void
    {
        $filter_methods = $this->getFilterMethods();
        foreach ($filter->toArray() as $k => $v) {
            $method = $filter_methods[$k] ?? null;
            if ($method && method_exists($this, $method)) {
                $this->$method($query, $filter, $v);
            }
        }
    }

    protected function getFilterMethods(): array
    {
        return [];
    }
}
