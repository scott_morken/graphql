<?php

namespace Smorken\Graphql\Contracts\Response;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Models\Connection;
use Smorken\Graphql\Contracts\Models\Model;

interface ToModel
{

    public function connection(FromClient $response): ?Connection;

    public function connectionFromIterable(iterable $items): ?Connection;

    public function connections(FromClient $response): Collection;

    public function connectionsFromIterable(iterable $items): Collection;

    public function many(FromClient $response): Collection;

    public function manyFromIterable(iterable $items): Collection;

    public function one(FromClient $response): ?Model;

    public function oneFromIterable(iterable $items): Collection;

    public function setModel(Model $model): self;
}
