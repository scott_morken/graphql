<?php

namespace Smorken\Graphql\Clients;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use Psr\Http\Message\ResponseInterface;

class MockHandlerProxy implements \Smorken\Graphql\Contracts\Clients\MockHandlerProxy
{

    protected static ?MockHandlerProxy $instance = null;

    protected ?MockHandler $handler = null;

    protected array $responseStack = [];

    protected function __construct()
    {
    }

    public static function getInstance(): \Smorken\Graphql\Contracts\Clients\MockHandlerProxy
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public static function reset(): void
    {
        self::$instance = null;
    }

    public function addResponse(
        GuzzleException|ResponseInterface $response
    ): self {
        if ($this->getHandler()) {
            $this->addResponseToHandler($response);
        } else {
            $this->responseStack[] = $response;
        }
        return $this;
    }

    public function getHandler(): ?MockHandler
    {
        return $this->handler;
    }

    public function setHandler(?MockHandler $handler): self
    {
        $this->handler = $handler;
        foreach ($this->responseStack as $i => $response) {
            $this->addResponseToHandler($response);
            unset($this->responseStack[$i]);
        }
        return $this;
    }

    protected function addResponseToHandler(GuzzleException|ResponseInterface $response): void
    {
        $this->getHandler()->append($response);
    }
}
