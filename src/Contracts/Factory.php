<?php

namespace Smorken\Graphql\Contracts;

use Smorken\Graphql\Contracts\Clients\Client;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Contracts\Response\FromClient;

interface Factory
{

    /**
     * Handle authentication - changes need to be handled by reference to
     * an internal object
     */
    public function authenticate(): void;

    /**
     * @param  string  $message
     * @param  array  $context
     */
    public function debug(string $message, mixed $context = []): void;

    /**
     * @return \Smorken\Graphql\Contracts\Authenticate
     */
    public function getAuthenticate(): Authenticate;

    /**
     * @return \Smorken\Graphql\Contracts\Clients\Client
     */
    public function getClient(): Client;

    /**
     * Details on the last request
     *
     * @return \Smorken\Graphql\Contracts\Last
     */
    public function getLast(): Last;

    /**
     * @param  \Smorken\Graphql\Contracts\Query\Query  $query
     * @param  array  $variables
     * @return \Smorken\Graphql\Contracts\Response\FromClient
     */
    public function query(Query $query, array $variables = []): FromClient;
}
