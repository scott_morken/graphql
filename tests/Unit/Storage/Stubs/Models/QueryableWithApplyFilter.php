<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;
use Smorken\Support\Contracts\Filter;

class QueryableWithApplyFilter extends Queryable
{

    public function queryApplyField(Query $query, string $field): Query
    {
        return $query->addField($field);
    }

    protected function filterSomethingIs(Query $query, Filter $filter, $value): Query
    {
        return $query->addArgumentFromComponents('something', $value, false);
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_something_is' => 'filterSomethingIs',
        ];
    }
}
