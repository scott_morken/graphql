<?php

namespace Smorken\Graphql\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Factory;
use Smorken\Graphql\Contracts\Models\Connection;
use Smorken\Graphql\Contracts\Models\Model;
use Smorken\Graphql\Contracts\Models\MutateAndQuery;
use Smorken\Graphql\Contracts\Response\ToModel;
use Smorken\Support\Contracts\Filter;

interface Queryable
{

    public function all(): Collection;

    public function clearErrors(): void;

    public function find(string $id): ?Model;

    public function findWithFilter(string $id, Filter $filter): ?Model;

    public function getByFilter(Filter $filter): Collection;

    /**
     * @param  \Smorken\Support\Contracts\Filter  $filter
     * @param  string|null  $after
     * @param  int  $per_page
     * @return \Smorken\Graphql\Contracts\Models\Connection
     */
    public function getConnectionByFilter(Filter $filter, int $per_page = 100, string $after = null): Connection;

    /**
     * @param  \Smorken\Support\Contracts\Filter  $filter
     * @param  string|null  $after
     * @param  int  $per_page
     * @return Collection<Connection>
     */
    public function getConnectionsByFilter(Filter $filter, int $per_page = 100, string $after = null): Collection;

    public function getErrors(): array;

    public function getFactory(): Factory;

    public function getModel(): MutateAndQuery;

    public function getToModel(): ToModel;

    public function hasErrors(): bool;

    public function setFactory(Factory $factory): void;

    public function setModel(MutateAndQuery $model): void;

    public function setToModel(ToModel $toModel): void;
}
