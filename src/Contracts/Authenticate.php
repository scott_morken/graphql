<?php

namespace Smorken\Graphql\Contracts;

use Smorken\Graphql\Contracts\Clients\Client;
use Smorken\Graphql\Contracts\Clients\HasMockHandlerProxy;

interface Authenticate extends HasMockHandlerProxy
{

    public function authenticate(...$params): void;

    public function authenticateClient(Client $client): void;

    public function isAuthenticationException(\Throwable $exception, int $statusCode = 0): bool;

    public function setParam(string $key, $value): void;

    public function setParams(array $params): void;
}
