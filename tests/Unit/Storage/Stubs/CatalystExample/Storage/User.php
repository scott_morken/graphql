<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Storage;

use Illuminate\Support\Collection;
use Smorken\Graphql\Storage\Queryable;
use Smorken\Support\Contracts\Filter;

class User extends Queryable
{

    public function getBySsoIdIn(array $ssoIds, Filter $filter): Collection
    {
        if ($ssoIds) {
            $base = $this->getModel()->newQueryEmpty();
            foreach ($ssoIds as $index => $id) {
                $sq = $this->makeBaseQuery($this->getModel(), $index, $filter, false)
                           ->setName('userBySsoId')
                           ->ssoIdIs($id);
                $base->addField($sq);
            }
            $fromClient = $this->fromClientFromQuery($base, $base->getAllRequestVariablesAsArray());
            return $this->collectionFromFromClient($fromClient);
        }
        return new Collection();
    }
}
