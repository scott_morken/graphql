<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Queryable;

class LicensePool extends Queryable
{

    protected array $fields = [
        'id',
        'productId',
        'productName',
        'salesPackageId',
        'salesPackageName',
        'poNumber',
        'createdAt',
        'totalLicenseCount',
        'assignedLicenseCount',
        'licenseModel',
        'termStart',
        'termEnd',
    ];

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        return $this->defaultNewQuery(false)->setName('myOrg');
    }
}
