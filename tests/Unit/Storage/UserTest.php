<?php

namespace Tests\Smorken\Graphql\Unit\Storage;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Response\ToModel;
use Smorken\Support\Filter;
use Tests\Smorken\Graphql\Traits\Response;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types\User;

class UserTest extends TestCase
{

    use Response;

    protected $base_path = 'Storage/UserTest/';

    public function testGetBySsoIdInWithFilter(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('User.getBySsoIdIn.json');
        $params['factory']->getClient()->addResponse($response);
        $coll = $sut->getBySsoIdIn(
            ['sss100@maricopa.edu'],
            new Filter(
                [
                    'languagePrograms' => true,
                    'licenses' => true,
                    'groupIds' => true,
                ]
            )
        );
        $last = $sut->getFactory()->getLast();
        $query = 'query($ssoId_0: String!) {
index_0: userBySsoId(ssoId: $ssoId_0) {
id
email
createdAt
updatedAt
firstName
lastName
ssoId
status
languagePrograms {
id
level
languageOfStudy
}
licenses {
assignedAt
licensePool {
id
productId
productName
salesPackageId
salesPackageName
poNumber
totalLicenseCount
assignedLicenseCount
termStart
termEnd
createdAt
licenseModel
}
}
groupIds
}
}';
        $this->assertEquals($query, $last->query);
        $vars = [
            'ssoId_0' => 'sss100%40maricopa.edu'
        ];
        $this->assertEquals($vars, $last->variables);
    }

    protected function getParams(): array
    {
        return [
            'model' => new User(),
            'factory' => $this->createFactory(),
            'toModel' => new ToModel(),
        ];
    }

    protected function getSut(array $params): \Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Storage\User
    {
        return new \Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Storage\User(...array_values($params));
    }
}
