<?php

namespace Tests\Smorken\Graphql\Support;

use Psr\Log\LoggerInterface;
use Stringable;

class NullLogger implements LoggerInterface
{

    protected array $messages = [];

    public function alert(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function critical(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function debug(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function emergency(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function error(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function info(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function log($level, $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function notice(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    public function warning(Stringable|string $message, array $context = []): void
    {
        $this->add($message, $context);
    }

    protected function add(Stringable|string $message, array $context): void
    {
        $this->messages[] = ['message' => $message, 'context' => $context];
    }
}
