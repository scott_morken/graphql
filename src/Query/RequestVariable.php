<?php

namespace Smorken\Graphql\Query;

class RequestVariable implements \Smorken\Graphql\Contracts\Query\RequestVariable
{

    /**
     * @var string|null
     */
    protected ?string $name = null;

    /**
     * @var mixed
     */
    protected mixed $value = null;

    public function get(): mixed
    {
        return $this->toArray();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setValue($value): self
    {
        $this->value = $value;
        return $this;
    }

    public function newInstance(string $name, $value): static
    {
        return (new static())
            ->setName($name)
            ->setValue($value);
    }

    public function toArray(): array
    {
        return [
            $this->getName() => $this->getValue(),
        ];
    }
}
