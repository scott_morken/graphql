## GraphQL Model and Storage library for Laravel 6

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation (Laravel)

The service provider should automatically register.
If not, you can manually add `Smorken\Graphql\ServiceProvider::class` to the
providers section of `config/app.php`.

Publish the config file if needed

```
php artisan vendor:publish --provider="Smorken\Graphql\ServiceProvider"
```

This will provide a `config/graphql.php`
