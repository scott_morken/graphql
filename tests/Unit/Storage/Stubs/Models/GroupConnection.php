<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Connection;

class GroupConnection extends Connection
{

    protected array $models = [
        self::NODE => Group::class,
        self::EDGE => \Smorken\Graphql\Models\Queryable::class,
        self::PAGEINFO => \Smorken\Graphql\Models\PageInfo::class,
    ];

    public function queryOrgIdIs(Query $query, string $orgId): Query
    {
        $query->getBaseQuery()->addArgumentFromComponents('orgId', $orgId, false);
        return $query;
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        return $this->defaultNewQuery(false)->setName('myOrg');
    }
}
