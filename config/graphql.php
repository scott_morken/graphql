<?php
return [
    'providers' => [
        \Smorken\Graphql\Contracts\Clients\Client::class => env(
            'GRAPHQL_PROVIDER_CLIENT',
            \Smorken\Graphql\Clients\Client::class
        ),
        \Smorken\Graphql\Contracts\Clients\Proxy::class => env(
            'GRAPHQL_PROVIDER_CLIENT_PROXY',
            \Smorken\Graphql\Clients\GuzzleProxy::class
        ),
        \Smorken\Graphql\Contracts\Query\Argument::class => env(
            'GRAPHQL_PROVIDER_ARGUMENT',
            \Smorken\Graphql\PhpGraphQL\Argument::class
        ),
        \Smorken\Graphql\Contracts\Query\Query::class => env(
            'GRAPHQL_PROVIDER_QUERY',
            \Smorken\Graphql\PhpGraphQL\Query::class
        ),
        \Smorken\Graphql\Contracts\Query\RequestVariable::class => env(
            'GRAPHQL_PROVIDER_REQUEST_VARIABLE',
            \Smorken\Graphql\Query\RequestVariable::class
        ),
        \Smorken\Graphql\Contracts\Query\Variable::class => env(
            'GRAPHQL_PROVIDER_VARIABLE',
            \Smorken\Graphql\PhpGraphQL\Variable::class
        ),
        \Smorken\Graphql\Contracts\Response\FromClient::class => env(
            'GRAPHQL_PROVIDER_FROMCLIENT',
            \Smorken\Graphql\Response\FromClient::class
        ),
        \Smorken\Graphql\Contracts\Response\ToModel::class => env(
            'GRAPHQL_PROVIDER_TOMODEL',
            \Smorken\Graphql\Response\ToModel::class
        ),
        \Smorken\Graphql\Contracts\Authenticate::class => env(
            'GRAPHQL_PROVIDER_AUTHENTICATE',
            \Smorken\Graphql\Authenticate\None::class
        ),
        \Smorken\Graphql\Contracts\Factory::class => env('GRAPHQL_PROVIDER_FACTORY', \Smorken\Graphql\Factory::class),
        \Smorken\Graphql\Contracts\Last::class => env('GRAPHQL_PROVIDER_LAST', \Smorken\Graphql\Last::class),
    ],
    'client' => [
        'endpoint' => env('GRAPHQL_ENDPOINT', ''),
        'mock_responses' => env('GRAPHQL_CLIENT_MOCK_RESPONSES', false),
        'proxy_options' => [
            'connect_timeout' => 2,
            'timeout' => 30,
        ],
        'client_options' => [],
    ],
];
