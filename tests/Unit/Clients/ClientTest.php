<?php

namespace Tests\Smorken\Graphql\Unit\Clients;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Clients\GuzzleProxy;
use Smorken\Graphql\Clients\MockHandlerProxy;
use Smorken\Graphql\Contracts\Clients\Client;
use Smorken\Graphql\PhpGraphQL\Query;
use Smorken\Graphql\Response\FromClient;
use Tests\Smorken\Graphql\Traits\Response;

class ClientTest extends TestCase
{

    use Response;

    protected $base_path = 'Response/FromClientTest/';

    public function testAddResponseCanAddMockResponse(): void
    {
        $response = $this->makeResponse('one.model.json');
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $sut->addResponse($response);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
    }

    public function testClientException(): void
    {
        $response = new ClientException('Error', new Request('POST', 'test'), new \GuzzleHttp\Psr7\Response(500));
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $sut->addResponse($response);
        $this->expectException(ClientException::class);
        $this->expectExceptionMessage('Error');
        $r = $sut->query(new Query());
    }

    public function testClientExceptionStatus400CanPassThrough(): void
    {
        $response = new \GuzzleHttp\Psr7\Response(400);
        $ce = new ClientException('Error', new Request('POST', 'test'), $response);
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $sut->addResponse($ce);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
    }

    public function testStatus400PassesThroughResponse(): void
    {
        $response = $this->makeResponse('one.model.json', 400);
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $sut->addResponse($response);
        $r = $sut->query(new Query());
        $this->assertEquals('Status: 400 - Message: Bad Request - Url: None - Path: None', $r->getErrors()[0]);
    }

    public function testStatus400PassesThroughResponseWhenAllowed(): void
    {
        $response = $this->makeResponse('one.model.json', 400);
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $sut->addResponse($response);
        $r = $sut->query(new Query());
        $this->assertSame($response, $r->getResponse());
    }

    protected function getParams(): array
    {
        $handler = new MockHandler();
        $stack = HandlerStack::create($handler);
        $proxy = new GuzzleProxy('https://localhost', new \GuzzleHttp\Client(['handler' => $stack]));
        $proxy->getMockHandlerProxy()->setHandler($handler);
        return [
            'proxy' => $proxy,
            'from_client' => new FromClient(),
        ];
    }

    protected function getSut(array $params): Client
    {
        return new \Smorken\Graphql\Clients\Client(...array_values($params));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        MockHandlerProxy::reset();
    }
}
