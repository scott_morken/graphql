<?php

namespace Smorken\Graphql;

class Last implements Contracts\Last
{

    public ?string $query = null;

    public array|string|null $results = null;

    /**
     * @var float|null
     */
    public ?float $start = null;

    /**
     * @var float|null
     */
    public ?float $stop = null;

    public array $variables = [];

    public function __construct()
    {
        $this->start();
    }

    public function getElapsed(): float
    {
        $this->stop();
        return number_format(($this->stop ?? 0) - ($this->start ?? 0), 4);
    }

    public function reset(): void
    {
        $this->query = null;
        $this->results = null;
        $this->start = null;
        $this->stop = null;
        $this->variables = [];
        $this->start();
    }

    public function start(): void
    {
        if (is_null($this->start)) {
            $this->start = microtime(true);
        }
    }

    public function stop(): void
    {
        if (is_null($this->stop)) {
            $this->stop = microtime(true);
        }
    }

    public function toArray(): array
    {
        return [
            'query' => (string) $this->query,
            'variables' => $this->variables,
            'results' => $this->results,
            'elapsed' => $this->getElapsed(),
        ];
    }
}
