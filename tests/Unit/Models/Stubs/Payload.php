<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Model\Relations\FakeBelongsToViaCallback;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;

class Payload extends \Smorken\Graphql\Models\Payload
{

    use FakeRelations;

    public function user(): FakeBelongsToViaCallback
    {
        return $this->fakeBelongsToViaCallback(
            User::class,
            function (FakeRelation $relation, $model) {
                return $relation->getRelated()->newInstance($model->getAttributeFromArray('user'));
            }
        );
    }
}
