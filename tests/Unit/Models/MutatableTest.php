<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Models\Mutatable;
use Tests\Smorken\Graphql\Unit\Models\Stubs\MutatableWithDefaults;

class MutatableTest extends TestCase
{

    public function testGetValidationRulesIsArray(): void
    {
        $sut = new Mutatable();
        $this->assertEquals([], $sut->getValidationRules());
    }

    public function testGetValidationRulesOverridesSetsEntireKey(): void
    {
        $sut = new MutatableWithDefaults();
        $expected = [
            'foo' => 'required',
            'bar' => 'optional',
        ];
        $this->assertEquals($expected, $sut->getValidationRules(['bar' => 'optional']));
    }

    public function testGetValidationRulesWithOverrides(): void
    {
        $sut = new Mutatable();
        $this->assertEquals(['foo' => 'required'], $sut->getValidationRules(['foo' => 'required']));
    }

    public function testGetValidationRulesWithRules(): void
    {
        $sut = new MutatableWithDefaults();
        $expected = [
            'foo' => 'required',
            'bar' => [
                'required',
                'int',
            ],
        ];
        $this->assertEquals($expected, $sut->getValidationRules());
    }

    public function testNewQueryAppliesDefaults(): void
    {
        $sut = new MutatableWithDefaults();
        $q = $sut->newMutation();
        $expected = 'mutation {
mutatableWithDefaults {
id
name
}
}';
        $this->assertEquals($expected, (string) $q);
    }

    public function testNewQueryAppliesQueryType(): void
    {
        $sut = new Mutatable();
        $q = $sut->newMutation();
        $expected = 'mutation {
mutatable
}';
        $this->assertEquals($expected, (string) $q);
    }

    public function testNewQueryIsNewInstance(): void
    {
        $sut = new Mutatable();
        $this->assertNotSame($sut->newMutation(), $sut->getQuery());
    }

    public function testNewQueryWithoutDefaults(): void
    {
        $sut = new MutatableWithDefaults();
        $q = $sut->newMutationWithoutDefaults();
        $expected = 'mutation {
mutatableWithDefaults
}';
        $this->assertEquals($expected, (string) $q);
    }
}
