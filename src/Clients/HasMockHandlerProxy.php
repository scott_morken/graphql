<?php

namespace Smorken\Graphql\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Contracts\Clients\MockHandlerProxy;

trait HasMockHandlerProxy
{

    protected ?\Smorken\Graphql\Contracts\Clients\MockHandlerProxy $mockHandlerProxy = null;

    /**
     * @param  \Psr\Http\Message\ResponseInterface|\GuzzleHttp\Exception\GuzzleException  $response
     */
    public function addResponse(ResponseInterface|GuzzleException $response): void
    {
        $this->getMockHandlerProxy()->addResponse($response);
    }

    public function getMockHandlerProxy(): MockHandlerProxy
    {
        if (is_null($this->mockHandlerProxy)) {
            $this->mockHandlerProxy = \Smorken\Graphql\Clients\MockHandlerProxy::getInstance();
        }
        return $this->mockHandlerProxy;
    }
}
