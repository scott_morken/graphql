<?php

namespace Smorken\Graphql\Clients;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Contracts\Clients\Proxy;
use Smorken\Graphql\Contracts\Query\Query;

class GuzzleProxy implements Proxy
{

    use HasMockHandlerProxy;

    protected array $addHeaders = [];

    /**
     * @var \GuzzleHttp\Client
     */
    protected ClientInterface $client;

    /**
     * @var string
     */
    protected string $endpoint;

    /**
     * @var array
     */
    protected array $options = [];

    public function __construct(string $endpoint, ClientInterface $client, array $options = [])
    {
        $this->endpoint = $endpoint;
        $this->client = $client;
        $this->options = $options;
    }

    public function __call(string $method, array $args)
    {
        if (method_exists($this->client, $method)) {
            return $this->client->$method(...array_values($args));
        }
        return null;
    }

    public function query(Query $query, array $vars = []): ResponseInterface
    {
        try {
            $response = $this->client->post(
                $this->endpoint,
                [
                    'headers' => $this->getHeaders(),
                    'json' => $this->getBodyData($query, $vars),
                ]
            );
        } catch (ClientException $ce) {
            $response = $ce->getResponse();
            // 400 - bad request means that the api request was successful but
            // there was a syntax error in the query so we should return it,
            // otherwise, rethrow
            if ($response->getStatusCode() !== 400) {
                throw $ce;
            }
        }
        return $response;
    }

    public function setHeader(string $header, string $value): void
    {
        $this->addHeaders[$header] = $value;
    }

    protected function addHeadersToRequest(RequestInterface $request): RequestInterface
    {
        foreach ($this->getHeaders() as $header => $value) {
            $request = $request->withHeader($header, $value);
        }
        return $request;
    }

    protected function ensureVariables(array $vars)
    {
        if (empty($vars)) {
            $vars = (object) null;
        }
        return $vars;
    }

    protected function getBodyData(Query $query, array $vars): array
    {
        return [
            'query' => (string) $query,
            'variables' => $this->ensureVariables($vars),
        ];
    }

    protected function getHeaders(): array
    {
        $headers = $this->options['headers'] ?? [];
        return array_replace($headers, $this->addHeaders);
    }
}
