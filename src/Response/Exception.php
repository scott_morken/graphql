<?php

namespace Smorken\Graphql\Response;

use Throwable;

class Exception extends \Smorken\Graphql\Exception
{

    /**
     * @var array
     */
    protected array $detail = [];

    /**
     * @var int
     */
    protected int $status;

    public function __construct(int $status, array $detail, $code = 0, Throwable $previous = null)
    {
        $this->status = $status;
        $this->detail = $detail;
        $message = sprintf(
            '[%d] %d errors. %s (first).',
            $status,
            count($this->detail),
            ($this->getErrorMessages()[0] ?? 'Unknown.')
        );
        parent::__construct($message, $code, $previous);
    }

    public function getErrorDetails(): array
    {
        return $this->detail;
    }

    public function getErrorMessages(): array
    {
        $messages = [];
        foreach ($this->getErrorDetails() as $detail) {
            $messages[] = (string) $detail;
        }
        return $messages;
    }

    public function getStatusCode(): int
    {
        return $this->status;
    }
}
