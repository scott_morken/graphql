<?php

namespace Smorken\Graphql\Models\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;

trait AttributeToRelation
{

    protected function addAttributeAsModelForRelation(string $attribute, Model $model, Relation $relation): ?Model
    {
        $c = $model->getAttributeFromArray($attribute);
        if ($c) {
            return $relation->getRelated()->newInstance($c);
        }
        return null;
    }

    protected function addAttributeToCollectionForRelation(
        string $attribute,
        Model $model,
        Relation $relation,
        Collection $collection
    ): void {
        $items = $model->getAttributeFromArray($attribute);
        if ($items && count($items)) {
            foreach ($items as $item) {
                $m = $relation->getRelated()->newInstance($item);
                $collection->push($m);
            }
        }
    }
}
