<?php

namespace Smorken\Graphql\Contracts\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Contracts\Response\FromClient;

interface Client
{

    /**
     * @param  \Psr\Http\Message\ResponseInterface|\GuzzleHttp\Exception\GuzzleException  $response
     */
    public function addResponse(ResponseInterface|GuzzleException $response): void;

    /**
     * @return \Smorken\Graphql\Contracts\Response\FromClient
     */
    public function getFromClient(): FromClient;

    /**
     * @return \Smorken\Graphql\Contracts\Clients\Proxy
     */
    public function getProxy(): Proxy;

    /**
     * @param  Query  $query
     * @param  array  $vars
     * @return \Smorken\Graphql\Contracts\Response\FromClient
     */
    public function query(Query $query, array $vars = []): FromClient;

    /**
     * @param  string  $header
     * @param  string  $value
     */
    public function setHeader(string $header, string $value): void;

    /**
     * @param  \Smorken\Graphql\Contracts\Clients\Proxy
     */
    public function setProxy(Proxy $proxy): void;
}
