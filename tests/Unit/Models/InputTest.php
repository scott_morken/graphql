<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Contracts\Models\Payload;
use Smorken\Graphql\Models\Input;
use Tests\Smorken\Graphql\Unit\Models\Stubs\TestInput;

class InputTest extends TestCase
{

    public function testGetPayloadModelDefault(): void
    {
        $sut = new Input();
        $this->assertInstanceOf(Payload::class, $sut->getPayloadModel());
    }

    public function testGetPayloadModelFromSet(): void
    {
        $sut = new Input();
        $p = new Stubs\Payload();
        $sut->setPayloadModel($p);
        $this->assertSame($p, $sut->getPayloadModel());
    }

    public function testToInputIsEmptyByDefault(): void
    {
        $sut = new Input(['foo' => 'bar', 'fiz' => 'buzz', 'id' => 42]);
        $expected = [];
        $this->assertEquals($expected, $sut->toInput());
    }

    public function testToInputLimited(): void
    {
        $sut = new TestInput(['foo' => 'bar', 'fiz' => 'buzz', 'id' => 42]);
        $expected = [
            'foo' => 'bar', 'id' => 42,
        ];
        $this->assertEquals($expected, $sut->toInput());
    }
}
