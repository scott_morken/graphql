<?php

namespace Smorken\Graphql\Models;

use Smorken\Model\VO;

class Model extends VO implements \Smorken\Graphql\Contracts\Models\Model
{

    /**
     * @var bool
     */
    protected bool $canMulti = true;

    /**
     * Default query callbacks to apply to new queries
     *
     * @var array
     */
    protected array $defaultQueryStack = [];

    /**
     * Attribute names of fields
     *
     * @var array
     */
    protected array $fields = [];

    /**
     * @var string|null
     */
    protected ?string $queryName = null;

    /**
     * key => ['type' => 'String', 'required' => false, 'default' => null]
     *
     * @var array
     */
    protected array $requestVariables = [];

    /**
     * Handle additional data when creating a model in Response\ToModel
     *
     * @param $data
     */
    public function applyData($data): void
    {
        // override if needed, otherwise do nothing
    }

    /**
     * Override: flip the relationship check to that fakeRelations work on existing keys
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key): mixed
    {
        if (!$key) {
            return null;
        }
        if (!method_exists(self::class, $key) && method_exists($this, 'getRelationValue')) {
            $value = $this->getRelationValue($key);
            if ($value) {
                return $value;
            }
        }

        // If the attribute exists in the attribute array or has a "get" mutator we will
        // get the attribute's value. Otherwise, we will proceed as if the developers
        // are asking for a relationship's value. This covers both types of values.
        if (array_key_exists($key, $this->attributes) || $this->hasGetMutator($key)) {
            return $this->getAttributeValue($key);
        }
        return null;
    }

    public function getName(): string
    {
        return class_basename($this);
    }
}
