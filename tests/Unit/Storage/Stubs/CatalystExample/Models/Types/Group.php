<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;
use Smorken\Graphql\Models\Traits\AttributeToRelation;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Connections\UserConnection;

class Group extends Queryable
{

    use AttributeToRelation, FakeRelations;

    protected array $fields = ['id', 'name', 'description'];

    public $primaryKey = 'groupId';

    /**
     * @var \Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Connections\UserConnection
     */
    protected $userConnection;

    public function queryUserConnection(Query $query, ?Filter $filter = null): Query
    {
        $uc = $this->getUserConnection()
                   ->newInstance()
                   ->newQuery()
                   ->setMulti(true)
                   ->applyFilter($filter)
                   ->defaultFields();
        return $query->addField($uc);
    }

    public function userConnection()
    {
        return $this->fakeBelongsToViaCallback(
            UserConnection::class,
            function (FakeRelation $relation, Group $model) {
                return $this->addAttributeAsModelForRelation('users', $model, $relation);
            }
        );
    }

    protected function filterUserConnection(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            if ($value instanceof Filter) {
                $this->queryUserConnection($query, $value);
            } else {
                $this->queryUserConnection($query);
            }
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_user_connection' => 'filterUserConnection',
        ];
    }

    protected function getUserConnection(): UserConnection
    {
        if (!$this->userConnection) {
            $this->userConnection = $this->userConnection()->getRelated();
        }
        return $this->userConnection;
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        return $this->newQueryEmpty()->setName('myOrg');
    }
}
