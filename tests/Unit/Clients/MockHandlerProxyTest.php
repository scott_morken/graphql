<?php

namespace Tests\Smorken\Graphql\Unit\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Clients\MockHandlerProxy;

class MockHandlerProxyTest extends TestCase
{

    public function testInstanceAppliesResponsesToHandlerAfterSet(): void
    {
        $sut = MockHandlerProxy::getInstance();
        $response = new Response(200, [], 'foo bar');
        $sut->addResponse($response);
        $handler = new MockHandler();
        $sut->setHandler($handler);
        $stack = HandlerStack::create($handler);
        $client = new Client(['handler' => $stack]);
        $r = $client->get('https://localhost');
        $this->assertSame($response, $r);
    }

    public function testInstanceUpdatesMockHandler(): void
    {
        $one = MockHandlerProxy::getInstance();
        $two = MockHandlerProxy::getInstance();
        $this->assertSame($one, $two);
        $handler = new MockHandler();
        $two->setHandler($handler);
        $this->assertSame($handler, $one->getHandler());
        $this->assertSame($handler, $two->getHandler());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        MockHandlerProxy::reset();
    }
}
