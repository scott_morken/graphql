<?php

namespace Tests\Smorken\Graphql\Traits;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Authenticate\None;
use Smorken\Graphql\Clients\Client;
use Smorken\Graphql\Clients\GuzzleProxy;
use Smorken\Graphql\Contracts\Factory;
use Smorken\Graphql\Response\FromClient;
use Tests\Smorken\Graphql\Support\NullLogger;

trait Response
{

    protected function createFactory(): Factory
    {
        $handler = new MockHandler();
        $stack = HandlerStack::create($handler);
        $proxy = new GuzzleProxy('https://localhost', new \GuzzleHttp\Client(['handler' => $stack]));
        $proxy->getMockHandlerProxy()->setHandler($handler);
        $client = new Client($proxy, new FromClient());
        return new \Smorken\Graphql\Factory(new None(), $client, new NullLogger(), true);
    }

    protected function getFilename(string $file): string
    {
        $base = null;
        if (property_exists($this, 'base_path')) {
            $base = ltrim(Str::finish($this->base_path, '/'), '/');
        }
        return sprintf('%s/../data/responses/%s%s', __DIR__, $base, ltrim($file, '/'));
    }

    protected function getResponseData(string $file): string
    {
        return file_get_contents($this->getFilename($file));
    }

    protected function makeResponse(string $file, int $status = 200): ResponseInterface
    {
        return new \GuzzleHttp\Psr7\Response($status, [], $this->getResponseData($file));
    }
}
