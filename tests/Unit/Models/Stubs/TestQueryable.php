<?php

namespace Tests\Smorken\Graphql\Unit\Models\Stubs;

use Smorken\Graphql\Models\Queryable;

class TestQueryable extends Queryable
{

    protected array $fields = ['id', 'name'];
}
