<?php

namespace Smorken\Graphql;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Psr\Log\LoggerInterface;
use Smorken\Graphql\Authenticate\None;
use Smorken\Graphql\Clients\GuzzleProxy;
use Smorken\Graphql\Clients\MockHandlerProxy;
use Smorken\Graphql\Contracts\Authenticate;
use Smorken\Graphql\Contracts\Clients\Client;
use Smorken\Graphql\Contracts\Clients\Proxy;
use Smorken\Graphql\Contracts\Query\Argument;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Contracts\Query\RequestVariable;
use Smorken\Graphql\Contracts\Query\Variable;
use Smorken\Graphql\Contracts\Response\FromClient;
use Smorken\Graphql\Contracts\Response\ToModel;
use Smorken\Graphql\Models\Mutatable;
use Smorken\Graphql\Models\Queryable;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    protected array $providers = [];

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
        Queryable::setQuery($this->app[Query::class]);
        Mutatable::setQuery($this->app[Query::class]);
        $this->setProvidersOnQuery();
    }

    public function register()
    {
        $this->bindAuthenticate();
        $this->bindFactory();
        $this->bindLast();
        $this->bindClient();
        $this->bindClientProxy();
        $this->bindArgument();
        $this->bindQuery();
        $this->bindRequestVariable();
        $this->bindVariable();
        $this->bindFromClient();
        $this->bindToModel();
    }

    protected function bindArgument()
    {
        $this->app->bind(
            Argument::class,
            function ($app) {
                $cls = $this->getProvider($app, Argument::class, \Smorken\Graphql\PhpGraphQL\Argument::class);
                return new $cls();
            }
        );
    }

    protected function bindAuthenticate()
    {
        $this->app->bind(
            Authenticate::class,
            function ($app) {
                $cls = $this->getProvider($app, Authenticate::class, None::class);
                return $app->make($cls);
            }
        );
    }

    protected function bindClient()
    {
        $this->app->bind(
            Client::class,
            function ($app) {
                $cls = $this->getProvider($app, Client::class, \Smorken\Graphql\Clients\Client::class);
                $proxy = $app[Proxy::class];
                $fc = $app[FromClient::class];
                return new $cls($proxy, $fc);
            }
        );
    }

    protected function bindClientProxy()
    {
        $this->app->bind(
            Proxy::class,
            function ($app) {
                $cls = $this->getProvider($app, Proxy::class, GuzzleProxy::class);
                $proxy_options = $app['config']->get('graphql.client.proxy_options', []);
                if ($app['config']->get('graphql.client.mock_responses', false)) {
                    $handler = $this->getMockHandler();
                    $stack = HandlerStack::create($handler);
                    $proxy_options['handler'] = $stack;
                }
                $backend = new \GuzzleHttp\Client($proxy_options);
                return new $cls(
                    $app['config']->get('graphql.client.endpoint'),
                    $backend,
                    $app['config']->get('graphql.client.client_options')
                );
            }
        );
    }

    protected function bindFactory()
    {
        $this->app->bind(
            \Smorken\Graphql\Contracts\Factory::class,
            function ($app) {
                $cls = $this->getProvider($app, \Smorken\Graphql\Contracts\Factory::class, Factory::class);
                $auth = $app[Authenticate::class];
                $client = $app[Client::class];
                $logger = $app[LoggerInterface::class];
                $debug = $app['config']->get('app.debug', false);
                return new $cls($auth, $client, $logger, $debug);
            }
        );
    }

    protected function bindFromClient()
    {
        $this->app->bind(
            FromClient::class,
            function ($app) {
                $cls = $this->getProvider($app, FromClient::class, \Smorken\Graphql\Response\FromClient::class);
                return new $cls();
            }
        );
    }

    protected function bindLast()
    {
        $this->app->bind(
            \Smorken\Graphql\Contracts\Last::class,
            function ($app) {
                $cls = $this->getProvider($app, \Smorken\Graphql\Contracts\Last::class, Last::class);
                return new $cls();
            }
        );
    }

    protected function bindQuery()
    {
        $this->app->bind(
            Query::class,
            function ($app) {
                $cls = $this->getProvider($app, Query::class, \Smorken\Graphql\PhpGraphQL\Query::class);
                return new $cls();
            }
        );
    }

    protected function bindRequestVariable()
    {
        $this->app->bind(
            RequestVariable::class,
            function ($app) {
                $cls = $this->getProvider($app, RequestVariable::class, \Smorken\Graphql\Query\RequestVariable::class);
                return new $cls();
            }
        );
    }

    protected function bindToModel()
    {
        $this->app->bind(
            ToModel::class,
            function ($app) {
                $cls = $this->getProvider($app, ToModel::class, \Smorken\Graphql\Response\ToModel::class);
                return new $cls();
            }
        );
    }

    protected function bindVariable()
    {
        $this->app->bind(
            Variable::class,
            function ($app) {
                $cls = $this->getProvider($app, Variable::class, \Smorken\Graphql\PhpGraphQL\Variable::class);
                return new $cls();
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/graphql.php';
        $this->mergeConfigFrom($config, 'graphql');
        $this->publishes([$config => config_path('graphql.php')], 'config');
    }

    protected function getMockHandler(): MockHandler
    {
        $handler = MockHandlerProxy::getInstance()->getHandler();
        if (!$handler) {
            $handler = new MockHandler();
            MockHandlerProxy::getInstance()->setHandler($handler);
        }
        return $handler;
    }

    protected function getProvider($app, string $interface, string $concrete)
    {
        if (empty($this->providers)) {
            $this->providers = $app['config']->get('graphql.providers', []);
        }
        return $this->providers[$interface] ?? $concrete;
    }

    protected function setProvidersOnQuery()
    {
        $cls = $this->getProvider($this->app, Query::class, \Smorken\Graphql\PhpGraphQL\Query::class);
        $cls::setArgumentProvider($this->app[Argument::class]);
        $cls::setRequestVariableProvider($this->app[RequestVariable::class]);
        $cls::setVariableProvider($this->app[Variable::class]);
    }
}
