<?php

namespace Tests\Smorken\Graphql\Unit\Response;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Response\FromClient;
use Tests\Smorken\Graphql\Traits\Response;

class FromClientTest extends TestCase
{

    use Response;

    protected $base_path = 'Response/FromClientTest/';

    public function testErrorByStatusCodeAndDataExists(): void
    {
        $response = $this->makeResponse('one.model.json', 401);
        $sut = new FromClient($response);
        $this->assertTrue($sut->hasErrors());
        $this->assertCount(1, $sut->getErrors());
        $this->assertEquals(
            'Status: 401 - Message: Unauthorized - Url: None - Path: None',
            (string) $sut->getErrors()[0]
        );
        $this->assertCount(1, $sut->getData());
    }

    public function testErrorWhenDataExists(): void
    {
        $response = $this->makeResponse('error.notSkippableWithData.json');
        $sut = new FromClient($response);
        $this->assertTrue($sut->hasErrors());
        $this->assertCount(1, $sut->getErrors());
        $this->assertCount(1, $sut->getData());
    }

    public function testErrorWithStatusCode(): void
    {
        $response = $this->makeResponse('error.notSkippable.json');
        $sut = new FromClient($response);
        $this->assertTrue($sut->hasErrors());
        $this->assertEquals(
            "Status: 409 - Message: An active User with email 'first1.last1@example.edu' already exists in this organization - Url: http://ego/api/users/ - Path: 0: createUser",
            (string) $sut->getErrors()[0]
        );
    }

    public function testNullResponse(): void
    {
        $sut = new FromClient();
        $this->assertEmpty($sut->getData());
        $this->assertEmpty($sut->getErrors());
        $this->assertNull($sut->getResponse());
        $this->assertEquals([], $sut->toArray());
        $this->assertFalse($sut->hasErrors());
    }

    public function testSkippableError(): void
    {
        $response = $this->makeResponse('error.skippable.json');
        $sut = new FromClient($response);
        $this->assertEquals(
            [
                'userBySsoId' => null,
            ],
            $sut->getData()
        );
        $this->assertCount(1, $sut->getErrors());
        $this->assertEquals(
            [
                'message' => '404: Not Found',
                'locations' => [],
                'path' => [
                    'userBySsoId',
                ],
                'extensions' => [
                    'code' => 'INTERNAL_SERVER_ERROR',
                    'response' => [
                        'status' => 404,
                        'statusText' => 'Not Found',
                        'url' => 'http://ego/api/users/userBySsoExternalId/abc123/org-id-0',
                        'body' => [
                            'type' => 'ErrorResponse',
                            'error' => 'DOES_NOT_EXIST',
                            'error_description' => "No user found in with externalId 'abc123' in org 'org-id-0'",
                        ],
                    ],
                ],
            ],
            $sut->getErrors()[0]->toRawArray()
        );
        $this->assertSame($response, $sut->getResponse());
        $this->assertEquals(
            [
                'errors' => [
                    [
                        'message' => '404: Not Found',
                        'locations' => [],
                        'path' => [
                            'userBySsoId',
                        ],
                        'extensions' => [
                            'code' => 'INTERNAL_SERVER_ERROR',
                            'response' => [
                                'status' => 404,
                                'statusText' => 'Not Found',
                                'url' => 'http://ego/api/users/userBySsoExternalId/abc123/org-id-0',
                                'body' => [
                                    'type' => 'ErrorResponse',
                                    'error' => 'DOES_NOT_EXIST',
                                    'error_description' => "No user found in with externalId 'abc123' in org 'org-id-0'",
                                ],
                            ],
                        ],
                    ],
                ],
                'data' => [
                    'userBySsoId' => null,
                ],
            ],
            $sut->toArray()
        );
        $this->assertTrue($sut->hasErrors());
    }

    public function testValidResponse(): void
    {
        $response = $this->makeResponse('one.model.json');
        $sut = new FromClient($response);
        $this->assertEquals(
            [
                'model' => [
                    'id' => 'some-id',
                    'name' => 'One Model',
                ],
            ],
            $sut->getData()
        );
        $this->assertEmpty($sut->getErrors());
        $this->assertSame($response, $sut->getResponse());
        $this->assertEquals(
            [
                'data' => [
                    'model' => [
                        'id' => 'some-id',
                        'name' => 'One Model',
                    ],
                ],
            ],
            $sut->toArray()
        );
        $this->assertFalse($sut->hasErrors());
    }
}
