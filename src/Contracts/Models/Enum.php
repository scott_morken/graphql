<?php

namespace Smorken\Graphql\Contracts\Models;

interface Enum extends Queryable
{

    public function __toString();
}
