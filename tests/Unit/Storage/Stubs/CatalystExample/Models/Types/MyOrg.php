<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types;

use Smorken\Graphql\Models\Queryable;

class MyOrg extends Queryable
{

    protected bool $canMulti = false;

    protected array $fields = ['id', 'namespace', 'name'];
}
