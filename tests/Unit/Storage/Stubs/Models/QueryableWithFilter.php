<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;

class QueryableWithFilter extends Queryable
{

    public function queryApplyField(Query $query, string $field): Query
    {
        return $query->addField($field);
    }
}
