<?php

namespace Tests\Smorken\Graphql\Unit\PhpGraphQL\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;

class QueryModel extends Queryable implements \Smorken\Graphql\Contracts\Models\Queryable
{

    public function queryMyFields(Query $query): Query
    {
        return $query->addField(
            $query->newInstance($this)
                  ->setName('subQuery')
                  ->addFields(['id', 'descr'])
        );
    }

    public function queryMyVariable(Query $query, string $var): Query
    {
        return $query->addVariableFromComponents('myVar', 'String', false, $var);
    }
}
