<?php

namespace Smorken\Graphql\Models;

class PageInfo extends Queryable implements \Smorken\Graphql\Contracts\Models\PageInfo
{

    protected array $fields = ['hasNextPage', 'endCursor'];
}
