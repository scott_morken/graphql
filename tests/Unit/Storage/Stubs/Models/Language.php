<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Queryable;

class Language extends Queryable
{

    protected array $fields = ['id', 'code',];
}
