<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Queryable;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;
use Smorken\Support\Contracts\Filter;

class Group extends Queryable
{

    use FakeRelations;

    protected array $fields = [
        'id',
        'name',
    ];

    /**
     * @var \Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\UserConnection
     */
    protected $userConnection;

    public function queryDescriptionField(Query $query): Query
    {
        return $query->addField('description');
    }

    public function queryOrgIdIs(Query $query, string $id): Query
    {
        $query->getBaseQuery()->addArgumentFromComponents('orgId', $id, false);
        return $query;
    }

    public function queryUserConnection(Query $query, ?Filter $filter = null): Query
    {
        $uc = $this->getUserConnection()
                   ->newInstance()
                   ->newQuery()
                   ->setMulti(true)
                   ->applyFilter($filter)
                   ->defaultFields();
        return $query->addField($uc);
    }

    public function userConnection()
    {
        return $this->fakeBelongsToViaCallback(
            UserConnection::class,
            function (FakeRelation $relation, Group $model) {
                $c = $model->getAttributeFromArray('users');
                if ($c) {
                    return $relation->getRelated()->newInstance($c);
                }
                return null;
            }
        );
    }

    protected function filterGroupDescription(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            $query->descriptionField();
        }
        return $query;
    }

    protected function filterUserConnection(Query $query, Filter $filter, $value): Query
    {
        if ($value) {
            if ($value instanceof Filter) {
                $this->queryUserConnection($query, $value);
            } else {
                $this->queryUserConnection($query);
            }
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_user_connection' => 'filterUserConnection',
            'f_group_description' => 'filterGroupDescription',
        ];
    }

    protected function getUserConnection(): UserConnection
    {
        if (!$this->userConnection) {
            $this->userConnection = $this->userConnection()->getRelated();
        }
        return $this->userConnection;
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
        return $this->defaultNewQuery(false)->setName('myOrg');
    }
}
