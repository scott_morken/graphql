<?php

namespace Smorken\Graphql\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Smorken\Graphql\Contracts\Clients\Proxy;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Contracts\Response\FromClient;

class Client implements \Smorken\Graphql\Contracts\Clients\Client
{

    /**
     * @var \Smorken\Graphql\Contracts\Response\FromClient
     */
    protected FromClient $fromClient;

    /**
     * @var Proxy
     */
    protected Proxy $proxy;

    public function __construct(Proxy $proxy, FromClient $fromClient)
    {
        $this->setProxy($proxy);
        $this->fromClient = $fromClient;
    }

    public function addResponse(ResponseInterface|GuzzleException $response): void
    {
        $this->getProxy()->addResponse($response);
    }

    public function getFromClient(): FromClient
    {
        return $this->fromClient;
    }

    public function getProxy(): Proxy
    {
        return $this->proxy;
    }

    public function setProxy(Proxy $proxy): void
    {
        $this->proxy = $proxy;
    }

    public function query(Query $query, array $vars = []): FromClient
    {
        $response = $this->getProxy()->query($query, $vars);
        return $this->getFromClient()->newInstance($response);
    }

    public function setHeader(string $header, string $value): void
    {
        $this->getProxy()->setHeader($header, $value);
    }
}
