<?php

namespace Smorken\Graphql;

use Illuminate\Support\Str;

class Index
{

    protected static string $indexName = 'index';

    protected static string $indexSeparator = '_';

    protected static array $previous = [];

    protected static array $values = [
        'indexName' => 'index',
        'indexSeparator' => '_',
    ];

    public static function apply(?string $name, ?string $index): string
    {
        $suffix = sprintf('%s%s', self::getIndexSeparator(), $index);
        if (!is_null($name) && strlen($name) && !is_null($index) && !Str::endsWith($name, $suffix)) {
            $name = sprintf('%s%s', $name, $suffix);
        }
        return $name ?? '';
    }

    public static function applyToKeys(array $items, ?string $index): array
    {
        $keyed = [];
        foreach ($items as $k => $v) {
            $k = self::apply($k, $index);
            $keyed[$k] = $v;
        }
        return $keyed;
    }

    public static function index(?string $name, ?string $index): string
    {
        $prefix = sprintf('%s:', self::apply(self::getIndexName(), $index));
        if (!is_null($name) && strlen($name) && !is_null($index) && !Str::startsWith($name, $prefix)) {
            $name = sprintf('%s %s', $prefix, $name);
        }
        return $name ?? '';
    }

    public static function reset(): void
    {
        foreach (self::$previous as $k => $v) {
            self::$values[$k] = $v;
        }
    }

    protected static function getIndexName(): string
    {
        return self::$values['indexName'] ?? 'index';
    }

    public static function setIndexName(string $name): void
    {
        self::$previous['indexName'] = self::getIndexName();
        self::$values['indexName'] = $name;
    }

    protected static function getIndexSeparator(): string
    {
        return self::$values['indexSeparator'] ?? '_';
    }

    public static function setIndexSeparator(string $separator): void
    {
        self::$previous['indexSeparator'] = self::getIndexSeparator();
        self::$values['indexSeparator'] = $separator;
    }
}
