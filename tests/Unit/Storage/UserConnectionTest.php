<?php

namespace Tests\Smorken\Graphql\Unit\Storage;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Response\ToModel;
use Smorken\Support\Filter;
use Tests\Smorken\Graphql\Traits\Response;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Connections\UserConnection;

class UserConnectionTest extends TestCase
{

    use Response;

    protected $base_path = 'Storage/UserConnectionTest/';

    public function testChunkBaseQuery(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('UserConnection.chunk.1.json');
        $params['factory']->getClient()->addResponse($response);
        $response = $this->makeResponse('UserConnection.chunk.2.json');
        $params['factory']->getClient()->addResponse($response);
        $counts = [5, 1];
        $usersLine = [
            'users(first: 100 after: null)',
            'users(first: 100 after: "MTAw")',
        ];
        $counter = 0;
        $sut->chunk(
            new Filter(),
            function ($models) use ($counts, $usersLine, $sut, &$counter) {
                $this->assertCount($counts[$counter], $models);
                $this->assertStringContainsString($usersLine[$counter], $sut->getFactory()->getLast()->query);
                $counter++;
            }
        );
        $last = $sut->getFactory()->getLast();
        $expected = 'query {
myOrg {
users(first: 100 after: "MTAw") {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
email
createdAt
updatedAt
firstName
lastName
ssoId
status
}
}
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals([], $last->variables);
    }

    protected function getParams(): array
    {
        return [
            'model' => new UserConnection(),
            'factory' => $this->createFactory(),
            'toModel' => new ToModel(),
        ];
    }

    protected function getSut(array $params):
    \Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Storage\UserConnection {
        return new \Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Storage\UserConnection(
            ...
            array_values($params)
        );
    }
}
