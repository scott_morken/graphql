<?php

namespace Tests\Smorken\Graphql\Unit\Models;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Graphql\Unit\Models\Stubs\Payload;
use Tests\Smorken\Graphql\Unit\Models\Stubs\User;

class PayloadTest extends TestCase
{

    public function testGetName(): void
    {
        $sut = new Payload();
        $this->assertEquals('Payload', $sut->getName());
    }

    public function testToArray(): void
    {
        $sut = new Payload(['id' => 'id-1', 'user' => ['id' => 'user-id', 'firstName' => 'Foo']]);
        $expected = [
            'id' => 'id-1',
            'user' => [
                'id' => 'user-id',
                'firstName' => 'Foo',
            ],
        ];
        $this->assertEquals($expected, $sut->toArray());
    }

    public function testUserRelation(): void
    {
        $sut = new Payload(['id' => 'id-1', 'user' => ['id' => 'user-id', 'firstName' => 'Foo']]);
        $user = $sut->user;
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('user-id', $user->id);
        $this->assertEquals('Foo', $user->firstName);
    }
}
