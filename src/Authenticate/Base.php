<?php

namespace Smorken\Graphql\Authenticate;

use Smorken\Graphql\Clients\HasMockHandlerProxy;
use Smorken\Graphql\Contracts\Authenticate;

abstract class Base implements Authenticate
{

    use HasMockHandlerProxy;

    protected array $params = [];

    public function isAuthenticationException(\Throwable $exception, int $statusCode = 0): bool
    {
        return $statusCode === 401;
    }

    public function setParam(string $key, $value): void
    {
        $this->params[$key] = $value;
    }

    public function setParams(array $params): void
    {
        foreach ($params as $k => $v) {
            $this->setParam($k, $v);
        }
    }
}
