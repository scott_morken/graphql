<?php

namespace Smorken\Graphql\Contracts\Models;

/**
 * Interface PageInfo
 *
 * @package Smorken\Graphql\Contracts\Models
 *
 * @property bool $hasNextPage
 * @property string $endCursor
 */
interface PageInfo extends Queryable
{

}
