<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Connections;

use Illuminate\Support\Collection;
use Smorken\Graphql\Contracts\Query\Query;
use Smorken\Graphql\Models\Connection;
use Smorken\Graphql\Models\Traits\AttributeToRelation;
use Smorken\Model\Relations\FakeRelation;
use Smorken\Model\Relations\Traits\FakeRelations;
use Smorken\Support\Contracts\Filter;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types\Group;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types\MyOrg;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\CatalystExample\Models\Types\User;

class UserConnection extends Connection
{

    use FakeRelations, AttributeToRelation;

    protected array $models = [
        self::NODE => User::class,
        self::EDGE => \Smorken\Graphql\Models\Queryable::class,
        self::PAGEINFO => \Smorken\Graphql\Models\PageInfo::class,
    ];

    protected ?string $queryName = 'user';

    public function queryGroupAsParent(Query $query): Query
    {
        $groupQuery = (new Group())->newQueryWithoutDefaults();
        $groupQuery->addField($query);
        return $query;
    }

    public function queryMyOrgAndGroupAsParent(Query $query): Query
    {
        $groupQuery = $this->queryGroupAsParent($query)->getBaseQuery();
        $this->queryMyOrgAsParent($groupQuery);
        return $query;
    }

    public function queryMyOrgAsParent(Query $query): Query
    {
        $orgQuery = (new MyOrg())->newQueryWithoutDefaults();
        $orgQuery->addField($query);
        return $query;
    }

    public function users()
    {
        return $this->fakeHasManyViaCallback(
            User::class,
            function (FakeRelation $relation, UserConnection $model, Collection $collection) {
                $nodes = $model->nodes;
                foreach ($nodes as $node) {
                    $collection->push($node);
                }
            }
        );
    }

    protected function filterGroupParent(Query $query, Filter $filter, $value): Query
    {
        if ($value === true) {
            $query->groupAsParent();
        }
        return $query;
    }

    protected function filterMyOrgAndGroupParent(Query $query, Filter $filter, $value): Query
    {
        if ($value === true) {
            $query->myOrgAndGroupAsParent();
        }
        return $query;
    }

    protected function filterMyOrgParent(Query $query, Filter $filter, $value): Query
    {
        if ($value === true) {
            $query->myOrgAsParent();
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_first' => 'filterFirst',
            'f_after_cursor' => 'filterAfterCursor',
            'f_myOrg_parent' => 'filterMyOrgParent',
            'f_group_parent' => 'filterGroupParent',
            'f_myOrg_group_parent' => 'filterMyOrgAndGroupParent',
        ];
    }

    protected function newParentQuery(): ?\Smorken\Graphql\Contracts\Query\Query
    {
//        $group = (new Group())->newQueryWithoutDefaults();
//        $org = (new MyOrg())->newQueryWithoutDefaults()->addField($group);
//        return $group;
        return null;
    }
}
