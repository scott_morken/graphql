<?php

namespace Smorken\Graphql\Contracts\Query;

interface Variable
{

    public function __toString(): string;

    /**
     * @return object proxied variable object
     */
    public function get(): mixed;

    public function getDefaultValue(): mixed;

    public function getIsRequired(): bool;

    public function getName(): string;

    public function getType(): string;

    public function newInstance(
        string $name,
        string $type,
        bool $isRequired = false,
        $defaultValue = null
    ): static;

    public function setDefaultValue($value): self;

    public function setIsRequired(bool $isRequired): self;

    public function setName(string $name): self;

    public function setType(string $type): self;
}
