<?php

namespace Smorken\Graphql\Contracts\Models;

interface Input extends Mutatable
{

    /**
     * The name of the variable key to add to the request variables
     *
     * @return string
     */
    public function getInputVariableName(): string;

    /**
     * @return \Smorken\Graphql\Contracts\Models\Payload
     */
    public function getPayloadModel(): Payload;

    /**
     * @param  \Smorken\Graphql\Contracts\Models\Payload  $model
     * @return \Smorken\Graphql\Contracts\Models\Input
     */
    public function setPayloadModel(Payload $model): self;

    /**
     * @return array
     */
    public function toInput(): array;
}
