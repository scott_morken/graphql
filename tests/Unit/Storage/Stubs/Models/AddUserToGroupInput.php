<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Input;

class AddUserToGroupInput extends Input
{
    protected array $inputAttributes = [
        'userId',
        'groupId',
        'clientMutationId',
    ];

    protected \Smorken\Graphql\Contracts\Models\Payload|string $payload = AddUserToGroupPayload::class;

    protected array $rules = [
        'userId' => 'required',
        'groupId' => 'required',
    ];
}
