<?php

namespace Smorken\Graphql\Models\Traits;

use Smorken\Graphql\Contracts\Query\Query;

trait Mutate
{

    public function getRules(): array
    {
        return $this->rules ?? [];
    }

    public function getValidationRules(array $overrides = []): array
    {
        return array_replace($this->getRules(), $overrides);
    }

    public function newMutation(): Query
    {
        return $this->defaultNewQuery(true);
    }

    public function newMutationEmpty(): \Smorken\Graphql\Contracts\Query\Query
    {
        return $this->getQuery()->newInstance(null)
                    ->setQueryType($this->getQueryType());
    }

    public function newMutationWithoutDefaults(): Query
    {
        return $this->defaultNewQuery(false);
    }

    public function queryDefaultVariables(Query $query, array $overrides = []): Query
    {
        foreach ($this->getRequestVariables($overrides) as $key => $value) {
            $this->addVariableByKey($query, $key);
            $this->addArgumentAsVariable($query, $key);
            $this->addRequestVariable($query, $key, $value);
        }
        return $query;
    }

    protected function addArgumentAsVariable(Query $query, string $key): void
    {
        $query->addArgumentAsVariable($key);
    }

    protected function addRequestVariable(Query $query, string $key, $value): void
    {
        $query->addRequestVariableFromComponents($key, $value);
    }

    protected function addVariableByKey(Query $query, string $key): void
    {
        $reqvar = $this->requestVariables[$key] ?? [];
        $query->addVariableFromComponents(
            $key,
            $reqvar['type'] ?? 'String',
            $reqvar['required'] ?? false,
            $reqvar['default'] ?? null
        );
    }
}
