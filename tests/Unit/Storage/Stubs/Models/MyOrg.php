<?php

namespace Tests\Smorken\Graphql\Unit\Storage\Stubs\Models;

use Smorken\Graphql\Models\Queryable;

class MyOrg extends Queryable
{

    protected bool $canMulti = false;

    protected array $fields = ['id', 'namespace', 'name'];
}
