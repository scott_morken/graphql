<?php

namespace Tests\Smorken\Graphql\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Index;

class IndexTest extends TestCase
{

    public function testApplyDoesNotAddAgain(): void
    {
        $this->assertEquals('foo_0', Index::apply('foo_0', 0));
    }

    public function testApplyToKeysWithNumericKeys(): void
    {
        $this->assertEquals(['0_0' => 'bar'], Index::applyToKeys(['bar'], 0));
    }

    public function testApplyToKeysWithStringKeys(): void
    {
        $this->assertEquals(['foo_0' => 'bar'], Index::applyToKeys(['foo' => 'bar'], 0));
    }

    public function testApplyWithDifferentIndexSeparator(): void
    {
        Index::setIndexSeparator('*');
        $this->assertEquals('foo*bar', Index::apply('foo', 'bar'));
        Index::reset();
    }

    public function testApplyWithFalseyAndInt(): void
    {
        $this->assertEquals('0_0', Index::apply(0, 0));
    }

    public function testApplyWithIntAndInt(): void
    {
        $this->assertEquals('1_0', Index::apply(1, 0));
    }

    public function testApplyWithNullAndNull(): void
    {
        $this->assertEquals('', Index::apply(null, null));
    }

    public function testApplyWithStringAndInt(): void
    {
        $this->assertEquals('foo_0', Index::apply('foo', 0));
    }

    public function testApplyWithStringAndNull(): void
    {
        $this->assertEquals('foo', Index::apply('foo', null));
    }

    public function testApplyWithStringAndString(): void
    {
        $this->assertEquals('foo_bar', Index::apply('foo', 'bar'));
    }

    public function testIndexDoesNotAddAgain(): void
    {
        $this->assertEquals('index_0: foo', Index::index('index_0: foo', 0));
    }

    public function testIndexWithDifferentIndexName(): void
    {
        Index::setIndexName('something');
        $this->assertEquals('something_bar: foo', Index::index('foo', 'bar'));
        Index::reset();
    }

    public function testIndexWithFalseyAndInt(): void
    {
        $this->assertEquals('index_0: 0', Index::index(0, 0));
    }

    public function testIndexWithIntAndInt(): void
    {
        $this->assertEquals('index_0: 1', Index::index(1, 0));
    }

    public function testIndexWithNullAndNull(): void
    {
        $this->assertEquals('', Index::index(null, null));
    }

    public function testIndexWithStringAndInt(): void
    {
        $this->assertEquals('index_0: foo', Index::index('foo', 0));
    }

    public function testIndexWithStringAndNull(): void
    {
        $this->assertEquals('foo', Index::index('foo', null));
    }

    public function testIndexWithStringAndString(): void
    {
        $this->assertEquals('index_bar: foo', Index::index('foo', 'bar'));
    }
}
