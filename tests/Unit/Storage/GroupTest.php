<?php

namespace Tests\Smorken\Graphql\Unit\Storage;

use PHPUnit\Framework\TestCase;
use Smorken\Graphql\Contracts\Models\PageInfo;
use Smorken\Graphql\Response\ToModel;
use Smorken\Support\Filter;
use Tests\Smorken\Graphql\Traits\Response;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\Group;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\Language;
use Tests\Smorken\Graphql\Unit\Storage\Stubs\Models\UserConnection;

class GroupTest extends TestCase
{

    use Response;

    protected $base_path = 'Storage/GroupTest/';

    public function testGetByFilterApplyConnectionViaModel(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('Groups.all.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->getByFilter(new Filter(['f_user_connection' => true]));
        $this->assertCount(2, $all);
        $last = $sut->getFactory()->getLast();
        $expected = 'query {
myOrg {
groups {
id
name
userConnections {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
firstName
lastName
email
}
}
}
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals([], $last->variables);
    }

    public function testGetByFilterApplyQueryWithDefaultFieldsViaNodeModel(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('Groups.all.usersAndLanguages.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->getByFilter(new Filter(['f_user_connection' => new Filter(['f_languages' => true])]));
        $this->assertCount(2, $all);
        $this->assertEquals('group-id-0', $all['groups_0']->id);
        $uc = $all['groups_0']->userConnection;
        $this->assertInstanceOf(UserConnection::class, $uc);
        $this->assertInstanceOf(PageInfo::class, $uc->pageInfo);
        $this->assertEquals(2, $uc->totalCount);
        $this->assertFalse($uc->pageInfo->hasNextPage);
        $this->assertCount(2, $uc->users);
        $this->assertEquals('user-id-0', $uc->users[0]->id);
        $this->assertCount(2, $uc->users[0]->languages);
        $this->assertInstanceOf(Language::class, $uc->users[0]->languages[0]);
        $this->assertEquals('es_419', $uc->users[0]->languages[0]->code);
        $this->assertEquals('en_US', $uc->users[0]->languages[1]->code);
        $this->assertEquals('group-id-1', $all['groups_1']->id);
        $uc = $all['groups_1']->userConnection;
        $this->assertNull($uc);
        $last = $sut->getFactory()->getLast();
        $expected = 'query {
myOrg {
groups {
id
name
userConnections {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
firstName
lastName
email
languages {
id
code
}
}
}
}
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals([], $last->variables);
    }

    public function testGetByFilterApplyQueryWithDefaultFieldsViaNodeModelOverridesDefaults(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('Groups.all.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->getByFilter(new Filter(['f_user_connection' => new Filter(['f_languages' => ['code', 'description']])]));
        $this->assertCount(2, $all);
        $last = $sut->getFactory()->getLast();
        $expected = 'query {
myOrg {
groups {
id
name
userConnections {
totalCount
pageInfo {
hasNextPage
endCursor
}
edges {
node {
id
firstName
lastName
email
languages {
code
description
}
}
}
}
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals([], $last->variables);
    }

    public function testGetByFilterApplyViaModel(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('Groups.all.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->getByFilter(new Filter(['f_group_description' => true]));
        $this->assertCount(2, $all);
        $last = $sut->getFactory()->getLast();
        $expected = 'query {
myOrg {
groups {
id
name
description
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals([], $last->variables);
    }

    public function testGetByFilterApplyViaStorage(): void
    {
        $params = $this->getParams();
        $sut = $this->getSut($params);
        $response = $this->makeResponse('Groups.all.json');
        $params['factory']->getClient()->addResponse($response);
        $all = $sut->getByFilter(new Filter(['f_org_id' => 'abc123']));
        $this->assertCount(2, $all);
        $last = $sut->getFactory()->getLast();
        $expected = 'query {
myOrg(orgId: "abc123") {
groups {
id
name
}
}
}';
        $this->assertEquals($expected, $last->query);
        $this->assertEquals([], $last->variables);
    }

    protected function getParams(): array
    {
        return [
            'model' => new Group(),
            'factory' => $this->createFactory(),
            'toModel' => new ToModel(),
        ];
    }

    protected function getSut(array $params): \Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage\Group
    {
        return new \Tests\Smorken\Graphql\Unit\Storage\Stubs\Storage\Group(...array_values($params));
    }
}
